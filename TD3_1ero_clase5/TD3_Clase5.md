Nosotros tenemos que cargarle en algun lado los parametros a la “funcion”

Podemos precargar en registros los parametros o si tengo inicializada la pila, pasarlselos a la pila. Pero con la pila hay que tener cuidado de cual viene primero en la pila.

Devolver por la pila es medio peligroso si no se tiene en cuenta el desbalanceo que se produce.


Por ahora con:  

- mov
- jxx
    - JZ
    - JNZ
    - JE
    - JNE
- cmp
- loop
- push
- pop
- jmp
- call
- iret
- stosb
- lodsb

Para resolver el ejercicio 3 vamos a seguir a partir de los tropiezos que tuvieron los que lo intentaron


### Aca explicó el funcionamiento de la compuerta A20

Una de las primeras cosas que se deberia hacer es deshabilitar las interrupciones, porque sino, ni bien cae una interrupcion, soné.  
Si no configuro el controlador externo de interrupciones, no deberia llegar nada, por ahi no pasa nada, pero esta bieno poder deshabilitarlas.  
Esto se vera en detalle despues. 

Pregunta: si cuando esta en modo protegido esta todo solapado, no puedo usar las cosas? Si, pero se puede recorrer la memoria con un solo registro sin tener que cambiar de segmento. El cuidado que hay que tener es saber cuales son las barridas que se haran. La mejor practica es no solapar los segmentos. Esta chanchada nos ayuda a copiar de cualquier lugar de la memoria a cualquier otro. Aca la proteccion de memoria no proteje porque no lo estamos usando como se debe.

El tema de los segmentos con los descriptores en modo protegido es dificil de digerir de entrada porque cambia totalmente la forma en que se trabajaba en modo real.

En modo real tenemos los registros selectores:

- CD
- DS
- SS
- ES

Recordar que se suman corridos en modo real

Ahora, por un modo de compatibilidad Intel extendio el registro con una zona invisible, "llamada descriptor de segmento"

CAda vez que logro que uno de los registros selectores cambie su valor, el procesador lo onta y cambia lo que esta en GDT y lo coloca en el registro del segmento.



Al principio de la GDT tiene que que haber 8 bytes que sean 0's y luego empieza el 1er descriptor valido.



gdt:
    dq 0 ; des nulo (dq = define quad word(8 bytes))
    

    
Leyendo formato, 30/159


Ahora nos mostrara codigo de ejemplo de como se cargan los datos al descriptor de segmento. mediante la gdt.


Align 8: la tabla GDT  puede estar en cualuiqer lado del mapa de memoria. pero tiene que ser multiplo de 8. Align rellena con 0's.

se empieza a llenar el descriptor desde derecha a izquierda.



En bochs podemos poner info gdt para que nos la muestre y poder ver si esta bien cargada o no.
Hace lo que haria el procesador para encontrar el GDT, busca a donde apunta GDTR.

El siguiente byte valido es de atributos, por eso esta explicado cada bit.

Document/repos/td3/2018/ej-gtp/gtp-03.


atributos

```
Align 8
GDT16:
NULL_SEL equ $-GDT16
    dq 0x0 ; descriptor nulo


CS_SEL equ $-GDT16 ; descriptro de seg. codigo
    dw 0xFFFF ; limite
    dw 0x00 ; base (16)
    db 0x00 ; base (8)

    db          10011010b
                       +-----   Accedido 
                                Readable
                                Conforming
                                d/c
                                No system
                                \2 bits de DPL = 0 (mayor privilegio)
                                /
                                Presente = 1 se usa para implementar un mecanismo llamado de memoria virtual. Hay segmenos que no entran en memoria, se guardan en disco rigido. Sirve para implementar una pseudo memoria virtual, para engañar al usuario haciendole creer que tiene mas memoria. Windows lo hace con el Pagefile.sys.
                                Pero windows y otros sistemas operativos no usan segmentos, ellos quieren independizarse del procesador, los sistemas operativos usan paginas, por eso se llama pagefile.sys.
                                
; Despues viene este siguiente byte.
    db          11001111b
                                \
                                 \
                                 / 4 bits de campo limite
                                /
                                Avl =  es para que usemos para lo que queramos, no se usa
                                Long, no lo usamos, por eso lo dejamos en 0.
                                Def = 1 Le dice al procesador si tiene que interpretar los bytes de prefijos para instrucicones de 32 o 64. Como lo definimos como segmento de codigo de 32 bits, lo dejamos en.
                                Gran = 1 como usamos tamaño maximo, tenemos que usar maxima granularidad.
```

ahora se define un byte en cero.


El campo indice es el numero de entrada en la tabla.
0 es el descriptor Null, la 2nda entrada es el descriptor 1, con el offset 8.
Si quiero acceder a DS

Ante violacion del mecanismo de interrupcion, el mecanismo se resetea.

Nosotros tenemos un problema.




Hay un segmento especial para la pila que permite empujar la base hacia atras, son los segmentos "expand down", se controla con un bit. si Activo ese bit, sabes que es expand down y se tiene que expandir hacia abajo.

Hay que poner la base en el tamaño maximo al que calculo que llegara la pila. Le doy un mega y cuando se empiece a pasar del limite, salta la proteccion. Por eso el campo se llama limite. Si me paso del limite, revienta.


Si el procesador ve que se esta por pasar la pila, salta la rutina de atencion de interrupcion, busca espacio en algun otro lado.


La pila mayoritariamente se usa para guardar saltos de .

Lo que se hace es pasar un puntero al segmento donde estan los 


En el sistema hay 4 pilas. Hay una por cada nivel de privilegio.

Se tiene que pushear en alguna pila valida la direccion de retorno.

El usuario revento algo en su pila, quien hace le reacomodamiento? se me acaba la pila, tengo que correr la pila, pero al hacer la interrupcion, tiene que guardar en la pila la direccion de retorno. Se guarda en la pila de nivel de privilegio mayor (0). Ahi se guarda la direccion de retorno del sistema operativo. Si esa pila no tiene lugar, reset.


Conmuta el nivel de pila cuando conmuta el nivel de privilegios.




Nos empezamos a enterar de lo que pasa en modo protegido.

Si revienta, revisar los bits de atributo del descriptor.


                                
                                
________________________________________________________________________
________________________________________________________________________





En el nasm no vamos a encontrar el 
mov ax, cr0
, porque es de otro assembler

smsw: Store Machine Status Word
palabra de estado de maquina.
smsw    eax

msw es pseudonimo de cr0

despues con un ana mascara or, CR0_PE es una.

Solo muevo la parte baja de eax para no impactar.

Como el PE esta en la parte baja d

Funciona tanto con eax como con ax.

al hacer esto ya se habilito la proteccion.


smsw    eax ; copia los 16 bits mas bajos de CR0 en AX
or      eax, CR0_PE
lmsw    ax

Con esta instruccion, la de la linea 32, habilite la proteccion, pero aun no intento acceder a la GDT ni cargo ningun selector de modo protegido.

recien con la siguiente instruccion, el jmp
jmp es igual en modo real y protegido
le ponog un valor al registro selector y un offset, despues veremos que valor resultante es, pero salta a la instruccion siguiente. Y aprovecha qye hace el salto para cargar un descriptor a CS.


El manual de intel dice, cuando cargue de modo el procesador debe vaciar el pipeline.
vacia =  flush, tira la cadena. Vacia el deposito de la cola de prebusqueda de instrucciones (pipeline)

va metiendo instrucciones a medio procesar, y si habia un salto, (C)


Debe vaciar porque las instrucciones siguientes iban a estar en modo real y las siguientes deben estar en modo protegido, y funcionan de forma muy diferente ambas maquinas de estado.

Como esto es una maquina de estado compleja, 



Para cambiar entre modo protegido a modo real y viceversa ,hacer siempre un flush del pipeline, porque si no se hace, una instruccion podria querer hacer saltar una interrupcion.




Como cargo el GDTR?
El manual de intel dice que hay que usar la instruccion lgdt, que es la que carga un valor en gdtr.
lgdt    [dsL gdtr_img]




Al final de la GTD pone:

GDT_LENGTH equ $-GDT16

Calcula el tamaño de la GDT.  
El tamaño sera 24 bytes, entonces (por manual de intel) se pone 24 bytes -1.


GDT16: es la GDT que cargo estando en modo real.



(C)


El proximo programa ya no volvemos a modo real.



equ = define.





Cambie los tamaños de los desscriptores y mi mapa de memoria ahora tiene 4 Gb. Ahora en la ubicacion de la ROM, en 0xf0000 hasta 0xFFFF
El programa ahora se copia a si mismo en cero, despues mas arriba y mas arriba.
Y tambien dice defina la pila en algun lado.
La pila la voy a usar en modo real. Entonces puedo poner ESP en algun lugar.

Si me paso con la pila, desborda y pisa todo lo que esta mas alla, porque no hay proteccion por estar en modo real.



Si no ponemos USE32 (es una directiva para el compilador, para que use las instrucciones extendidas) hay problemas, explota, el simulador se cuelga y el procesador real se resetea.

Este codigo que nos manda incluye el codigo del gate a20.
Analizarlo bien para entender como funciona.


Una vez carguemos GDT y GDTR ver con info GDT como nos muestra en pantalla el contenido de GDT. Si tenemos dudas se si la GDT esta bien cargada, entonces ponerle infoGDT, que si esta mal algo lo va a decir.



Push eax y ecx y pop ecx y eax es una buena practica de programacion. Para salvaguardar los registros que toco en mi funcion llamada. Esto conviene hacerlo siempre.


En ambos modos debe ser igual. La forma de ubicar el segmento.



Una vez que copiamos el programa, elegimos una de las copias y ejecutamos una de ellas. En el ejemplo que nos da, copia sobre la copia cero.



(C)Tratar de que se solapen las cosas para que reviente todo.

De ahora en adelante no volvemos mas a modo real, tenemos los mecanismos de proteccion siempre activados.
La forma en que el procesador me avisa de los mecanismos de proteccion es a travez de las interrupcioens.
__________________________________________________________________________________________________________
__________________________________________________________________________________________________________



# Empezamos interrupciones
El ppt de clase 5.

3 tipos de fuentes de interrupcion:
- Hardware
    Son las que entran por la pata de interrupcion. En el chipset hay un PIC, controlador de interrupciones priorizado. El chipset emula como si fueran 2 PICS en cascada. De forma que a cada uno le entran 8 interrupciones. Y como estan uno cascadeado al otro, desde el pin de entrada de interrupciones, se pueden ver 15 interrupciones. el Master tiene 7 interrupciones directas y el esclavo tiene 8 indirectas.
    Si apago las interrupciones, se apagan las excepciones.
    
- Software
    Puedo generar una interrupcion con una instruccion.
    Puedo simular interrupciones por hardware por software. Despues veremos para que sirve, segun el manual de intel puede haber 256, de 0 a 255.

- Internas



Los PIC tienen cableado el bus de datos y a travez de el puede enterarse el micro cual de las interrupciones fue la que interrumpio.

Una interrupcion se identifica con tipos.


Aca entran en conflicto el diseño de la PC con el manual de intel.
Lo que pasa es que tenemos que configurar los PIC y decirles que cuando venga la patita cero una interrupcion le mando un dato y dice que le puedo mandar el que quiera.

Lo que sabemos es que la 0 del pic master va a un chip de timer
Y la pata 1 esta conectada al teclado.

El manual no dice que puedo poner la que quiera donde quiera, de las 256 hay al menos 32 que estan determinadas para el mecanismo de proteccion.


Cualquier violacion a un mecanismo de proteccion produce una int 13

Supongamos que quiero que cuando alguien aprete una tecla del teclado quiero que me salte la int 13.
Leyendo el manual de intel sabemos que de la 32 en adelante puedo usarlas como quiera.

Puede aceptar 256 fuentes de interrupcion configurables, pero el procesador se reserva las primeras 32 para diversos usos que luego veremos.

Las 15 interrupciones de los PIC las podremos poner del 32 al 256.

El numero de chip original es el PIC 8259.
Buscar rutina de inicializacion standar, como lo hace linux.


Hasta aca es como vienen las interrupciones.

Tabla de descriptores de interrupcion, IDT, en vez de ser GDT.

Tendremos que dotar la tabla para (C)

Hay que configurar los PIC para que generen tipos mas altos.


El procesador tiene una pata que dice Mapa de memoria y Mapa de I/O.
Con las instrucciones mov manejo el Mapa de memoria.
Con in y out manejo el Mapa de I/O.

Esta en google, podemos copiarla directamente. No hace falta escribirla nosotros.

Tipo 2 = NMI.

Modo real tambien tiene una tabla de interrupciones. Si queremos hacer una division por cero (y estuvieran habilitadas as interrupciones) nos va a saltar la interrupcion.

Hasta la interrupcion 7 incluida todas funcionan en modo real y protegido. De la 8 en adelante Son las que funcionan solo en modo protegido. A medida que vayamos viendo los diferentes modos de proteccion iremos viendo las siguientes interrupciones, pero esta tabla que esta en el ppt nos da un pantallazo.

De la 20 a la 31 estan reservadas por intel, no se pueden usar.
Si en este procesador quiero usar la 20 para le teclado puedo? Si. Debo? No.



En info 1 deberiamos ver int 80. Si quiero hacer un programa en assembler en linux que haga printf. En el linux se hace con la INT 80. Es una int de las que puedo usar por software.

Se fija el valor de los registros que le paso.

Todas las funciones de la stdlib las tenemos en interrupciones. Son todas por software las otras interrupciones que podemos usar.

En el harware de una PC moderna aparte de los PICS. Tenemos otras interrupciones por hardware, por ejemplo, una placa de video. Es un modulo que le agrega el fabricante al sistema operativo mediante un driver. Que permite usar una de las interrupciones.

Ahora que configuramos todo esto, tenemos que ver como ejecutamos un programa cuando venga una interrupcion.


Despues de esto sigue paginacion.

La rutina de atencion de interrupcion de uso de pagina es de uso extensivo.

