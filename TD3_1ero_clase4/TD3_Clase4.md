# Resumen clase 4

Halt 
es similar a:

aca: hlt
	jmp aca.

Otra opcion seria 
jmp $, pero pone al procesador 100%, no deberia usarse en la practica, pero nosotros lo usaremos porque hlt no anda bien. Tendriamos que agregar como comentario al usar jmp $ el ; hlt.


Si no se inicializa la pila que pasa? Puede no haber pila?

La pila es un pedazo de memoria apuntado por: SS:SP
Tenga la bazura que tenga, la pila va a existir siempre.

La idea es inicializarla en un lugar conocido donde sepa que puede escribir.


Los registros de segmento se acceden siempre a travez de otros registros.

La pila siempre se decrementa.
El SP siempre debe estar apuntado al final.




La idea es que la funcion loop este en otra parte del codigo, mas abajo y la llame.



Al principio dijimos que lo ibamos a copiar en el primer segmento de memoria RAM, eso lo tienen hecho.

Ahora, la funcion que copia, hay que hacerlo con una funcion. Por eso hay que usar la funcion call. Para saltar a una parte del programa que va a copiar.

Los parametros de la funcion son origen, destino y cuanto copiar.

SI:DI son origen y destino y la cantidad es Cx.

Cuando la funcion toma el control, supone que esos valores estan en los valores correctos para funcionar.

Para volver se usa return, en assembler es ret.










call td3_memcopy

El enunciado dice en el prototipo que esa funcion devuelve un puntero en el prototipo que tenemos. Eso queda a criterio nuestro, cual sera el puntero que devolveremos. No dice puntero a que, pero dice que devuelve un puntero.
Podemos hacer que el puntero que devuelve sea uno de los registros apuntando al inicio del lugar donde se copio. De forma tal que antes del ret, el registro DI apunte al inicio de donde “pegamos”.


Nosotros teniamos:

```
Inicio:
    mov ax, cs
    mov ds, ax
    ---
    ---
    ---

    cx ← cantidad
    call td3_memcopy
    jmp (punto de entrada de la copia); 0:0

td3_memcopy:
    lodsb
    stosb
    loop td3_memcopy
    ret
```

Puedo copiar la funcion td3_memcopy en lugar de inicio.
Pero en ese caso al hacer jmp 0:0 va a caer en td3_memcopy


Si no le especifico el segmento, va a saltar al de la ROM


jmp y call tienen 2 sabores, intrasegmento y extrasegmento(le tengo que especificar el segmento)







Hasta ahora todo bien, pero cuando quiera ejecutar la instruccion de retorno se cuelga, porque al ejecutar la instruccion call pasa algo automaticamente por hardware.

Call llama y ejecuta, es como un jump, pero a nivel registro que hace?
Pone el numero td3_memcopy en un registro, en el IP. Entonces hace un salto.

Tiene que volver a la instrucicon siguiente a call, pero ese numero esta guardado en la pila.

Antes de cambiar el IP, guarda en un par de registros(en la pila)(aun no sabemos donde apuntan) SS:SP.

Son 4 bytes, 2 para el CS y 2 para el IP.


Cuando hace un call siempre guarda el CS, ahora cambia el IP y va a ejecutar la funcion, despues retorna. Si la pila esta intacta, entonces la instruccion RET va la pila, 



Que valores tiene SS y SP en el reset? 0:0

Pero como la pila se decrementa antes de usarse, 




La pila la apunto a 0x10000 y se va a decrementar entonces va a apuntar al final.

Cuando saca, saca e incrementa, saca e incrementa.

Si hago calls anidados se apilan ahi. Y los saca solos en el orden correcto.



Antes de hacer el ret hay que hacer un POP

En cualquier lado que haya que hacer push hay que hacer pop.

Siempre que guardemos algo en la pila manualmente hay que sacarlo antes de retornar porque sino no va a poder retornar.



Todo lo que uso dentro de la funcion hago un push y despues un pop.

Siempre que hago un push para variables hago un pop.

No convendria armar una pila alternativa?







Como cargamos 1000 en SS?
Habia 2 instrucciones move que estaban al pedo antes de ir a inicio.

```
Reset:
    cli
    mov ax, 0x1000
    mov ss, ax
    jmp inicio
```


El modo protegido es casi innecesario.

Cuando alguien trata de escribir donde no debe hay una rutina de atencion de interrupcion para esos casos.


Explico la tecnica para ver si la pila alcanzo. Si la pila es muy usada me da una idea de si la pila alcanza.
Le pongo un patron de valores repetidos entonces al usarlo el programa va destruyendo esto y haciendo una marca de agua.

1ro puedo elegir el tamaño de segmento que quiera, no estoy limitado a 64k.
Con el modo protegido.
Si alguien trata de escribir en ese segmento sin permiso me avisa.


Si al bochs le paso:

    u /16 o

me muestra los primeros 16 bytes de memoria.


Si el call es dentro del segmento, no va a pushear el 
si le paso solo el IP, voy a pushear solo el que voy a impactar, va a salrar dentro dle mismo segmento, no necesita guardar el valor de CS, solo el de IP.





Ahora se terminan los segmentos de 64k.

Ahora vemos como es la matrix 

En los proximos ejercicios de la guia nos van a pedir copiar mas alla del 1er mega.



Ppt Memory management Unit.
Administracion de memoria
Gestion de memoria


tenemos 2 opciones para usar toda la memoria completa, pero nos obligan a salir de modo real

Una es el modo protegido, pero no usaremos ese modo.

En la arquitectura hay un modulo que se encarga de manejar el acceso a memoria.
Esa unidad de gestion de memoria (MMU(Memory Management Unit))esta compuesta internamente por …

El programa intensamente va a requerir acceso a memoria, para buscar la instruccion que sigue o para guardar o buscar un dato en memoria.
En este momento el programa genera direcciones logicas en el formato 
Selector: Offset
Me refiero al segmento y dentro del mismo a un determinado desplazamiento.
Cuando busca una instruccion usa CS e IP. Busca la instruccion siguiente.

La MMU tiene que convertir este par de registros en el bus de direcciones fisico del procesador.
Pero en el medio, dependiendo del modo en que use el procesador, primero la unidad de segmentacion, que es la que determina cual es el segmento objetivo, donde esta el segmento al que quiero acceder y calcula el offset dentro del segmento.
De esta unidad sale la direccion lineal.
Calcula la direccion lineal componiendola con selector y offset.
Si no se hace ninguna otra traduccion mas, la direccion lineal se convierte en la direccion fisica.
(como lo venimos usado)

Este procesador puede usar la memoria paginada, donde se produce una traduccion de direccion lineal a direccion fisica, por ahora no lo usaremos, pero nos lo cuenta para que sepamos que existe.

Este trabajo de componer la direccion fisica que haciamos manualmente, lo hara automaticamente la Unidad de segmentacion.


En este modo que esta el procesador puede generar direcciones de 20 bit nada mas, necesitamos que nos genere direcciones de 32 bits.
Una de las formas es cambiarlo a modo protegido. Pero usarlo en modo protegido para copiar un pedazo de memoria es como matar un pajaro a cañonazos.

Nosotros lo que haremos es pasar a modo protegido a para acceder a 32 bits y luego volver a modo real, y desde modo real, con la unidad de segmentacion reconfigurada poder acceder a direcciones mas grandes.

Una pregunta que podria hacer seria, por que no quedarnos dentro de modo protegido??
Es facil cambiar de modo, es solo prender un bit, que genera un monton de cambios en el procesador.
Se activa la unidad de proteccion, que es nuestra pesadilla de ahora en adelante.
Cualquier cosa que queramos ejecutar tocando la memoria nos va a dar una advertencia, porque la idea es que hagamos el programa bien.

Solo en modo protegido puedo reconfigurar la unidad de segmentacion.
Si trabajo en 20 bits, lo reconfiguro a 32, despues en 20 y vuelvo a entrar en modo protegido, no se pone denuevo en 32 automaticamente, hay que tocarlo manualmente.



En modo protegido, cualquier acceso dentro de un segmento protegido, produce una interrupcion.


El otro metodo es dividir la memoria en paginas de igual tamaño.
El modo real se parece a las paginas, si las paginas fueran de 64k, las paginas son accedidas a travez de tablas de paginas, no a travez de registros.



El registro selector siempre tiene 16 bits.

En modo real , el offset es de 16 bits.



El espacio lineal es el mapa maximo, de 32 bits llega a 4gb.



18/159

Resulta que el procesador tiene 4 registros de control (Control Register) CR0, CR1, CR2, CR3 que tienen funciones especiales.
En particular nos interesa el bit PE(Protection Enable) del CR0, que nos permite pasar a modo protegido.

El tema es que para que el modo protegido siga funcionando bien hay que configurar un monton de cosas.
Entre estas cosas, los registros selectores del segmento, que ya no se usan en forma de suma desplazada de Selector y Offset para obtener la direccion lineal.
Al activar la proteccion necesita alcanzar direcciones lineales mas grandes, por eso, en el modo protegido, 


Los registros selectores son (entre otros):

- CS
- SS
- DS
- ES

Es la parte del registro a la cual tengo acceso directamente con instrucciones.

Por ejemplo

	mov Ax, CS

Resulta que si entro en los registros, con el bochs, me muestra mas informacion al ver la informacion del CS. Son campos de bits, que se llaman descriptor del segmento. Tiene la configuracion para que al estar en modo protegido funcinen en………


El registro del segmento esta compuesto por el selector y el descriptor.
Queda configurado en valores que dice que los segmentos son de 64k.
Y voy a acceder al registro del segmento y voy a decir que ahora son todos de 4Gb.
De esta forma reconfiguro la unidad de segmentacion.


Todos los registros del procesador en realidad son registros de semgneto, de los cuales la parte visible es el selector.


Como entidad interna del procesador, los primeros 16 bits representan el selectorn CS…..


Una cosa es la arquitectura del lenguaje assembler y otra es la arquitectura del procesador. Podemos acceder a algunas cosas con el assembler, pero a otras solo podemos acceder con el simulador.


No existe un mov para tocar los descriptores del segmento, vamos a tener que usar mecanimos indirectos para poder cargar valores ahi.



Si el procesador puede manejar simultaneamente 8 selectores, por que voy a querer 8192 descriptores? Para poder tener varios segmentos de datos diferentes.

Por ejemplo, en la tabla de descriptores(que es una expresion de deseo, esta en memoria ram, no en el procesador) No es lo que el procesador esta usando.

El profesor dice:
"voy a definir un segmento para un programa", entonces va a ser un segmento de codigo, en los atributos le pongo "aca estoy describiendo un segmento en cierto lugar de la memoria que es para meter codigo". Y despues puedo tener 100 mas segmentos de codigo, pero lo que cambia entre uno y otro es el lugar en la memoria donde estan.

Por ejemplo son todos de 1MByte, reservo todos los segmentos y los pongo en lugares distintos de la memoria, cada segmento va a alojar sera un programa, de usuario por ejemplo. Y le asocio un segmento de datos a cada uno mas chiquito, por ahi de medio mega. Entonces tengo 100 de 1MByte y 100 de medio MByte, en total tengo 4GB. Y despues mi sistema operativo cuando quiera cargar una aplicacion que hace? En ese lugar de la memoria carga el programa, donde yo defini que estaba el segmento con un programa de usuario. Y luego le dice al procesador "usa ese descriptor" para que sea el programa en curso.
Procesador: De donde lo saco?
Sacalo de la tabla de descriptores que tenes ahi!

Entonces el procesador tiene que tener una forma de poder ubicar esa tabla, porque la va a estar usando, cada vez que yo le diga que tiene que cambiar de segemento.

El programa corre y dice "bueno, ahora CS hace un call o un jmp" va a ir a otro segmento o lugar del programa, es decir, va a hacer un call o jump intersegmento. Entonces va a cambiar el valor de CS y cuando cambia el valor de CS va a tener que seleccionar un valor distinto para poner aca. De donde va a sacar el valor nuevo? De la tabla de descriptores.

Pero para poder ubicar esa tabla, que puede estar ubicada en cualquier lugar de memoria, va a necesitar un registro mas:
El registro GDTR es un puntero que le dice al procesador donde esta la tabla.

Antes de pasar a modo protegido, para poder cargar los descriptores en el procesador, voy a tener que definir esa tabla de descriptores.

El procesador puede hacer referencia a 2 tipos de tablas de descriptores, no solo una. Es decir, no nos quedamos con 8192 descriptores, podemos tener mas.
Puede hacer referencia a 2 tablas de descriptores simultaneamente.

La G porque es global y la L porque es local
Global Descriptor Table Register (GDTR)
Local Descriptor Table Register (LDTR)

Por ahora no se explica cuando se usa la local y cuando la global. Por ahora usaremos la global siempre, salvo que indique lo contrario.

Ahora, ojo, porque la LDTR es un selector(por eso no se explica ahora), y si es un selector,quiere decir que tiene asociado un descriptor.

El GDTR no es un regitro selector, es un registro especial, es tan especial, que es de 48 bits, porque este registro se divide en 2 campos: base y tamaño.    y no tiene descriptor.
__Base - 32 bits:__ para poder ubicarl la tabla en cualquier lado de los 4gb, donde yo quiera.
__Tamaño - 16 bits:__ Para poder especificar un tamaño de tabla de hasta 64k, de ahi salen los  8192 descriptores que puede tener, porque cada descriptor es de 8 Bytes. Tamaño es el tamaño que va a tener la tabla, osea, yo puedo tener una tabla de descriptores de un solo descriptor, entonces en tamaño le pongo 8, porque un descriptor ocupa 8 Bytes. O puedo tener tamaño maximo que son 8192.

Pero, hay una limitacion en modo real.

En modo real, en principio (hasta donde nosotros sabemos), podemos mover de a 16 bits, __pero__ podemos usar registros de a 32 y hasta de 64 si el micro es de 64 bits en modo real, pero solamente podemos mover valores de ese tamaño entre registros.

Despues veremos como hacemos para cargar el registro GDTR, para operar con GDTR hay una instruccion especial.

Entonces, vamos sumando, tenemos:

1) definir la tabla
2) hacer que el registro GDTR apunte a la tabla, donde esta y cuanto mide, todo eso hay que decirle.
3) Hay que cargarle a esta tabla los valores corrector para despues poner en los descriptores de segmento del procesador.
Cargar bytes ya lo sabemos hacer, mover a memoria. El programa que copia, mueve a memoria, de memoria a memoria.

Nuestro programa tiene ahora que hacer una inicializacion mas exhaustiva.

Una vez que definimos las tablas, prendemos el modo protegido.
Una vez hecho eso, le decimos 
Le indicamos a cada descriptor un registro.



Indice me dice el numero de descriptor en la tabla de descriptores.

TI: Table indicator
0: tabla global: GDT
1: tabla local: LDT


en modo protegido, 8:0

RPL: niveles de privilegio de proteccion
0 es mas privilegiado, 3 es el menos privilegiado (4 posibles, 2 bits.)

Un programa que tenga tal privilegio, no puede acceder a un segmento de datos que tenga un nivel de privilegio mas alto.
segmento de pila tiene tal privilegio, etc..


Nosotros para aprender vamos a ponerle a todo el maximo privilegio (0)

Cuando ve el 8 lo interpeta como nivel de privilegio 0, tabla 0, la entrada 1.

1000

Agarra los 8 bytes que estan en la tabla y los pone el el registro del degmento.





El modo unreal es el modo real pero con acceso al mapa de 4GB. Para eso me paso a modo protegido, hago los cambio y vuelvo a modo real.


Cuando llego al modo unreal, al hacer la composicion con los registros como hacia antes, 



Si uso 4gb, como le cambie el tamaño, puedo hacer

mov [DS:0xFFFFFF], Ax

ahora me lo deja hacer.

Todos los registros selectores los apunto a cero, y despues todos los offset son de 32 bits.

Para hacer la direccion logica sigue haciendo el desplazamiento, pero uno es cero.

El ip sigue siendo de 16 bits, pero el modo unreal me permite copiar cosas mas alla del 1MB.



Los corchetes son para que sea un puntero a memoria, sino no anda.

Los registros para 32 bits son los mismos offsets de modo real, pero con la E antes, de extendido.


GDT: Global Descriptor Table


Yo tengo la tira de 8 Bytes que es un descriptor.

Descriptor (8)

La direccion base son 4 byte


Tannenbaum. Hizo un unix que corre en modo real, pero hace la proteccion de memoria por software, pero es muy hackeable.

Del 16 al 31 dejaron 2 bytes reservados para futuros usos. Por eso esta todo recortado


Para nuestros usos, el tamaño
Si el tamaño puede ser 4gb, como le digo 4gb con 20 bit.


El bit llamado G (Granularity) de los atributos indica en unidades de que esta expresado el tamaño, entonces si lo pongo en unidades mas grandes, los 20 bit me los pueden representar.

Si G=1 usa unidades de 4k. 4K * 1MB de direcciones me da los 4Gb.


Esta clase tiene explicado cada uno de los atributos del descriptor.

Con la presentacion podriamos inferir los valores numericos para que funciones cada ejemplo.



Es el capitulo 3a la parte de 32 bits.





PL: Privilege level (2 bits) nivel de privilegio de ese descriptor. El segmento que tenga ponemos siempre 0 para que coincida siempre.


Lo que dice 0 y 1 va como dice ahi.




Default/Big
Aca tenemos un problema

Los descriptores del segmento describen no solo donde empieza y el tamaño, sino tambien los atributos y me dice si son para cosa que funcionaran a 16 bit o en 32.

El codigo que se ejecuta en ese segmento es para 16 bit.


Mov ax, 0
el procesador ve A8

Pero no va a saber si tiene que mover algo a Ax o a eax.
Como lo sabe? Con el bit D.
Sin importar si esta en modo protegido o real.
Si el bit de atributo del descriptor esta en 16 bit, lo interpeta como que tiene que usar Ax.

Antes del A8 le va a meter un 66, si encuentra 66 y despues A8, entonces tiene que interpretar que tiene que invertir el significado del bit default.

Ve que esta en 0, pero como lo mande en 16 bits, ve que lo tiene que invertir.

El bit funciona con el prefijado

Ahi empieza otro problema, al pasar de modo real a modo protegido.

En el simulador se puede ver D con sreg.

Cuando pasamos a modo protegido empiezan los problemas porque por poner mal el puntero del GDT por ejemplo interpreta todo mal.

El modo protegido es muy laborioso.


66 y 67
Uno es para hacer referencia a posiciones de memoria



jmp inicio, lo interpreta dependiendo del prefijo.


El compilador tiene una directiva que se llama bits.

Podemos ponerle 
bits 16 o 
bits 32

esto es para el prefijado del compilador.

Por defalt esta en bits 16.

No hay correlacion entre:
bits 16 – D=0 o 
bits 32 – D=1
no es asi, la correlacion esta en la logica del programa.



Que hay que hacer en estas proximas 2 semanas?

Nos va a ir dando indicaciones para ir resolviendo el ejercicio 3.
Armar la GDT, armar los descriptores, etc.

El ya nos paso el codigo para activar el codigo A20.

Para poder acceder arriba del 1er MB tenemos que entrara en modo unreal.
La tarea es hacer todo eso.

Hay que aprender a cargar el GDTR leyendo el manual de intel.
Escribir los bytes en la memoria para que tengan el formato de un descriptor.



Nos cuenta el resto por mensaje.
