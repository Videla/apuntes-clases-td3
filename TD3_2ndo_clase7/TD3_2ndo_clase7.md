# Clase 7 19-09-2019

Nosotros lo unico que vamos a hacer con los socket es darle las indicaciones para que se conecte.

Vamos a proveer la direccion y el numero de puerto al que se tiene que conectar.

Lo mas generico y abarcativo es decir que Q es un proceso que corre en un sistema operativo y P es otro proceso que puede ser en otro sistema operativo y se pueden comunicar si es que cumplen estandar POSIX.

Lo que necesito para conectarme es en principio la direccion IP, por ejemplo:

IP:10.0.0.2

Las redes hacen que lleguen los paquetes de un protocolo de un punto al otro. En principio los 2 tienen que tener una direccion de red distinta.

El problema es que puede haber mas de un proceso por IP que quiera hacer uso de la red. El numero de puerto es para multiplexar el uso de una direccion IP.

Una vez el paquete llego a destino hay otro protocolo dentro de IP. Tenemos TCP y UDP. que tienen sus diferencias y usos.

Lo que el paquete le agrega al encabezado es el header o overhead.

Dentro el paquete IP tenemos el pquete TCP con su overhead
Tambien tiene otros valores que me ayudan.

Un IP puede transportar 64k.
En ethernet no entra todo el paquete IP, entonces ethernet la corta en pedazos.

De la misma forma, el TCP tiene dentro un numero de puerto origen y un numero de puerto destino. Lo usa para asociar el file descriptor de un proceso con el paquete.

NATPAT hace la traslacion de direcciones IP y la hace el router y se encarga de que me pueda comunicar con el servidor a travez de mi router.

Si me quiero comunicar directamente con una computadora en india hoy en dia ya no puedo hacerlo directamente, necesito hacerlo con una VPN, que es un tunel para comunicarme que genera como si fuera una red local.

Cada socket desconectado listo para usar tiene al menos direccion IP y numero de puerto.




El file descriptor sera tratado como si fuera un 

Puedo usar un socket para hacer una comunicacion entre procesos.

De hecho, el driver tiene la direccion IP, que es la del sistema dentro de la red que esta conectado. Y ademas tiene una direccion que es la loopback, IP: 127.0.0.1 es la loopback por defecto.

La IP hay que configurarla.

Tengo como una especie de get IP del sistema que me dice cual esta configurada. Por ejemplo, de donde saca la red destino cuando le digo anda a tal lugar.

Indirectamente cuando escribo www.afsdf.com a travez de el protocolo DNS puede consultar por nombre la IP de una 


Si en linux editamos el resolv.conf, que dice nameserver 172.20.10.1. Este archivo se genera solo con el router que le esta dando acceso a internet.


Lo importante que hay que entender es el numero ip y puerto origen y numero ip y puerto destino.


Lo que llegamos a ver la clase pasada fue:

- La funcion para reservar un socket se llama socket.

necesita la familia de protocolos y otros datos mas, porque protocolos de red hay muchos. Nosotros en particular usaremos IP. Y el tipo de protocolo de transporte que usare por IP para el numero de puerto.
Dijimos que vamos a usar en este caso TCP.

Hay unos include que hay que hacer.

``` c
#include <socket.h>
```

Despues habia que decirle el tipo de protocolo de transporte, TCP o UDP.

``` c
SOCK_DGRAM // TCP
```

o

``` c
SOCK_STREAM // UDP
```

Con esto dejamos reservado el numero de file descriptor para ese socket.

Esto no es lo mismo que un open, no esta listo aun para leer y escribir.
Ahora hay que asociarlo o vincularlo con:

``` c
bind()
```

Que va a permitir elegir la IP o numero de puerto origen para el socket que reserve

Aun no esta listo para usar.
Bind asocia mi file descriptor con mi IP y puerto origen

el remoto tendra un socket asociado con un file descriptor.



Si sere el que acepta o inicia las conexiones, tendre que definir los datos del destinatario.
Para conectar necesito IP y puerto para origen y para destino.

Si yo voy a preguntar a mi destino, voy a usar la funcion connect. Y en la funcion connect le tengo que pasar como parametro la direccion IP y numero de puerto de destino. Y esto hara que el driver de TCP IP del sistema arme el paquete y lo mande a la red. Cuando llegue al destino lo va a desarmar y lo va a entregar a quien corresponda.

Este programa ahora si va a poder ver lo que hay ahi dentro.


HTML es una manera de formatear el texto con tags por ejemplo 

``` html
<h1> HOLA </h1>
```

Si lo llamamos html el formato queda asociado al navegador.

hay tags mas complejos.

yo puedo abrir un tag que diga <javascript> entonces el navegador dira: lo que viene a continuacion es un script.

La version de HTML y todo lo que puedo hacer con el en el navegador es una caracteristica del navegador.

El servidor al que me conecto va a poder interpretar todos los tags que le tiren. Si es un servidor web manda un archivo de texto y dentro esta esto.

Despues tenemos algo para que no nos puedan sniffear los paquetes.

HTML va en texto plano.
Es la sintaxis que me ayuda a interpretar esto. El protocolo que se usa para transportar html a traves de la red es HTTP (Hiper Text Transfer Protocol).

En el modelo de capas teniamos las cosas de arriba, una de esas capas es HTTP.

HTTP es te transporto el texto como esta. Si quiero que se encripte. El protocolo es HTTPS. Lo que hace es lo mismo que HTTP, pero antes de meter los paquetes dentro del transporte los encripta.
Cuando llega del otro lado, el hhtp del cliente lo desencripta y lee el html.

En ese estado el HTML es estatico, lo interesante es que por ejemplo al tipo le pida que consulte una base de datos o la hora. Tiene que pasar algo que dentro diga la hora actual. Esa pagina dejo de ser estatica y paso a ser dinamica. Voy a generar dinamicamente con la hora del momento la hora y embederla en html. Como lo hago? con CGI.

El servidor tiene guardado en algun lado el texto fijo. Y despues tendra una forma de poder generar el texto con la hora del momento.

Eso se usa con PHP, JAVA, etc.

En el practico que resolveremos tenemos que mostrar informacion con este formato.

En el dispositivo que diseñamos leemos un acelerometro por ejemplo. Y en pantalla tenemos que mostrar ese valor en G (el valor de la gravedad terrestre) para cada eje. tenemos que desarrollar el driver del acelerometro y un driver que arme el servidor web.

Vamos a tener que hacer 2 cosas, el servidor y el cliente.

Y el servidor que genere los valores en el momento en que se pida la consulta.


vamos a programar primero el proceso que abra el socket. En esto nos va a ayudar el netcat.
Que sirve para iniciar una conexion o escuchar y ver que pasa.
Lo va a mostrar con 2 shell. En uno pondremos una instancia de netcat escuchando conexiones t en otro el netcat escribir.

Para poder usar el programa cliente hay que usar el programa servidor. Lo 2ndo que mostrara es un servidor minimo que nos muestra eso, "Hola que tal, la hora y listo."

``` bash
cat http_argento.sh
```

``` bash
#!/bin/bash
while true
do
    {
        echo -e `HTTP/1.1 200 OK\r\n`;
        echo -e `<html><body>`;
        echo -e `meta http-equiv="refresh" content="5"`
        echo -e `<h2>HOLA</h2>`
        echo -e `<h3>Server HTTP argento</h3>`
        echo -e `<h3>La hora actual es:</h3>`

        date

        echo -e `</body></html>`

    }   | nc -1 8080
```

Aca estamos usando como lenguage de CGI el lenguaje bash

La hora se va cambiando El html que le tiramos es un HTML que hace meta refresh
Si le saco el meta http-equiv refresh no hace mas refresh.

El comando Get sin parametros adicionales lo que dice es: necesito la pagina de inicio.
Y dice, yo quiero esto y te digo, estoy conectando aca. La conexion tiene que permanecer abierta ```keep-alive```.

Hay informacion de el sistema operativo.

El netcat muestra todo lo que recibe en pantalla.

Lo que le contesta el servidor es una pagina en Html, el navegador las interpreta y las muestra en pantalla.

```netcat -l``` es escucha, sino ponemos nada es tcp, si queremos hacer udp, hacemos ```-u```


Despues del recreo nos va a dar el primer problema que tenemos que resolver.

La funcion para enviar es ```send```. Una vez que conecto podemos empezar a enviar.
El que va a esperar va a usar ```recv``` pero ahora no nos interesa porque lo va a hacer el netcat.

Desde el netcat podemos tipear texto para contestarle.
El otro va a tener que hacer recv y mostrarlo en pantalla.

``` bash
nc localhost 6500
HOLA
```

Tenemos que hacer un programa en C que haga la parte de enviar.

Se queda esperando el recv.

El host byte order esta definido por la endianness del procesador

Vamos a definir para hacer las conversiones:

``` c
hton() //host to network
ntoh() //network to host
```

Para poner el __numero de puerto__ e __ip__ hay que pasarlos por

``` c
a = hton(8080)
```

el valor ```a``` lo guardamos en la estructura.

Me garantiza poner en el orden correcto para la red en el orden correcto del procesador 

Si mi procesador tiene el endianness de la red no hace nada. Si esta al revez lo da vuelta.

En realidad hton no existe como tal, son 4.

``` c
htons (short)
htonl (long)
ntohs (short)
ntohl (long)
```

Las short son para dar vuelta los 16 bit y las long para los 32 bit.

El que recibe tiene que aplicar ntohs o ntohl.

Cuando recibe los paqutes, quiere ver desde que ip se esta conectando.
En la estructura hay que poner ip y puerto destino.


Podemos probar esto con la beagleboard.
Bajar el debian para la pagina de la beagle.
Hay 2 versiones, la que trae todo, que ocupa mucho espacio y otra mas.
Descargar la otra, que es mas liviana y hace todo lo que hace falta.

Puede ser un buffer distinto para recibir y para transmitir, no es ningun problema.

Tenemos que escribir el driver del sensor para linux y el driver para el servidor.

Ahora vamos a aprender a hacer el servidor que va a escuchar conexiones.

Para poder ver la informacion en todos los servidores.

Esas son las conexiones de diseño. Como hacemos para atender los requerimientos de al menos un cliente. Despues vemos como hacemos para poder atender a mas de un cliente en base a forks o threads.

Hay que hacer el socket


hay un comando de shell que se llama __netstat__ y puedo filtrar por protocolo


Los puertos bien conocidos(wkp: well known ports) estan en:

```
http 80
mail 25
telnet 23
ssh 22
miservicio 6500
```

``` c
int s; //socket para escuchar
int sc; //socket conectado
s=socket(...)

socketadmin . origen -> el puerto que escucha( ej 6500)
                    -> origen.sin_port = 6500

bind(s)

-1 = accept(s, puntero a los datos del que conecta, tamaño, )

// si accept devuelve mayor que 0.
// nos devuelve otro socket.
// cuando se destranca la funcio, el socket inicial sigue en escucha. 
// Y el sc se queda conectado al cliente.
// Es conveniente que cada socket sea manejado por uno o mas threads para que sea concurrente.
// manejarlo con un solo thread hace que uno de los 2 sockets sea innecesario.
// el s no queda conectado
//
```

A un thread le paso todos los valores que quiera a traves de un puntero a estructura.

si suponemos que:
s es el file descriptor 3 y
sc es el file descriptor 4.

si se siguen sumando clientes, a cada cliente le va a asignar un nuevo socket con un nuevo file descriptor y cada thread se queda hablando con su cliente a traves de un file descriptor.

Explico el problema (C)

por ahora sin crear un thread.
