// Para correr este programa primero debe correrse el comando: nc -l 6500 en otro teminal
// Este es el codigo del servidor y esta basado en el codigo de: https://www.geeksforgeeks.org/socket-programming-cc/

#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>

#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#define MI_PORT 0        // Si le paso el parametro 0, me asigna uno
#define PUERTO_DEST 6500 // Si le paso el parametro 0, me asigna uno

void myBind(int mysocket, struct sockaddr_in origen)
{
  if (bind(mysocket, (struct sockaddr *)&origen, sizeof origen) < 0)
  {
    perror("Bind failed. Error: ");
    exit(EXIT_FAILURE);
  }
  return;
}

void myConnect(int mysocket, struct sockaddr_in destino)
{
  if (connect(mysocket, (struct sockaddr *)&destino, sizeof destino) < 0)
  {
    perror("Connect failed. Error: ");
    exit(EXIT_FAILURE);
  }
  return;
}

int main(void)
{
  int mysocket = 0;
  struct sockaddr_in origen, destino;
  char buffer_recv[1024] = {0};
  char buffer_send[1024] = {0};

  if ((mysocket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    perror("\n Socket creation error: \n");
    exit(EXIT_FAILURE);
  }

  origen.sin_family = PF_INET;
  origen.sin_port = htons(MI_PORT);
  origen.sin_addr.s_addr = inet_addr("127.0.0.1");

  memset(destino.sin_zero, '\0', sizeof destino.sin_zero);

  destino.sin_family = PF_INET;
  destino.sin_port = htons(PUERTO_DEST);
  destino.sin_addr.s_addr = inet_addr("127.0.0.1");

  myBind(mysocket, origen);
  myConnect(mysocket, destino);

  recv(mysocket, buffer_recv, 30, 0);
  printf("Mensaje recibido: %s\n", buffer_recv);

  sprintf(buffer_send, "HOLA COMO TE VA\n");
  send(mysocket, buffer_send, strlen(buffer_send), 0);
  printf("Mensaje enviado: %s\n", buffer_send);

  shutdown(mysocket, SHUT_RDWR);
  return 0;
}