// Esta basado en el ejemplo de bugs y lucas visto en clase

#define NOMBRE_ARCHIVO "archivoide.txt"
#define TAMANIO_MAX_MSJ_PIPE 200

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

pthread_mutex_t mutex_lock;

void *thread_escribeArchivo(void *ptr)
{
  char *nombreArchivo;
  FILE *filePointer;
  char id[10];
  char buff[200];
  int pipeFileDescriptor;

  pthread_mutex_lock(&mutex_lock);

  sprintf(id, "%d", (unsigned int)pthread_self());
  nombreArchivo = (char *)ptr;
  printf("escribeArchivo: %s: %s\n", id, nombreArchivo);

  if ((pipeFileDescriptor = open("/tmp/myfifo", O_RDONLY)) == -1)
  {
    perror("Error en open");
    exit(EXIT_FAILURE);
  }

  read(pipeFileDescriptor, buff, TAMANIO_MAX_MSJ_PIPE);
  printf("escribeArchivo: Mensaje Recibido: %s", buff);

  close(pipeFileDescriptor);
  unlink("/tmp/myfifo");

  if ((filePointer = fopen(nombreArchivo, "w")) == (FILE *)NULL)
  {
    perror("Error opening file");
    exit(EXIT_FAILURE);
  }

  printf("escribeArchivo: Escribiendo en el archivo...\n");
  fprintf(filePointer, "%s", buff);

  fclose(filePointer);
  pthread_mutex_unlock(&mutex_lock);

  return NULL;
}

void *thread_leeArchivoImprime(void *ptr)
{
  char *nombreArchivo;
  FILE *filePointer;
  char id[10];
  char *str;

  pthread_mutex_lock(&mutex_lock);

  sprintf(id, "%d", (unsigned int)pthread_self());
  nombreArchivo = (char *)ptr;
  printf("leeArchivoImprime: %s: %s\n", id, nombreArchivo);

  if ((filePointer = fopen(nombreArchivo, "r")) == (FILE *)NULL)
  {
    perror("Error opening file");
    exit(EXIT_FAILURE);
  }

  printf("leeArchivoImprime: Leyendo del archivo...\n");
  char c;
  while ((c = fgetc(filePointer)) != EOF)
  {
    fprintf(stdout, "%c", c);
  }

  fclose(filePointer);
  pthread_mutex_unlock(&mutex_lock);

  return NULL;
}

int main(void)
{
  pthread_t escribeArchivo;
  pthread_t leeArchivoImprime;
  int iret1, iret2;
  const char *nombreArchivo = NOMBRE_ARCHIVO;

  if (pthread_mutex_init(&mutex_lock, NULL) != 0)
  {
    printf("Mutex init failed\n");
    exit(EXIT_FAILURE);
  }

  iret1 = pthread_create(&escribeArchivo, NULL, thread_escribeArchivo, (void *)nombreArchivo);
  if (iret1)
  {
    fprintf(stderr, "main: Error - pthread_create() return code: %d\n", iret1);
    exit(EXIT_FAILURE);
  }

  iret2 = pthread_create(&leeArchivoImprime, NULL, thread_leeArchivoImprime, (void *)nombreArchivo);
  if (iret2)
  {
    fprintf(stderr, "main: Error - pthread_create() return code: %d\n", iret2);
    exit(EXIT_FAILURE);
  }

  char input_string[TAMANIO_MAX_MSJ_PIPE];
  int pipeFileDescriptor;

  if (mknod("/tmp/myfifo", S_IFIFO | 0666, 0) < 0)
  {
    perror("Error en mknod");
    exit(EXIT_FAILURE);
  }

  if ((pipeFileDescriptor = open("/tmp/myfifo", O_WRONLY)) == -1)
  {
    perror("Error en Open");
    exit(EXIT_FAILURE);
  }

  printf("main: Escriba su mensaje: ");
  fgets(input_string, TAMANIO_MAX_MSJ_PIPE, stdin);
  printf("main: Se ingreso el texto: %s", input_string);

  write(pipeFileDescriptor, input_string, strlen(input_string) + 1);

  close(pipeFileDescriptor);

  printf("main: pthread_create() para thread 'escribeArchivo' retorna: %d\n", iret1);
  printf("main: pthread_create() para thread 'leeArchivoImprime' retorna: %d\n", iret2);

  printf("main: Ahora hago join de los thread\n");
  pthread_join(escribeArchivo, NULL);
  pthread_join(leeArchivoImprime, NULL);

  if (pthread_mutex_destroy(&mutex_lock) != 0)
  {
    printf("\nmutex destroy failed\n");
    exit(EXIT_FAILURE);
  }

  printf("main: ahora si, terminando ejecucion.\n");
  exit(EXIT_SUCCESS);
}
