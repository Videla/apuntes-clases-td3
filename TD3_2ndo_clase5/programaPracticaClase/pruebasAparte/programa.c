// Mejorar para que se pueda seleccionar el archivo desde linea de comandos con | o con <

#define NOMBRE_ARCHIVO "archivito.txt"

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
  const char *nombreArchivo = NOMBRE_ARCHIVO;
  FILE *filePointer;

  if ((filePointer = fopen(nombreArchivo, "r")) == (FILE *)NULL) // Se tomo desde: https://www.linuxquestions.org/questions/programming-9/fgets-and-buffer-overflow-4175467362/
  {
    perror("Error opening file");
    exit(EXIT_FAILURE);
  }

  printf("%s: \n", nombreArchivo);

  char c;
  while ((c = fgetc(filePointer)) != EOF)
  {
    fprintf(stdout, "%c", c);
  }
  fclose(filePointer);

  return (EXIT_SUCCESS);
}
