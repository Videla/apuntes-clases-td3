// Mejorado a partir de https://stackoverflow.com/questions/174531/how-to-read-the-content-of-a-file-to-a-string-in-c

#define NOMBRE_ARCHIVO "archivito.txt"

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
  const char *nombreArchivo = NOMBRE_ARCHIVO;

  char *buffer = 0;
  long length;

  FILE *filePointer;

  if ((filePointer = fopen(nombreArchivo, "r")) == (FILE *)NULL)
  {
    perror("Error opening file");
    exit(EXIT_FAILURE);
  }

  printf("%s: \n", nombreArchivo);

  fseek(filePointer, 0, SEEK_END);
  length = ftell(filePointer);
  fseek(filePointer, 0, SEEK_SET);
  buffer = malloc(length);

  if (buffer)
  {
    fread(buffer, 1, length, filePointer);
  }
  fclose(filePointer);

  if (buffer)
  {
    printf(buffer)
  }

  return (EXIT_SUCCESS);
}
