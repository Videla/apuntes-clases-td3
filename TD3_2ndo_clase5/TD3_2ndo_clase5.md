# Clase 5 05-09-2019

formas de conectar procesos(IPC):

- Señal
- Pipe
- FIFO
- Cola
- MEM.COMP

Solo se necesita parentesco para pipes.

COW

LWP

Thread

Task

## Arranaca la clase

## Hilos

Cuando queria hacer un multiprocesamiento, lo que hacia era que un proceso cree otro. Se pueden sincronizar y pasar mensajes. Eso es comunicacion entre procesos.

Ahora yo tengo un proceso que va a correr un fragmento de codigo, un main. Y lo que hare es que el main en un momento llame a una funcion. Que la tiene declarada como una funcion global de su programa. Pero, cuando invoca la funcion, no retorna y sigue como haria nomalmente. Lo que haremos ahora sera que esta funcion se ejecute como otro proceso dentro del proceso inicial.

Cuando llame a la funcion, copia a la funcion y genera una instancia de la funcion dentro del proceso.

Y cuando genera esa instancia, corren los dos procesos "al mismo tiempo"

Puede pasar que se llame mas de una vez a la funcion y se generen 2 instancias de la funcion.

Es menos carga para el sistema operativo, en cuanto a estructura de datos. No tengo que crear una nueva estructura de datos.

Para pasar datos entre funciones puedo usar una variable global.

Facilita la comunicacion a nivel de variables globales.

Si el proceso quiere acceder al archivo, las funciones pueden acceder tambien. Me facilita la interaccion entre los hilos.

Al hablar de threads siempre habra un _main thread_, que es la funcion main del programa. que es el encargado de generar otra funciones hilo.

Tambien un hilo puede generar otro hilo.

Para poder hacer esto, voy a necesitar una biblioteca de funciones especial para generar hilos. Para crearlos y destruirlos.

Como me ayuda el hardware para generar threads.

Si tengo un procesador con un nucleo, nadie puede tener 2 al mismo tiempo.
Opcion 1...
Un solo

Opcion 2, tengo un CPU que tiene HyperThreading. Que se vera mejor en micro arquitectura. Si es hiperthreading divide instrucciones de cada thread. Divide intrucciones de forma tal que dice, estas instrucciones son del thread 1, y estas instruccinoes son del thread 2.

Un nucleo hyperthreading puede ejecutar simultaneamente instruccinoes de un programa que tenga 2 threads.

Despues puedo tener un procesador con 2 nucleos donde cada nucleo es monothread.

Y despues tengo 2 cores multithread.

Hoy en dia los nucleos son al menos 4 threads cada nucleo.

Cuando tengo esto corriendo el SO se encarga de despacharlo a los nucleos que corresponda con el hyperthreading que corresponda.

Si tengo un procesador con 2 nucleos y 8 threads en total. Que hace el SO? Le tira el proceso a un nucleo.
Un proceso no puede estar en 2 nucleos al mismo tiempo.

No puede procesar un proceso en un nucleo y otro proceso en otro nucleo.

2 procesos pueden operar sobre una variable al mismo tiempo, eso es un recurso critico, entonces como se hace para cuidar que 2 procesos no entren a una variable al mismo tiempo?

Tengo que usar semafors o flags.

Como una bandera con una manijita. Subir la banderita lleva tiempo.

Estan andando los 5 procesos al mismo tiempo y el main dice voy a tener que operar sobre la variable A.

En el camino hasta que sube la bandera puede otro hilo querer acceder a la variable A.

Entonces, ni bien el programa empieza a girar, se bloquea el acceso.

Todos antes de tocar la varible A, tienen que ver el estado de la banderita.

El main cumple con la regla, pero los thread no. Entonces el main antes de acceder levanta la bandera. Pero los procesos no.
Uno escribe el programa y hay que preocuparese de que en cada parte del programa que se use la variables se toque la banderita.

Si no lo programo teniendo en cuenta esto voy a tener problemas.

Todos los procesadores que pueden hacer multitarea, hoy en dia todos, provee en assembler una instruccion test and set. Que me permite ver el valor de una variable y dependiendo del valor de la variable ponerla en 1.

Voy a la memoria, busco el bit. Me traigo cuanto vale la variable.
La operacion test and set se llama operacion atomica, porque no se puede dividir.

todos los procesadores que menejan multitarea tienen instruccion atomica de forma tal que el scheduler no pueda interrumpir. El scheduler espera que termine la instruccion en curso y despues hace lo que tenga que hacer.

Como lo hago a nivel sistema operativo? Porque a nivel sistema operativo ejecuto instruccines en lenguajes de mas alto nivel.

Si quiero acceder a la variable A, la bandera va a ser la variable booleana B.

``` c
if(b){
    accedo a A
} else {
    espero
}
```

El problema de hacer esto es que todo esto son muchas instruccinoes no atomicas.

Mientras pregunto cuando vale B otro hace lo mismo y ambos ven que esta disponible y ambos acceden y hay problemas.

Uno de los mecanismos que hay para implementar operaciones atomicas es el __semaforo__.

Si hago que b sea un semaforo, el sistema operativo me garantiza que sea una instruccion atómica. Cuando ambos llaman a consultar el semaforo, como el semaforo es una system call, ambos thread llaman a la system call del sistema y en ese momento se paran los 2 porque ambos hacen una llamada. Y el kernel es uno para todos. Y el kernel va a resolver quien llega primero.

Un semaforo a nivel kernel es test and set.
Si quisiera implementar un semaforo con otras instrucciones me lleva mas de una instruccion y ese bloque de codigo deja de ser atomico. todas las instrucciones son atomicas.

A nivel unix, lo que tengo es mutex, que es una variable especial de tipo mutex en la que tengo funciones especiales para operar. El mutex es un semaforo para threads.

Puedo usar un semaforo con threads. Pero la variable tipo mutex es como un semaforo para procesos, pero para los threads. Esta mas orientado el mutex que los semaforos para los threds.

Windows en lugar de fork tiene ```create process```, tiene ```create thread``` y tiene ```crytical section``` que sirve para lo mismo que los mutex en linux. Mientras yo modifico ahi, nadie mas puede hacer lo mismo.

Cuando compilo el programa que usa threads, tengo que usar una biblioteca de funciones que no se agrega automcaticamente.

Ahora para compilar y agregar la libreria, tendre que agregarlo al compilar con el ```-l```

``` bash
gcc test.c -o test.exe -lpthread
```

-l va junto al nombre de la biblioteca.

y a su vez cuando escribo el programa.

``` c
#include <stdlib.h>
#include <pthreads.>
```

Ahora, de donde viene la p?
de Posix. todas las system call.

Posix es lo que vendrian a ser las normas ISO o IRAM pero para las System call.

(POSIX) Sistemas operativos portables tipo unix.

eNo dice como si implementan, sino como se llaman y cual es el prototipo de las funciones.

Entonces si tengo la funcion open, que lleva 3 parametros y devuelve un entero. Posix la define asi en su estandar.

```
i <- open(a, b, c)
```

Todas las implementaciones de Unix tienen que implementarlo de esta forma.

Esto me garantiza que si agarro el fuente que escribi para linux, el cual escribi con la sintaxis posix compatible y lo llevo a la MAC(MAC cumple con posix) y lo compilo, va a compilar y funcionar bien.

No todos los Linux cumplen el estandar posix.

En el man dice si la funcion cumple con posix.

La idea al programar es usar todas funciones que sean posix compliant de forma tal que si programamos en la PC o en la beagle pueda ser compatible. y el codigo tenga portabilidad.

En los unix se llama sin la p de pthread, pero cumple posix y funciona.

La portabilidad del fuente esta garantizada.

como crear un thread?

todas las funcinoes de la bilbioteca empiezan con pthread, la de creacion se llama create.

```
0 o distinto de 0 <- pthread_create(-> threadId, -> atributo, -> funcion, -> parametros)
```

El threadId seria una especie de PID para el thread

0: OK
!= de 0: error(lo escribe en errno)

Todos los parametros son punteros, si no le paso todos los parametros, le paso un puntero a null y son los que usaremos por defecto.

El puntero a funcion es el nombre de la funcion tal cual la definimos.

Cuando usamos señales, cuando instalo el handler de la señal, la funcion instraladora uno de sus parametros era el nombre de la funcion....

Si quiero crear una isntancia de la funcion como thread, le pongo el nombre en la funcion create de arriba.

Y como la funcion necesita 3 parametros, no se los puedo pasar en la parte de la funcion, porque eso es un punero a funcion. Ahora veremos en la practica como se hace.

En principio a la funcion no la puedo definir asi.

func(a, b, c)

lo que haremos es darle el puntero a la funcion.

Creamos una estuctura, una variable puntero a la estuctura y a la funcion y al thread le pasamos el puntero a la estructura donde estan cargados los parametros. Los tiene que desenroscar del puntero a la estructura.

Si creo una funcion suma, y una funcion divide que reciben los mismos parametros y hago suma, divide y suma, necesito usar mutex para que esten sincronizados correctamente.




En el programa de ejemplo, antes de que se pueda ejecutar

El thread que nace que hace? Pone el mensaje en pantalla. El que crea se va a dormier entonces aparece lo que se acaba de crear. Despues de dormir se crea el 2ndo thread. Ahi que no duerme le gana el MAIN a Lucas, pero podria no ser asi y que gane Lucas.

Podria pasar que un printf se corte a la mitad.

Edsger Dijkstra. Fue parte de los que inventaron los semaforos, los mutex, etc. Y el planteo el problema de la mesa redonda.

---
---
---

Mutex viene de Mutual Exclusion.

Tenemos una funcion para bloquear el mutex y apropiarmelo
Cuando termino de usarlo lo desbloqueo.
Lo que suele pasar es que el programa principal va a intentar acceder al recurso asi no se van entorpeciendo los unos a los otros.
Es como un malloc y un free de un pedazo de memoria.

- inic
- lock
- unlock
- destroy

Todas las funciones que usan thread empezaban con pthread_

Como son todas de pthread todas empezaran con pthread.
Como trabajaremos sobre mutex, agregamos mutex. Y luego el nombre de la funcion

``` c
pthread_mutex_init();
pthread_mutex_lock();
pthread_mutex_unlock();
pthread_mutex_destroy();
```

El uso de mutex es mucho mas facil que el uso de semaforos con procesos.

Si los procesos llegan al m
No nos tenemos que preocupar por cual llega primero a bloquear el mutex, eso lo resuelve el kernel.

Tengo 2 threads.

Una vez el mutex unlock corre en uno de los procesos, ese thread sigue corriendo y el otro proceso que esperaba el recurso va a poder correr.

La forma de manejo del mutex escapa al alcance de la materia.

Que pasa si hay 3 threads que llegan al mismo tiempo y todos llegan al mismo tiempo a ejecutar mutex_lock.

Por lo general el mutex lock y unlock estan dentro de un loop.


Hay una variante a esto, una funcion mas, que funciona de la siguiente forma.

Puede pasar que en la logica del programa y por el tipo de problema sea de la siguiente forma.

Si esta libre el thread, lo tomo. Y si no esta libre me voy a hacer otra cosa y vuelvo despues. Pero, como mutex es una operacoin atomica. Como hago para preguntar si esta libre sin bloquearlo? Hay una opcion intermedia.

```
pthread_mutex_try_to_lock
```

Esta funcion devuelve un valor porque no es bloqueante, nunca bloquea, este ocupado el mutex o no.
Si devuelve OK = true, significa que pudo tomar el recurso. Si devuelve distinto de ok, significa que el mutex lo tiene otro y el recurso estaba ocupado. Pero no me bloquea, entonces pregunto. Si estaba libre, hago lo que tenia que hacer con el recurso. Sino, hago otra cosa. Es como en digitales 2 que se ponia if para ver si estaba habilitado el semaforo.

Ahora veremos la sintaxis de las funciones.
tipos de datos asociados:

```
pthread_mutex_t: Es el tipo de dato que identifica a la variable tipo mutex.
pthread_mutexattr_t a: Son los atributos que le pasaremos. Por defecto le pasamos NULL al inicializar el mutex.
```

-> significa puntero, al pasarlo como argumento hay que poner &

``` c
pthread_mutex_init(-> mutex, -> atributos);
    // devuelve un descriptor de mutex o OK
pthread_mutex_lock(-> mutex);
    // devuelve falla o ok
pthread_mutex_unlock(-> mutex);
pthread_mutex_destroy(-> mutex);
```

Puede que este instalada la biblioteca, tenga los includes, pero .

En los atributos esta al valor inicial al crear el mutex.
Todas las funciones de la bilioteca pthread devuelven 0 si funciono bien y devuelven el codigo de falla si funciono mal. No usan errno.
PAra ver el codigo de error hay que ver el manual de la funcion.
Hay una cantidad maxima de mutex que se pueden crear en el sistema.
Un codigo de error puede ser que ya no hay lugar para crear mutexes en el sistema.

--

## Parte practica

En la parte practica haremos un programa que dentro tenga 2 funciones leer y escribir.
Vamos a tener que crear un thread con una funcion leer y otro con una funcion escribir.
El parametro que el main le va a pasar al crearlos es el nombre de un archivo.
Que va a hacer el mutex escribir?
El main thread despues de crear los threads sera hacer scanf de un string. Con el mecanismo que queramos, un buffer en memoria compartida(malloc), un buffer string. El thread escribir esta esperando constantemente de la forma mas optima posible que el main thread le pase el string, cuando se lo pase, tiene que acceder al archivo en disco. Cuando le llega el string desde el scanf del main thread.

Y el thread leer tiene que acceder al archivo y leerlo, e imprimirlo en pantalla.
Para relacionar a los 2 threads hijos, hay que usar un mutex que restrinja el recurso de la memoria.

Va a mandar el enunciado en el Git del curso.
el programa no termina nunca.
