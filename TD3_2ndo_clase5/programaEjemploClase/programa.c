#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

void *thread_imprime(void *ptr); //declaro la funcion thread
//La funcion que llamare es una sola, pero la llamare con parametros diferentes. Generare 2 threads de la funcion imprime y a cada thread le paso arguemtnso distintos.

int main(void)
{
  pthread_t bugs, lucas; //para guardar identificadores de threads
  const char *mensaje1 = "Temporada de pato!";
  const char *mensaje2 = "Temporada de conejo!";
  int iret1, iret2;
  char id[10] = "MAIN"; //para imprimir el thread ID
                        // creacion de threads independientes.

  iret1 = pthread_create(&bugs, NULL, thread_imprime, (void *)mensaje1);
  if (iret1)
  {
    fprintf(stderr, "%s: Error - pthread_create() return code: %d\n", id, iret1);
    exit(EXIT_FAILURE); //exit sale del programa, apaga todos los threads y muestra lo que tiene entre parentesis. Termina el proceso y todos los hilos.
    //Si pones un exit en un hilo termina el proceso, para temrinar un hilo hay que poner pthread_exit o algo asi
  }

  sleep(2); //lo aguantamos un toque para ver los mensajes bien

  iret2 = pthread_create(&lucas, NULL, thread_imprime, (void *)mensaje2);
  if (iret2)
  {
    fprintf(stderr, "%s: Error - pthread_create() return code: %d\n", id, iret2);
    exit(EXIT_FAILURE);
  }

  printf("%s: pthread_create() para thread 'Bugs' retorna: %d\n", id, iret1);
  printf("%s: pthread_create() para thread 'Lucas' retorna: %d\n", id, iret2);

  printf("%s: Aguradando que terminen de pelear Bugs y Lucas...\n", id);

  // Espera a que terminene todos los threads antes de continuar el programa principal, sino se corre
  // el riesgo de terminal el main() antes de que termine algun thread, esto lo cortaria abruptanente
  // ya que al morir el proceso que contiene los threads muenen todos ellos.

  pthread_join(bugs, NULL);
  pthread_join(lucas, NULL);

  printf("%s: ahora si, terminando ejecucion. \n", id);

  exit(EXIT_SUCCESS);
}

void *thread_imprime(void *ptr)
{
  char *mens;
  char id[10];                                     //para imprimir el thread Id
  sprintf(id, "%d", (unsigned int)pthread_self()); //es el equivalente al getpid

  mens = (char *)ptr; //lo casteamos

  printf("%s: %s \n", id, mens);
  sleep(2); //para ver que terminan a tiempos diferentes

  return NULL;

  // // Si la funcion tuviera que devolver un entero, como hago?
  // // Defino
  // int *p;
  // int a;// Si la funcion tuviera que devolver un entero, como hago?
  // // Defino
  // int *p;
  // int a;
  // // opero con a, le doy un valor, despues le digo que p apunte a la variable a y hago return p
  // return (void *)p;
  // // La variable thread id queda apuntada a lo que devuelve la funcion.

  // // opero con a, le doy un valor, despues le digo que p apunte a la variable a y hago return p
  // return (void *)p;
  // // La variable thread id queda apuntada a lo que devuelve la funcion.
  // // No se usa mucho esto porque se hace para operar en memoria, o trabajar con archivos en disco, etc.
}