# Clase 11

Proteccion

El selector nulo de la GDT es la entrada de la GDT que debe tener todos ceros.
Los primeros 8 Bytes, tienen que estar todos en 0, sino es una GDT invalida.

Se usar
xor ax, ax
en lugar de
mov ax, 0
porque la ultima ocupa 1 Byte, en cambio la otra ocupa 3 o 5.

Los anillos se ubican en el anillo correspondiente al campo DPL de su descriptor.

PL:00 Kernel
PL:01 Servicios: la biblioteca standar de C stdlib o stdio

PL:03 printf que hace un usuario va a invocar funciones que tienen mayor privilegio

Hacemos una representacion de bandas


-------------------------------
PL: 0
-------------------------------

-------------------------------

-------------------------------
PL:3
-------------------------------

Intel propone que en los anillos 1 y 2 haya servicios del S.O.
Pero en la practica los servicios del S.O. se colocan en el anillo 0 junto al kernel.


El procesador primero chequea todo lo que tiene que ver consegmentos y despues lo que tiene que ver con paginas.

Nos podemos olvidar del dolor de cabeza del cambio de pilas y del cambio de páginas.

Los codifica al revez, mayor numero, mayor privilegio.
0 usuario       3 segmentacion (CPL)
1 supervisor    0, 1 y 2 segmentacion (CPL)

El bit 16 de CR0, se utiliza bajo el nombre WP y si esta habilitado no puede escribir en paginas protegidas por el procesador.

Proteccion de segmentos es la int 13
proteccion de paginas salta Page fault.

Para que no se pueda ejecutar codigo en paginas de datos hay una funcionalidad opcional de PAE que hace que salte page fault cuando intente ejecutar codigo en paginas con execute disable habilitado. .

Diferencia entre abi de 32 y de 64 uno usa registros y la pila y el otro usa solo registros.

pag 44 erratas si debia decir sin

System call es analogo a la puerta de llamadas.

Puerta de llamada no se usa en sistemas operativos, se usa interrupciones.


0x7EFA000 son direcciones lineales. Se sabe que es lineal porque dice que los segmentos son flat.


Dentro del TLB tenemos

PDT y PTE de 10 bits cada uno.

Y despues tenemos 20 bits de la direccion traducida.

Despues tiene atributos y bits de control.


todo lo que termina en 000 esta alineado al menos en 1k.


