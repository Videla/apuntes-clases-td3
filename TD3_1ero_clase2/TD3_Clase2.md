# TD3 Clase 2

Bochs pronunciado BOX es una maquina virtual que tiene como diferencia y ventaja que permite debuggear el codigo a nivel de assembler.

Simula la circuiteria de una compu y permite ver las instrucciones que esta corriendo en ese momento.  
Puedo ver los registros del micro, la memoria, etc.

Tiene 2 ventanas, una donde se ve el escritorio (display) y en la otra la consola. La que se ve el escritorio es la que se puede congelar.  
Tiene un boton para apagar.

Puedo insertarle un CD

Tiene un archivo de configuracion de texto plano.  
Se le dice que procesador va a emular, velocidad de simulacion, instrucciones por segundo.  
El limite es lo que de la maquina fisica.

Con Cntrl+alt despega el mouse del display y se puede llevar a la consola, etc.  
.  
.  
El objetivo de hoy es escribir un programa de inicio, donde nos ponemos en el lugar del diseñador de ese sistema.  
La simulacion empieza encendiendo el sistema.  
Tenemos que seguir la secuencia de lo que hace el procesador cuando inicia.

Esta sera nuestra plataforma de desarrollo para el primer cuatrimestre.  
Es un mini sistema operativo.

Para empezar a ejecutar hay que conocer la arquitectura de hardware y luego el assembler.

Para que funcione lo primero es tenerlo configurado.

Nos proporciona un archivo de configuracion minimo.  
El archivo de configuracion puede tener cualquier nombre.  
Si se lo llama bochsrc,  

En una carpeta podemos tener los archivos:  
test.asm  
test.bin  
bochsrc  

si ejecutamos bochs en esa carpeta, busca un archivo llamado bochsrc y lo usa como archivo de configuracion.

El bochs intenta ejecutar una imagen. ROM BIOS/

Una pc adentro tiene un microprocesador, memoria volatil (DRAM) y no volatil(ROM), de donde se lee el programa.

El programa de inicializacion que esta en ROM se llama ROM BIOS.

Hay una diferencia entre RAM estatica y dinamica.

## Ram estatica vs dinamica

La RAM dinamica es una RAM que tiene un capacitor donde almacena carga y la carga almacenada indica el estado de un bit.  
Con el tiempo el capacitor se descarga y hay que recordarle constantemente lo que tiene almacenado, es un transitor.  
La ram estatica es un flip flop, que usa 6 transistores para almacenar el bit.

Ventaja ram estatica, mas rapido el acceso. La RAM dinamica no, porque en un ciclo de resfresco refresca todos los bits al mismo tiempo, eso lleva un tiempo. Mientras hace eso, no puede accederla, pero es infinitamente mas barata.

La cache del procesador es estatica y la RAM que conocemos es dinamica.

La ram estatica ocupa 6 veces mas transitores, por eso es mas cara y mas rapida.  
.  
.  
.  
Tambien hay controladores, antes todo tenia su chip, ahora todos los perifericos vienen integrados en el chipset.

Quien hace la configuracion? El programa que esta en la ROM bios.

Hay un controlador que programa el acceso a la ram dinamica, y que requiere parametros muy precisos.  
.  
.  
Nosotros no lo necesitaremos porque nuestro sistema operativo entrara en la ROM,  
64k de tamaño como minimo y maximo 256k.  
El kernel de linux/unix original ocupaba 8k.
.  
.  
.  
.  
Esto que comentamos recien es la realidad, pero en bochs emulamos todo esto.

Aparte del archivo de configuracion necesito un archivo ROM.bin: el programa compilado traducido a codigo de maquina.

En la configuracion bochsrc indica, tu bios esta en el archivo ROM.bin, PC pentium 4, 1 gb de ram, etc.. El programa esta aca y empieza la simulacion.  
Primer ciclo, busca el primer byte a ejecutar.  
.  
.  
Solo hay un detalle, en la vida real como hace el procesador para sacar de la ROM un byte? Necesita un puntero.  
La rom esta conectada a memoria con un bus de direcciones escrito en bits.

Hay otro bus, el bus de datos, de la rom solamente puedo sacar datos.

Lo que comentaron hasta ahora puede ser cualquier arquitectura. Solo comentaron los componentes.  
Puso controladores, que son Unidades de entrada salida.

Luego veremos el concepo de arquitectura.

Todavia no saque el primer byte para comenzar a ejecutar.

Chipset incluye DRAM, USB, Red, Video, Chipset

El chipset tiene memoria estatica, registros.

El controlador de memoria dinamica no lo vamos a configurar, ni el controlador de disco rigido.  
Configuraremos el timer, etc…  
.  
.  
.  
Nosotros le diremos al bochs tengo un mother que tiene 1 gb de ram,

En la simulacion, el bochs no simula la configuracion del controlador de memoria dinamica, porque esta a nivel fisico. Es lo unico que esta disponible para acceso sin tener que configurarlo.
Pero a ese nivel no nos vamos a meter, aunque en clase nos van a dar los recursos por si un dia tenemos que luchar contra un controlador de memoria dinamica.

El controlador de memoria es flexible, se le pueden colocar memorias mas lentas o mas rapidas, de diferentes tecnologias. Algunos motherboard tienen un programa setup que permite configurar tensiones del funcionamiento, para que consuma mas o menos. Variar la velocidad a la cual ejecuta codigo, todo para ahorrar energia, pero esa configurabilidad se cobra y hace a esa motherboard mas costosa.
.  
.  
Breakpoint  
Vamos a usar programas de breakpoint, cuando pasa por una marca, se para el programa, son condiciones de detencion.

Muchas pcs tiene un puerto donde se coloca un hardware especial que se conecta aparte, donde conecto un JTAG que sirve para debuggear.

Como se ponen los breakpoint?  
Poniendo Cntrl+C en la consola se detiene, pero es muy dificil de controlarlo porque va muy rapido.  
Por eso se coloca en el programa donde queremos que se detenga.  
.  
.  
La maquina arranca detenida.  
En la consola de control, se ve que esta detenida con un cursor esperando que le digamos que hacer, arrancar, avanzar una instruccion, etc.

Pero inmediatamente el programa va a ser sencillo, aunque tenga pocas instrucciones puede tener un for que se repita 20000 veces. Le pongo un breakpoint afuera de ese lazo y le indico que corra.

El modo texto no falla jamas, nos quedamos con ese que anda mejor.

Para no molestar demasiado a la simulacion, los desarrolladores de bochs dijeron, veamos si hay alguna instruccion que sea inutil y efectivamente las hay y no se usan.

Se llama:  
xchg bx, bx.

Por ejemplo  
0x7F seria la de recien y  
0x8F xchg ax, bx

Esta instruccion intercambia 2 registros, A pasa a ser B, y B pasa a ser A, como el registro es el mismo, no cambia nada, por eso ningun programador la va a usar jamas, es inutil. Entonces el breakpoint funciona colocando esta instruccion en algun lado y cuando el simulador la detecta detiene la ejecucion.

Uno normalmente no se pregunta como hacen las maquinas virtuales para poder funcionar.

La maquina virtual simula en un archivo el contenido del disco rigido.  
A esta maquina virtual, bochs, no le pusieron tanta onda a la parte grafica, sino la simulacion.

Puede correr un windows 95, pero va a ser lento. Porque no se le puso tanta onda a la parte grafica por ejemplo.  
.  
.  
Cuando enciendo el micro, lo busca en la misma direccion siempre o en una distinta? En la misma, porque esta seteado en sus condiciones de inicio.  
Por eso tengo que saber las condiciones de inicio, eso esta en el manual del microprocesador (intel por ejemplo).

En general toda direccion de memoria que marca un hito en la vida del programa se le llama “vector”. En la jerga de electronica se le llama puntero.

A esa direccion de inicio se le llama “vector de reset”.

Un procesador es una maquina de correr instrucicones, es como una maquina de estados y debe tener como tal un punto de inicio.  
.  
.  
Hay un concepto que no hay que perder de vista, interrupcion de hardware. Todos los procesadores implememtan de algun modo la posibilidad de alterar de forma fisica, externa el flujo del programa.

La instruccion de salto es parte predefinida del programa. Pero si quiero por un evento externo al procesador presionar una tecla en el teclado? Cuando toco la tecla, genera un evento. Es una interrupcion. En el procesador eso pasa, lo interrumpe en lo que hace en ese momento.  
Cuando viene la interrupcion externa, hay que darle bolilla a lo que viene de afuera, eso tambien debe ser un evento previsto. Tiene que impactar en la logica que esta en curso.

Tengo que garantizar que cada vez que se toca una tecla, se guarda en un buffer de memoria unos cuantos bytes. Y cuando tiene tiempo le da bolilla. Ahora, el estaba haciendo algo, pero cuando llega a la tecla, tiene que guardar su valor, cada tecla tiene un codigo.

El chipset toma el byte que viene del dispositivo, le avisa al procesador, este para todo y se fija que le llego algo del teclado.  
Eso lo hace el procesador a travez del programa que tiene programado.

Cuando llega la interrupcion, el procesador al menos tiene otro vector aparte del de reset que es el vector para atender interrupciones.  
En nuestro primer ejemplo no vamos a usar las interrupciones, se van a deshabilitar.

Es complejo, el procesador tiene un priorizador de interrupciones.  
Cuando llega una interrupcion, vamos al vector de interrupciones.

El bus de direcciones es un bus de alta impedancia.  
.  
.  
.  
Si tengo microprocesador, rom y chipset y se tienen que comunicar entre ellos, usan el bus de datos.  
Pueden in y venir datos desde el micro al chipset.

El de direcciones va del micro al chipset y le dice desde que direccion va a sacar el dato.  
Ahora cuando tiene que leer un byte de programa, tambien va a usar el bus de datos, pero en ese caso el bus solo sale de la rom y va al micro. Y por supuesto hay que darle direcciones desde el micro a la ROM.

Ahora, hay otro bus chiquito que es el de control, tiene pocas lineas y dice a cual se selecciona, es el chip select. La direccion le llega a ROM y a Chipset y con el ship select se sabe cual tiene que escuchar.

El bus de direcciones tiene que estar en alta impedancia, si no lo pongo en alta impedancia, se van apilando las resistencias en paralelo y van tirando el bus a tierra a travez de sus direcciones. Por eso el dispositivo no seleccionado por el chip select pone sus direcciones en alta impedancia.

El bus de datos es de alta impedancia el de direcciones tambien, todo lo que se comparta por mas de un dispositivo suele ser de alta impedancia.

En microcontroladores se usa colector abierto

En el caso del totem pole, si entiendo el transistor de abajo tengo un cero, si lo hago con el de arriba, tengo un uno. Si levanto ambos, entonces queda en alta impedancia.

El que le da el valor es el dispositivo que no esta en alta impendancia.  
.  
.  
.  
En particular, vamos a simular un procesador de intel que tiene cierto vector de reset.

La PC original usaba un bus de datos de 16 bits. Cuando se resetean, todos los procesadores, incluso los i7 mas modernos inician el bus de datos en 16 bits, es su modo inicial. Intel llama a ese modo “Modo real”, (16 bits sin proteccion de memoria.)

Tiene otro modo mas para ser compatible hacia atras, pero no nos interesa.

Tiene varias limitaciones este modo real, no puede direccionar toda la memoria que quisiera, no puede direccionar toda la memoria de los modos 32 o 64 bits.

En dicho modo, el bus de direcciones solo usa 20 bits.

Todo esto sin importar el numero de bits del procesador.

Nuestro primer ejemplo lo veremos en modo real para ver de que se trata en/el assembler

PPT  
Compatibilidad, angel o demonio,  
La memoria

![1.png](1.png)
![2.png](2.png)
![3.png](3.png)

El mapa de memoria es dibujar esquematicamente que zonas o bloques contienen cosas o tienen alguna importancia especial en el funcionamiento del procesador que estamos viendo.

![4.png](4.png)

CS = 0xF000 = 0b1111_0000_0000_0000  
IP = 0xFFF0 = 0b1111_1111_1111_0000

Si hacemos CS<<4 + IP  
0b1111_0000_0000_0000_0000  
+  
     0b1111_1111_1111_0000
___________________________
0b1111_1111_1111_1111_0000 = 0xFFFF0  

Para este procesador en particular nos interesa que tamaño tiene ese mapa para el modo que usaremos, el real, y donde esta la direccion del vector de reset.

Este mapa esta pensado para el modo 32 bits, son 4gb, 0xFFFF_FFFF, en modo real son 20 bits, por lo tanto, dicho modo real alcanzara la direccion 0x000F_FFFF (que es igual a 0b0000_0000_0000_1111_1111_1111_1111_1111 (pueden contarse los 20 bits)), mi mapa de memoria de modo real es la parte de arriba a dicha direccion. Eso lo dice el manual de intel.

No esta marcado en el dibujo, pero 16 bytes antes del final del mapa de memoria en modo real esta el vector de reset.

Que direccion seria enteonces?

``` sh
0xFFFFF


0xFFFF0 → Aca esta el vector de reset.

0xF0000 → Aca comienzan la ROM.

0x00000



0xFFFFF = 0b0000_0000_0000_1111_1111_1111_1111_1111
- 16 bytes = 0b0000_0000_0000_0000_0001_0000_0000_1111 (direccionados)
__________________________________________
0xFFFF0 = 0b0000_0000_0000_1111_1111_1111_1111_0000
```

``` sh
0x00 = 0b0000_0000 00 Bytes
0x01 = 0b0000_0001 01 Bytes
0x02 = 0b0000_0010 02 Bytes
0x04 = 0b0000_0100 04 Bytes
0x08 = 0b0000_1000 08 Bytes
0x0F = 0b0000_1111 15 Bytes
0xFF = 0b1111_1111 255 Bytes

2^(8(bits)) = 256 bytes direccionables con 8 bits o 1 byte.
```

Eso esta explicado en  
<https://stackoverflow.com/questions/41305359/intel-reset-vector-and-documentation-pedantics-bits-vs-bytes>  
Y es porque la unidad mas chica de memoria referenciable por un microcontrolador es un byte.


Esto es que el vector llamado instruction pointer (IP) apunta al valor del vector de reset.

Yo no puedo cambiar donde va el instruction pointer, si lo hiciera en VHDL si podria, pero en procesadores fisicos no, esta hardcodeado.  
.  
.  
La primera instruccion es CLI, que deshabilita las instrucciones.

Hay otro registro del procesador que es el de estado, que es MSW(Registro de flags), donde con bits puedo setear el comportamiento de las interrupciones por ejemplo.
IF: interruption flag, eso normalmente esta en cero cuando inicio el procesador.

Intel asegura que siempre va a estar en cero cuando prenda la computadora, pero en la realidad puede que no se cumpla, para estar seguros siempre cuando inicia el programa vamos a ponerlo en cero.  
.  
.  
IP es el valor final que va a tener el bus de direcciones.  
Internamente Intel compone la direccion fisica a las patas de direccion con 2 valores o registros..

para instalar interfaz grafica de bochs instalar bochs-x  
.  
.  

---

Instruction Pointer = Program counter.  
.  
.  
Mientras el procesador este en modo real.

Cada vector de interrupcion ocupa 4 bytes, necesito 2 y medio, pero ocupa 4.

Cada valor para poner en el bus de interrupciones 256x4  1024

Con esos 4 bytes arma una direccion de 20 bits.

El controlador de interrupciones le dice por una pata interrupcion y por otra

Para contar en binario, con una mano cuantos bytes podemos contar? 5, pero cuando usamos binario podemos contar hasta 32.

Hay un detalle, cuando enciendo la compu la memoria esta en blanco.

El manual de Intel dice que previamente tengo que cargar en esa direccion de memoria las direcciones donde estan las rutinas de atencion de interrupciones.
.  
.  
El programa que saco de Rom tiene que configurar en RAM una tabla.

Parte del programa de inicializacion debe copiar los vectores de interrupcoin en la rom, las rutinas pueden estar en la misma ROM, I.S.R. entonces cuando le entra una interrupcion numero N que va de 0 a 255. Cuando desenergizo la RAM se borra entonces.

En el mapa de memoria, una parte es la ram y la otra la ROM.

La rom comercialmente tiene 64 k y vamos a usar este tamaño.
POST y BIOS esta en esos 64k.

En una pc comercial, la ROM se divide en POST, Power On Self Test, que cuando se energiza un sistema corresponde testear el sistema, que la ram no este arruinada, etc.. Se le escriben todos 0s y se lee que sean todos ceros, despues lo mismo con 1’s.

Cuando empezamos a trabajar con embebidos, empezamos a trabajar desde la 1ra instruccion.

El POST es muy importante y habria que hacerlo en la vida real.  
Por que confiaremos en esto? Porque la memoria que usamos en el simulador es la memoria de mi computadora, que ya arranco, entonces ya sabemos que la memoria funciona, el bochs confia en esta memoria.

En los manuales de Motherboard aparece que la secuencia de bips de sonido que hace la computadora y que significa que problema hay. Todo eso lo hace el POST.

En el vector de reset hay un clear interrupt y un jump hacia atras (64k), para que corra el POST.  
Si al compilador le digo compila eso y 

El punto donde tendrian que estar los 15 bytes antes del final lo llamo vector_reset.  
Al post lo pongo antes. Mi programa POST no hace nada.

Si le doy eso al compilador, el compilador no sabe donde  
F0000 tendria que ser su direccion.  
.  
.  
# Posicion de ensamblado
.  
.  
El 50% de los problemas de la parte inicial de la guia.

El objetivo es arreglar todo para que el vector de reset quede 16 bytes antes del final. Cuando logre hacer eso y compile el programa, el archivo tiene que tener 64 bytes.

Instalar Hex Viewer para ver en hexadecimal, para ver como quedo todo adentro byte por byte.  
.  
.  
Voy a ver 2 bytes al principio, despues casi 64k de relleno y en los ultimos 15 bytes 3 bytes codificando 2 instrucciones y despues algunos bytes mas hasta el final.

Los programas de la guia son incrementales, se construye uno sobre el siguiente y asi sucesivamente.  
.  
.  
Nosotros lo vamos a usar con un mapa de memoria de hasta 4gb.  
.  
.  
Podemos usar cualquier editor de texto.  
.  
.  
.  
# Ahora hablaremos del archivo bochsrc

config_interface, forma de configurar  
display_library, para linux x en lugar de sd12.

Lo que esta con # son comentarios.  
Romimage: señala el archivo donde esta la rom. Que se llama mi_bios.bin y tiene que ocupar 64k.

IPS son las instrucciones por segundo, se le puede dar mas o menos, nosotros usaremos lo que esta ahi, no tocamos.

Lo mas importante de la parte de abajo es magic_break, que habilitado, hace que funcionen los breakpoints.

port_e9_hack es algo que usaremos mas adelante. Para configurar los perifericos, tenemos el mapa de memoria que vimos hasta ahora y despues tenemos un mapa de perifericos donde configuro los registros del chipset. Tengo instrucciones que operan sobre el mapa de memoria e instrucciones que operan sobre el mapa de perifericos.  
Como los perifericos ocupan muy poca memoria en el mapa real, el mapa de perifericos ocupa 1k.

En la PC verdadera no esta asignado a nada, envio bytes de un periferico o recibo bytes de un periferico.

Escribo el byte que quiero que salga.

El byte e9 de ese mapa no tiene nada en la pc real, pero sirve para que por ejemplo escriba hola por ese puerto. Hace como un printf sobre la consola, con fines de debuggeo.

Hacete una rutina que los strings los vaya tirando byte a byte mensajes. 

Intel sugiere diferentes chipset para sus procesadores.  
Si uno quiere ciertas caracteristicas elije el chipset adecuado que intel recomienda.

El chipset que esta ahi es el que usaremos.  
.  
.  
.  
.  
__Ahora veremos el programa__, mañana lo va a subir.

2018/ej-gtp/ej-gaby/Main.asm

el editor se llama Geany.

Para rellenar la rom se usa la funcion  
times 0x10000-16-($-Inicio) db 0.

De todo lo que esta en ese archivo hay cosas que estan hechas para ser convertidas en codigo de maquina y hay otras cosas que son instrucciones del compilador (directivas al compilador) que indican como se distribuye la ram. Estas directivas no van a ser parte del codigo de la ROM.

Como se cuales son las directivas?  
No queda mas remedio de recurrir al manual o a la memoria para saber cuales son.

El lenguaje C tambien tiene instrucciones del lenguaje y directivas del compilador.  
Un “#define” es una directiva del compilador.

La directiva analoga al define en assembler es “equ”.

Mov  
.  
.  
.  
.  
.  
Hay una serie de registros

|     |             |     |
| --- | ----------- | --- |
| Ax  | SI          | CS  |
| Bx  | DI          | DS  |
| Cx  | SP          | ES  |
| Dx  | BP          | SS  |
| .   |             |     |
| .   |             |     |
|     | IP          |     |
|     | FLAGS (MSW) |     |

.  

## Lo que sigue es un 1 directo en un parcial

mov cs, ds es una instruccion que no existe.

En general se puede mover cualquier registro x con cualquiera. Lo mismo con Si y Di.  
Los que terminan en S se llaman registros selectores. Luego veremos que es lo que seleccionan.  
Los anteriores se llaman de proposito general.

Todos los registros mencionados son de 16 bits (en modo 16 bits).  
No se puede mover de un registro selector a otro selector, necesito un metodo intermedio

IP y FLAGs estan separados porque no se les puede asignar un valor con MOVE  
.  
.  
## finloop

la definicion de finloop esta dada por una formula con una sintaxis particular.

La cuenta es el signo $-Inicio (que se definio antes)

El compilador asigna un valor numerico a todas las etiquetas que tengan un “:” al final, les va a asignar el valor numerico de la posicion de ensamblado.

Las directivas no tienen posicion de ensamblado, no ocupan espacio en la ROM.

A Inicio, como no dije nada, le va a asignar el valor numerico 0.

Luego encuentra otra etiqueta, Copia.

Yo humano tengo que recurrir al manual, porque no se cuanto ocupan las instrucciones de Inicio.  
Si sumo todo eso, me va a dar alrededor de 12 bytes. Copia entonces equivaldra a 12.

Todos los numeros en el programa aparecen en hexadecimal.

Loop es un for. En el procesador se hace con los registros de proposito general.  
Cx es el contador de lazo por defecto y no se puede cambiar, no puedo decidir que el contador de lazo sea otro registro.

En este punto se empieza a ver que este es el programa que se copia a si mismo en otra posicion de memoria.

Tiene que calcular cuanto mide el programa, para eso usa las etiquetas y las va restando.

Lodsb: Lee un byte a donde esta apuntando SI y lo guarda donde apunta DI.  
Con STOS lo guarda.

Si: Source Index  
Di: Destination Index

Cuando se loopea, Cx tendria que cambiar su valor. Para saber si se incrementa o decrementa hay un bit en flag que se llama direction, DI.  
.  
.  
El valor de reset del direction flag indica que se va a incrementar.

Si (Source index) es desde donde se va a empezar a copiar.

Este loop no solamente altera a Cx, va a alterar tambien a Ci y a Di.

Va copiando y Si y Di se incrementan de forma automatica. Y los incrementa en funcion de lo que estoy moviendo, sea un byte o un word.

En la nomenclatura de Intel

``` sh
b: byte    en c seria un char
w: word (2 byte)
d: double word (2 word = 4 bytes)
q: quad word (4 word = 8 bytes)
dq: double quadword = 16bytes

lodsb para byte
lodsw para word
lodsd para double word
y asi sucesivamente.
```

Es lo mismo que si incremento punteros  
si incremento puntero a char se incrementa en …  
si incremento un puntero a int se incrementa en 4.  
.  
.  
En este punto pregunto como hacer un lazo anidado en assembler.  
Compiler explorer me permite traducir de C a asembler.

Si quisiera escribir un lazo anidado tendria que copiar el valor en la pila.

La pila es otro registro.

Pero como no puedo usarlos porque estan reservados.

Una pila LIFO (Last In First Out) es un buffer o una memoria donde tiro un dato y cae arriba. Meto primero x, luego y, luego z, para recuperar y tengo que sacar primero z.

Todos los procesadores manejan una pila, que es un pedazo de memoria y un puntero.

Uso el stack pointer.  
Tengo instrucciones dedicadas que guardan lo que esta en el acumulador a la pila y traer lo que esta en la pila  
.  
.  
Las instrucciones de la pila son push y pop.

En intel, puedo hacer push y pop de cualquier registro

``` sh
push(reg)
pop(reg)


mov ax, cs
mov ds, ax
push cs
pop ds 
```

Hay una condicion para usar la pila y es asignar el valor del stack pointer.  
(CCC)  
No puede ser cualuqier valor.

Este programa no hace uso de la pila. Porque aun no sabemos donde podemos ubicarla.  
.  
.  
En el programa podemos ver como usamos las instrucciones  
xchg bx, bx  
para llamar al breakpoint.

Este es un micro con set de instrucciones complejo, CISC.  
En ARM necesitariamos mas instrucciones, porque tiene RISC
.  
.  
Cuando termina de copiarse a si mismo a la parte baja de la memoria y salta a ejecutar el programa.  
Enseguida justificaremos por que usa  
jmp 0:0  
.  
.  
.  
La directiva  
times 0x10000-16-($-Inicio) db 0.

tiene un peso,  
.  
.  
db: define byte, es decirle al compilador que necesito que en la posicion actual donde estas ensamblando el programa no decodifiques una instruccion.

Times es repetir la directiva del final.  
Tantas veces como el numero que sigue, lo que esta el final.

Rellename con 0 tantas veces hace esta directiva.

Ahora tiene que llenar  
.  
.  
reset se va a ensamblar despues de 20 bytes. Y eso que esta ahi tiene que ser menos de 16 bytes.

Luego hay que rellenar los ultimos que quedan.

$-reset indica cuanto ocupa reset.

Fin de codigo tiene que dar 64k.  
.  
.  
Como quiero apuntar a 20 bits de direcciones y tengo registros de 16 bits entonces necesito 2 registros para cada direccion.

Los registros selectores son para eso, para seleccionar el segmento de 64 k que voy a seleccionar.

Funcionan cada uno con algo en particular y de a pares.

Los siguientes se llaman direccion logica, y se componen de 2 registros cada una.  

``` sh
Selector : Desplazamiento  
CS : IP, este par es un puntero a memoria de 20 bit.  
SS : SP, este par maneja el puntero a pila.  
DS : SI,  
ES : DI,  
```

Selector: selecciona un segmento de 64k, es como una ventanita.  
Con el otro se barren esos 64 para elegir algo.

El procesador usa estos registros para acceder a direccoines de memoria donde hay codigo.

``` sh
CS _____
     _____ IP
_____________
   FFFF0
```

si ponemos jmp 0 no vamos al principio, porque el principio esta en otro lado.

CS : IP maneja el bus de direcciones.

Esas 4 combinaciones no se pueden alterar

recien cuando volcamos un dato sobre el controlador de video aparece una pantalla con video.

En using log file muestra la direccion

como el modo de 16 bits tiene registros de 16 bits muestra el valor para el CS y para el IP.  
Fa es el codigo de operacion de la instruccion CLI.

Con step puedo avanzar de a 1.  
la siguiente instruccion esta en la posicion f000L fff1, el segmento de codigo no cambio, cambio el IP.

Le mueve 16 bits al registro Ax.  
B8 es el codigo de operacion de la instruccion move ax. Y ff00 es….

Intel guarda los bits menos significativos en la posicion de memoria mas alta, motorola es al revez y arm es configurable, cualquiera de los 2.

La siguiente instruccion carga en AX. B8 0000  
Siguiente paso, el jump. El jump es e9, que es el codigo de operacion de la instruccion jump.  
El compilador me tradujo la instruccion de una forma diferente a como se la codifique. Como yo se la di de forma de desplazamiento, me la codifico (CCC) y se ahorrro 2 bytes.

F000:0000, es el principio del programa.  
.  
.  
.  
.  
Reflexiones:

El archivo mi_bios.bin ocupa 64kiB (65.536)

1 kibibyte = 1024 B = 2^10 bytes.  

<https://es.wikipedia.org/wiki/Kibibyte>

64kiB = 64*1024 B = 65.536 Bytes = 0x1_0000 B.

65.536 Bytes = 524.288 bits.  
Esta cantidad de bytes puede ser direccionada mediante como maximo el numero: 0x1_0000  
O si empiezo desde el cero, con 0xFFFF, esto es equivalente a 16 bits.  
.  
.  
Cada dueto en hexadecimal es:  
0xFF = 255 = 0b1111_1111  
Es decir, necesito 2 valores en hexadecimal para representar un Char.  
Ademas, 0xFF es equivalente a un Byte.  
Y ademas The size of character variable is 1 byte. (<https://www.programiz.com/c-programming/c-data-types#char)>  
.  
.  
1MB, que es lo que me piden que pueda manejar de memoria equivale a:  
1024 kiB, que es claramente mayor a los 64kiB del archivo que seria la ROM.  
Por lo tanto entiendo que me piden que maneje RAM.  
1024 kiB = 1024 * 1024 B = 1048576 B = 0x10_0000 B  
Esta cantidad de bytes puede ser direccionada mediante como maximo el numero: 0x10_0000  
O si empiezo desde el cero, con 0x0F_FFFF, que equivale a 20 bits.

En el ejercicio 2 me piden que copie el programa a la direccion de memoria 0xF_0000  
Esta direccion es equivalente a la posicion de memoria: 983040, por lo tanto es bastante mayor a los 64kiB  
.  
.  
#### LODS

Esta instrucción toma la cadena que se encuentre en la dirección especificada por SI, la carga al registro AL (o AX) y suma o resta 1 (segun el estado de DF) a SI si la transferencia es de bytes o 2 si la transferencia es de palabras.  

<http://moisesrbb.tripod.com/unidad3.htm#unidad321>

LODS: Carga cadenas de un byte o palabra al acumulador.  
Sintaxis: LODS  
Toma la cadena que se encuentre en la dirección especificada por SI, la carga al registro AL (o AX) y suma o resta 1 (segun el estado de DF) a SI si la transferencia es de bytes o 2 si la transferencia es de palabras.  
Ejemplo:  

``` sh
MOV SI, OFFSET VARABLE1  
LODS
```  

La primer linea carga la dirección de VARIABLE1 en SI y la segunda linea lleva el contenido de esa localidad al registro AL.  
Los comandos LODSB y LODSW se utilizan de la misma forma, el primero carga un byte y el segundo una palabra (utiliza el registro completo AX).  
<http://www.mat.uson.mx/lcota/Lenguaje%20ensamblador.htm>  
.  
.  
.  
__LODS:__  
AC  | LODSB | Load byte at address DS:(E)SI into AL

__Description__  
Loads a byte, word, or doubleword from the source operand into the AL, AX, or EAX register, respectively. The source operand is a memory location, the address of which is read from the DS:EDI or the DS:SI registers (depending on the address-size attribute of the instruction, 32 or 16, respectively). The DS segment may be overridden with a segment override prefix.  

<http://faydoc.tripod.com/cpu/lods.htm>  
.  
.  
.  
.  
__STOSB:__  
AA | STOSB | Store AL at address ES:(E)DI  
  
__Description__  
Stores a byte, word, or doubleword from the AL, AX, or EAX register, respectively, into the destination operand. The destination operand is a memory location, the address of which is read from either the ES:EDI or the ES:DI registers (depending on the address-size attribute of the instruction, 32 or 16, respectively). The ES segment cannot be overridden with a segment override prefix.

At the assembly-code level, two forms of this instruction are allowed: the "explicit-operands" form and the "no-operands" form. The explicit-operands form (specified with the STOS mnemonic) allows the destination operand to be specified explicitly. Here, the destination operand should be a symbol that indicates the size and location of the destination value. The source operand is then automatically selected to match the size of the destination operand (the AL register for byte operands, AX for word operands, and EAX for doubleword operands). This explicit-operands form is provided to allow documentation; however, note that the documentation provided by this form can be misleading. That is, the destination operand symbol must specify the correct type (size) of the operand (byte, word, or doubleword), but it does not have to specify the correct location. The location is always specified by the ES:(E)DI registers, which must be loaded correctly before the store string instruction is executed.

The no-operands form provides "short forms" of the byte, word, and doubleword versions of the STOS instructions. Here also ES:(E)DI is assumed to be the destination operand and the AL, AX, or EAX register is assumed to be the source operand. The size of the destination and source operands is selected with the mnemonic: STOSB (byte read from register AL), STOSW (word from AX), or STOSD(doubleword from EAX).  
<http://faydoc.tripod.com/cpu/stosb.htm>
