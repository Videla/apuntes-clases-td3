# clase 3 22-08-2019

Es muy importante de pipes que lo que se lee del pipe, se borra del mismo. Esto es manejado automaticamente por el kernel.
Desde el punto de vista de la programacion, lo opero como un archivo de disco, es persistente PERO solo hasta que alguien lo lee.

El programa hijo empieza a correr desde despues del fork, por eso no vuelve a crearse otro pipe. Si arrancara desde antes, lo volveria a crear al pipe.

La task struct de ambos programas padre e hijo son iguales, pero el pid es distinto.
Que mas cambia? El puntero del proceso padre debe ser distinto. Tambien hay que cambiar los punteros anterior y siguiente para poder insertarlo en la cadena.

El campo fd es un puntero a un array que tiene un indice de 0 a N y ahi va a signando punteros a buffer que maneja los archivos del filesystem que fui abriendo.

Habiamos visto que los casilleros de los slot 0, 1 y 2 estan fijados a los flujos standar stdin, stdout y stderr.

En la carpeta proc, que no es una carpeta como tal, no esta en disco. Se crea dinamicamente
Dentro de la carpeta que contiene el task struct de cada proceso.

Por que el puntero al pipe tiene 2 posiciones. Porque uno es de lectura y otro de escritura.
Que pasa si intento escribir o leer de donde no debo???

Al abrir un pipe se ocupan las posiciones 3 y 4, una para lectura y una para escritura.

Para abrir archivos, copia el contenido en el stdin.
Cuando usamos una estructura con el tipo de dato FILE* que se usa cuando usamos fopen y fread.
El archivo es persistente, pero copia de apartes en el stdin.
Si tenemos 2 procesos leyendo un archivo, se va a mover el puntero de archivo. Y ninguno de los procesos va a poder leer el archivo completo. Para eso hay que usar fseek().

Si no tienen relacion de parentesco, no tienen forma de usar el mismo pipe.

Uno podria escribir codigo de kernel para poder encontrar esto y pasarselo al otro, seria como escribir un kernel. Pero hay otras formas de hacer esto entre procesos que no tienen relacion de parentesco.

Lo que nos lleva a la siugiente estructura.

## __Named pipe. (FIFO)__

El pipe comun tambien es FIFO???

Puede ser referenciado por un nombre de archivo
El named pipe tiene asociado un path name. Porque tiene asociado un path en el file system, pero no es un archivo. Si lo busco como archivo fisico no existe.
Si se el nombre que tiene en el file system lo puedo abrir.
En lugar de aparecer una _d_ o un _-_ al hacer __ll__, va a aparecer una _p_.

Para probarlo

``` console
sudo su -
mkfifo test
ll
```

mkfifo es una especie de atajo para el archivo mknod.
Lo puso de otro color, ni blanco(archivo) ni azul(carpeta), sino naranja. Y en lugar de ponerle / al final le pone |, porque es un pipe.

Nos falta ver una cosa de file system.
Que es file system?
file system es el juego de reglas para como van a estar ubicados los datos en un medio de almacenamiento. El file system basicamente es una base de datos.

En unix, todo puede interpretarse como un archivo. Lo correcto es decir que en unix todo es un objeto del file system.
Esta en el disco desde el punto de vista que esta en el file system.

En realidad un pedazo del disco bruto se usa como base de datos del file system, que es una coleccion de registros que hacen referncia a los datos que usa el file system.
Si el objeto del file system es por ejemplo un named pipe, lo que hay aca es una referencia que dice nombre, tipo, fecha de creacion, fecha de modificacion, atributos de acceso, etc.

El buffer asociado se copia a memoria y esta disponible para que yo lo lea y lo consua.
El buffer que maneja eso directamente esta en la memoria, no en el disco.

``` console
echo HOLA > /root/test
```

No me devuelve el cursor porque esta esperando que alguien lo lea.

En otra ventana de terminal hacemos

``` console
cat /root/test
```

tambien podriamos haber hecho vi en lugar de cat. nano en cambio no hubiera funcionado.

No se puede copiar el pipe, pero si se puede mover. Cambiarlo de lugar puede servir para que cambien los permisos de acceso.
Si quiero leer con __cat__ que hay dentro de test cuando esta vacio, me va a bloquear esperando un dato. Entonces los pipe pueden verse como una forma de sincronizacion entre procesos.
Como un semaforo? No. Es distinto.

Hay una funcion muy poderosa pero poco conocida llamada _ioctl_
Ioctl puede hacer que los read sean no bloqueantes.

La funcion read devuelve la cantidad de bytes que leyo.
Si estuviera en modo default seria bloqueante. Se queda parado esperando leer algo.
Con ioctl puedo configurar para que cuando lea y no haya nada (osea devuelva cero), no se bloquee. Que continue.

Esto lo dice porque ahora cuando empecemos a programar de forma multiproceso, es importante la sincronizacion entre procesos.
Por eso el comportamiento default es bloqueante. Para que todos los procesos vayan juntos sincronizados. Hay casos donde no tengo tiempo para esperar al otro. Depende de como me planteen el problema que tengo que resolver.

ioctl(1er parametro es siempre el file descriptor sobre el que voy a operar. Y el resto de los parametros depentede del primer parametro. Para cada caso hay que ver.) el ioctl tiene un switch case gignate en su manual. dependiendo del tipo de archivo. Es complejo y varia mucho la sintaxis de acuerdo a donde estoy operando.

ioctl tambien puede operar sobre conexiones de red.

Automaticamente cuando hago echo HOLA > /root/test le pone un EOF. Lo que permite que recupere el control de la consola. Si desde otro programa no le pongo el EOF. No corta la espera y puedo escribir como quiera.

El cat interpreta el /n y /r como fin de archivo.

Sprintf en lugar de salir a pantalla sale a un string. Es la forma mas rapida de armar un buffer con un string y despues pasarselo a un pipe.

En \n \t \r \0 la \ se llama caracter de escape, que significa que escapa de la secuencia que venia siguiente. Y si quiero guardar una barra? Tengo que poner:

```
\\
```

___
___
___

## Ejercicio

Un proceso padre tiene que generar 2 procesos hijos, el proceso padre escribe en el pipe. Los 2 procesos hijos leen del pipe y tienen que ir mostrando lo que van leyendo.

El proceso padre como escribe? Lee del teclado. Hacemos scanf y cuando le damos enter se manda el mensaje que ira al pipe.

Los procesos hijos van leyendo ese pipe.

Recordar el detalle de que los procesos hijos en algum momento leeran del pipe.

Los procesos hijos imprimen lo que encontraron en el printf diciendo quienes son(importante porque se imprimen todos en la misma pantalla) y lo que leyeron del pipe.

Despues de que el tipo lee algo, por una cuestion de poder ver lo que esta pasando, a proposito le vamos a poner un tiempo de dormir.

Lee del pipe, imprime, espera un segundo y vuelve a leer del pipe.

El otro hijo va a ser lo mismo, pero esperara un tiempo diferente.

Si tenemos tiempo podemos hacer una espera aleatoria entre 1 y 4 segundos.

Si hacemos un while 1 con un scanf no consume recursos. Porque se bloquea el proceso.
En otro ventana hacer ps -ef y ver lo que esta pasando
Primero crear el pipe y despues hacer el fork.

recomendacion, lo que leemos del teclado se va a guardar en un buffer y ese buffer ponerlo en un write.

Podemos probar lo siguiente. El string como vimo
Agregar un \0, agregar un \n y ver que al leer el read se va a comportar de formas distintas.
