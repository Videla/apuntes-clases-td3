// Ejemplo de pipe y fork con 2 hijos con comprobacion de error y movidos a funciones los procesos.

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>

int a[2];

void padre(void)
{
  printf("Padre: Hola, soy el padre, yo enviare mensajes a mis hijos.\n");
  char string[255];

  while (1)
  {
    printf("Padre: Escriba ahora en el teclado y aprete enter, yo enviare el mensaje a mis hijos.\n");
    scanf("%s", string);

    printf("Padre: El string %s fue ingresado, en unos segundos lo escribire en el pipe.\n", string);
    sleep(2);

    printf("Padre: Escribire en el pipe ahora.\n");
    write(a[1], string, strlen(string) + 1);
    sleep(4);
    string[0] = '\0';
  }
}

void hijo1(void)
{
  char buffer[255];
  printf("Hijo1: Hola, soy el hijo 1, cuando me mandes algo lo leere del pipe.\n");

  while (1)
  {
    read(a[0], buffer, 100);
    printf("Hijo1: Me llego algo, en unos segundos lo leere.\n");
    sleep(2);

    printf("Hijo1: He leido: %s\n", buffer);
    buffer[0] = '\0';
  }
}

void hijo2(void)
{
  char buffer[255];
  printf("Hijo2: Hola, soy el hijo 2, cuando me mandes algo lo leere del pipe.\n");

  while (1)
  {
    read(a[0], buffer, 100);
    printf("Hijo2: Me llego algo, en unos segundos lo leere.\n");
    sleep(2);

    printf("Hijo2: He leido: %s\n", buffer);
    buffer[0] = '\0';
  }
}

int main(void)
{
  pid_t child1Id, child2Id;

  if (pipe(a) < 0)
  {
    perror("pipe");
    exit(1);
  }

  printf("Bienvenido al programa de la clase!\n\n\n");

  if ((child1Id = fork()) < 0) // compruebo si hubo errores al hacer el fork
  {
    perror("Hubo un error! El error fue en fork child1Id: ");
    exit(1);
  }

  if (child1Id > 0) // is parent
  {
    if ((child2Id = fork()) < 0) // compruebo si hubo errores al hacer el fork
    {
      perror("Hubo un error! El error fue en fork child2Id: ");
      exit(1);
    }

    if (child2Id > 0) // Parent
      padre();

    else if (child2Id == 0) // Child2
      hijo2();
  }

  else if (child1Id == 0) // Child1
    hijo1();

  return 0;
}
