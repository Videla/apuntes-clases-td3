// Practica de funciones de fork.

#include <unistd.h>
#include <stdio.h>

int main(void)
{
  int childId;
  childId = fork();

  if (childId >= 0)
  {
    if (childId > 0) //is parent
    {
      int ownId = getpid();
      int bashId = getppid();
      printf("I am the parent, the parent Id is: %d and its child id is: %d, the PID of the bash is: %d\n", ownId, childId, bashId);
    }
    else //is child
    {
      int ownId = getpid();
      int parentId = getppid();
      printf("I am the child, the child Id is: %d and its parent id is: %d\n", ownId, parentId);
    }
  }
  else
  {
    perror("Hubo un error! El error fue: ");
  }

  return 0;
}
