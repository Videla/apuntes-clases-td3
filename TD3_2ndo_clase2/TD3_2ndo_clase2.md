# Clase 2 15-08-2019

Las señales son a los procesos como las interrupciones a los procesadores.

Mediante task_struct se conforma una lista multiplemente enlazada, no solo multiplemente enlazada.

Vamos a ver el kernel como pudiendo administrar cual es el proceso que vendra.

El sistema operativo puede correr en varios procesadores y en las tareas dice en que procesador va a correr y en que procesasor corre.

Campos importantes de las tareas son:

- PID: Process Id
- State:
  - running: se esta ejecutando

Si tengo un sistema que tiene mas de un nucleo, puede haber mas de uno que este en ese estado. Si solo tengo uno, puede haber solo un proceso en ese estado.
A el SO no le interesa si hay 4 procesadores o un procesador con varios nucleos.

Como puedo ver yo cual tarea se esta ejecutando?

El comando:

``` bash
ps
```

sin argumentos, me lista algunos datos del proceso y los que estan asociados con esta sesion en esta terminal de usuario.

Si quiero ver todos los procesos le tengo que agregar opciones:

``` bash
ps -e
```

El proceso con PID = 1 es el primer proceso.

El siguiente comando me muestra mas datos:

``` bash
ps -f
```

PPID: ID del padre del proceso.

El siguiente comando me muestra el nucleo en el que esta corriendo, y en el campo S me muestra el estado del proceso.

``` bash
ps -l
```

Hay otro comando que se va refrescando que se parece mas al administrador de tareas de windows.

``` bash
top
```

Tiene 2 nucleos porque hay 2 procesos en estado running.

Los que no estan en estado running pueden estar durmiendo

Me cuenta como esta paginada la memoria.
Se llama top porque se ordenan los procesos por alguna de las columans que estan ahi.

El promedio de carga tiene 3 valores, el instantaneo del ultimo minuto, el de los ultimos 5 y el de los ultimos 15.

Los procesos que no esta en running estan detenidos por varios motivos:

- S: Sleeping
- T: sTopped
- R: Running
- D: Defunct
- Z: Zombie Estado intermedio entre vivo y muerto.

De sleeping hay 2 sabores. Uno es __interrumpible__ y otro __no interrumpible__.

El campo estado esta codificado y va a estar corriendo en uno de esos estados.

Stopped significa que esta pausado, el sckeduler se saltea a todos los estados durmientes o detenidos. Se los va a saltear hasta que cambien de estado a running.

## Tipos de señales

Hay distintos tipos de señales asi como hay distintos tipos de interrupciones.

Las señales se llaman con sig.
sigterm
sigstop: Un proceso solo puede pasar a estado stopped mediante la señal stop.

El comando

``` bash
kill -l
```

me muestra el estado de las señales.

## Hacemos un programa

``` bash
while sleep 1
do
echo hola
done
```

En este caso el bash tiene el PID 47420

El siguente comando es como __echo__ pero imprime con letras mas grandes.

``` bash
figlet
```

``` bash
while sleep 1
do date | figlet
done
```

figlet hay que instalarlo.

Para detener un proceso hacemos:

``` bash
kill -17 47420
```

si ahora hago

``` bash
ps -e | grep ttys001
```

Vere el PID y el estado.
Puedo ver el padre, y el estado. Esta en T, est decir, que esta detenido. Como lo despauso?  
Con otra señal, que lo deje continuar. La señal es SIGCONT, la 19

Hacemos:

``` bash
kill-19 47420
```

Se pueden ver las tareas muertas?  
Si, pero es dificil, porque no estara en ese estado mucho tiempo. El Kernel lo marca y el scheduler pasa y lo ve, cuando tenga tiempo lo va a eliminar.  
EL scheduler tiene un modulo que se encarga de actualizar y acomodar el task array. Eso no es instantaneo, pero es muy rapido.

El scheduler hace todo lo que tiene que hacer en unos pocos milisegundos.

Al removerlo, tiene que desconectar los punteros que le apuntan. Decirle a su padre que ya no tiene hijo.

Cuando se lo manda a morir al procesador. Un proceso cae en el estado Defunct porque lo mato o porque termino. Cuando el kernel lo ve, desconecta, hace lugar, vuelve a conectar lo que haga falta y listo.

Para enviar la señal uno usa una llamada del sistema, una syscall.

La syscall es parte del kernel, por eso puede ir a tocar ahi. La syscall rapidamente lo marca. Una vez en estado D cuando le toca el scheduler lo saltea.

Lo que hace es: Lo que encuentre en ese estado lo remueve.

Puede pasar que alguna vez lo vea en ese estado.

El estado sleeping lo mas frecuente es que se de porque en el programa de usuario diga quiero leer un archivo o el puerto serie.  
Uno se queda con una funcion que lee del archivo hasta que haya algo.  
La funcion read o scanf. Mientras no haga nada, el proceso de scanf pasa a estado durmiente. Cuando se va a despertar? Cuando alguien toque una tecla va a producir el evento de cambiar de estado.  
En el caso de scanf esta en estado Sleeping interrumpible.  
Se podria despertar con una señal tambien.

La clase pasada hablamos del objeto del file system.

vimos que la 1er columna me indicaba el tipo de objeto de filesystem del cual se trataba. La rayita significaba un archivo que tiene contenido. Si tenia la letra D significaba directorio. con otras letras se codifican otras cosas, un puerto serie, un acceso directo, una conexion de red.  
Con la funcion open puedo abrir cualquier objeto y operar en ese objeto. Si bien siempre se dice en linux todo es un archivo, en realidad deberia decirse, todo es un objeto del file system. Cada pin puede ser visto como un archivo. Sobre el cual podemos leer y escribir. Lo puedo escribir de varias formas. No deja de verse representado en el file system como un objeto del file system.

Hay estados que estan durmiendo porque no le doy tiempo de CPU, pero hay otros que estan durmiendo y auque les de tiempo, no pueden hacer nada().

EL handler lo unico que hace es ponerlo en running.

Secuencia de Scanf(C)

Scanf es una funcion bloqueante. Cuando el scheduler detecta que es una funcion bloqueante lo saca de la lista de listos para correr.

Despues entra la prioridad de un proceso.

Un proceso puede tener mas o menos prioridad que otro.

Uno puede representarlo como que le da mas tiempo.  
No necesariamente le da todas las rodajas de tiempo juntas.

La prioridad tiene un nivel medio o estandar con la que van todos los procesos.  
Hay sucesos que producen que un proceso pueda aumentar o disminuir su prioridad.  Salvo que yo necesite por alguna razon cambiar la prioridad de un proceso hay formas de hacerlo.

Un sistema operativo grafico le da mas prioridad al foreground que al background.  Cualquier cosa que sea refresco de ventanas e interfaz grafica tiene que fluir por eso tiene mucha prioridad. El proceso que se encarga de traer trafico de la red va a tener menos prioridad.

(C)

La clase pasada vimos que un proceso puede crear otro proceso. Se llama fork porque produce una bifurcacion.

Cuando el proceso Q quiere crear un proceso hijo, usa la funcion fork, que es una syscall, es decir, ejecuta codigo del kernel. La task struct de Q se copia en un proceso Q' el proceso hijo suele ser un PID mayor al padre. es decir si Q = 80, Q' = 81.  
Es raro que pase, pero puede pasar que se interrumpa cuando esta haciendo la copia de la task_struct y le de un numero siguiente.

Es una copia entonces va a ejecutar el mismo programa. Tambien copia las variables.  Inicialmente copia todo. Para que esto sea util, tiene que haber una forma de que la logica del programa los pueda diferenciar.

La forma que nos da es que al proceso padre, fork le devuelva el PID del hijo y al hijo le devuelve 0.  
En principio pareciera que son iguales y van a a hacer lo mismo. Pero al padre le va a devolver un numero distinto de cero. Entonces le pregunto. Que me devolvio la funcion Fork? Me devolvio cero o mayor que cero? Si me devuelve mayor que cero va a ejecutar una seccion de codigo. Si me devuelve cero, va a ejecutar otra seccion de codigo. Cada proceso va a ejecutar una rama del if.

Yo programador tengo que tener cuidado de hacer esto.

Puede que fork falle, entonces me va a devolver -1.

``` c
#include <errno.h>
```

En info 1.
Vimos que es buena practica incluirla para que cuando falle una funcion syscall con -1.
Si lo incluimos se define una variable global del sistema llamada errno. Vamos a poder usarla. errno se setea a un valor que describe el error que se produjo.

La variable errno tendra el codigo del error.

errno pisa el error anterior. Pisa el valor de error del ultimo error.

Por eso es importante al hacer fork guardar el valor de fork en una variable.

``` c
pid.t p;
p = fork()
if(p>=0){ // no fallo
    if(p==0){
        codigo proceso hijo
    }
    else{
        codigo proceso padre
    }
else {
    // fallo
}
```

Hay otra funcion llamada __perror__, que tiene un prototipo casi igual al printf.
nosotros lo usamos como:

``` c
perror("se produjo el error: "); //tambien esta ubicada en errno.h
```

Cuando hacemos man fopen

en la descripcion de fopen dice: valores que retorna, si retorna -1 se produjo un error y la variable errno va a setearse en alguno de los valores, y explica que significa cada uno de los valores de errno.

man perror en la seccion 3 del manual, porque puede haber mas de un perror.

Todas las funcinoes que manejan archivos usaran el mismo valor para el mismo tipo de error.

___
Despues del recreo
___

``` c
cat perror.c

#include <stdio.j>
#include <errno.j>

extern int errno;

int main ()
{
    FILE * pfile;
    pFile = fopen ()

    if(pFile >= 0){
        if(pFile == 0){

        } else {

        }
    else {

    }
}
```

``` bash
./a.out
```

Hacer un programa que cree otro que ponga en pantalla yo soy el proceso padre, Mi PID es tal
Y el hijo tiene que decir yo soy el proceso hijo.

usamos las funciones:

``` c
getPid();
getPPid();
fork()
```

El siguiente comando me va a decir exactamemte el header que tengo que incluir para que funcione.

``` bash
man fork
```

El midnight commander es un administrador de carpetas en modo texto

Mi programa se llamo programa1.c

El programa que el profesor hizo es el siguiente:

```c
int main(){
    pid_t p;
    p = fork();
    if(p){
        sleep(8);
        printf("PADRE: mi PID vale: %d, el del bash es: %d\n", getpid(),  getppid());
        sleep(8);
        printf("PADRE: finalizando ejecucion.... \n");
    } else {
        sleep(5);
        printf("Hijo: mi PID vale: %d, el de PAPA es: %d\n", getpid(), getppid())
        sleep(1);
        printf("HIJO: finalizando ejecucion.... \n");
    }
}
```

Ambos procesos (hijo y padre) imprimen usando la misma ventana de terminal
el programa grafico es padre del interprete de comandos que se ejecuta en el.
El interprete de comandos hace un fork a si mismo y ejecuta mi programa.
Mientras corro esto puedo ver en otra ventana

Que era stdin, stdout y stderr?
Son file descriptors

cuando hacemos
open("miArchivo", a);
devuelve un file descriptor, que es un int y que no es exactamente un puntero.
Si devuelve un valor negativo es porque fallo algo. Pero supongamos que funciona.

lo guardo en una variable

``` c
int fd = open("miArchivo", a);
```

ahora lo quiero usar.

Hago:
buffer es un puntero a un buffer.
cant es la cantidad que voy a escribir a ese buffer.
write(fd, buffer, cant);

cuando lo ejecute, va a copiar la cantidad de caracteres cant que haya apuntados por el buffer.

file pointer que es?

nosotros vamos a usar exclusivamente:  
open, write, read.  
No fopen, ni nada de eso. Los usamos pelados.

fd es un puntero a una estructura struct_fd, que tiene punteros a los archivos que va abriendo.
Los punteros en desuso estan apuntando a null.

fopen devuelve un puntero, FILE *  
fopen esta mas orientado para abrir y cerrar archivos,  nosotros lo vamos a usar para todo.

Open sirve tambien para abrir conexion de red, un gpio, fopen no. Es mas generico.

Los primeros 3 casilleros del array apuntado por fd estaran casi siempre ocupados por stdin, stdout y sterr.

printf va a sacar por el file descriptor stdout.

stdin es 0 y stdout es 1 y stderr es 2
Es como si printf hiciera

``` c
write(1, "puntero al string", )
```

si hacemos esto, funciona.

Hay un comando de linux llamado strace. Que al pasarle otro programa, muestra todas las system calls que hizo el programa.

stdin, stdout y stderr estan asociados a un buffer que maneja el interfaz grafico de la ventana del terminal.

lo que leo de ese buffer lo interpreta como lectura de teclasos en el teclado.

por eso si hago read del file descriptor 0, va a ser como un scanf.

Fiscamente el codigo esta una sola vez. Lo que hace realmente el kernel es que las tablas de pagina de ambos procesos apuntan al mismo codigo.
Mientras ninguno de los 2 modifique un dato comun a los 2, el codigo esta en una sola pagina de codigo y ambos task apuntan a esa pagina. Cuando intento modificar alguna variable, salta un page fault porque estoy intentando escribir en una pagina que no debo. Y el handler se da cuenta de lo que paso y copia la pagina modificando lo que habia que modificar.

En el momento que hace fork, el tipo replica toda la estuctura, y al replicar la estructura,

Es importante entender la herencia de un proceso a otro.

El contexto de ejecucion del ancestro mas viejo se va pasando de generacion en generacion.



Abrimos 2 terminales.

Los linux tienen un a carpeta en la raiz del filesystem llamada proc, proc es processes. Y adentro tiene un monton de carpetitas con numeros como nombre.

Nos vamos a meter en alguno de los numeritos, por ejemplo en el del bash que estamos ejecutando
Luego nos metemos en fd

Ahi veremos que tenemos 0, 1, 2, y otro numero.

``` c
open("miArchivo", a);
```

Si hacemos echo hola > 1 descubriremos que logramos escribir en la otra terminal que teniamos archivo

Perror no usa el fd 1, sino el 2.

Si ambos procesos, padre e hijo hacen scanf, a quien le va a caer el teclazo?  
Al primero que lo agarre. Ambos procesos van a estar durmiendo esperando el teclazo. El que en la time slice en la que este liberado el dato en el buffer lo agarra, el otro no, porque una vez saca el dato del buffer ya no esta mas en el.

El operador:

``` bash
>
```

se llama redireccionador de file descriptor

## Ejercicio

Ahora planteamos una situacion.

Un proceso padre y un proceso hijo.

Van a usar un file descriptor para pasarse cosas entre ellos.
Pero queremos que no pueda verse en la terminal lo que se pasan entre ellos.

Lo que podemos user es un pipe.

El pipe se usa con el systemcall pipe(), la cual nos pide que le pasemos como parametro un vector de 2 enteros.  
En ese vector de 2 enteros va a devolver. Si falla devuelve un entero con -1, si funciona me devuelve 0.

El parametro va a guardar:  
en a[0] el extremo de lectura  
en a[1] el extremo de escritura.

Si estan usados 0, 1 y 2, vamos a guardar en el array, a[0] = 3 y a[1] = 4.

El padre espera un segundo y escribe. El hijo espera un momento y lee e imprime.
