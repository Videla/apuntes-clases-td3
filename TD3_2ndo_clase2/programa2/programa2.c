// Practica de fork + pipe

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

int main(void)
{
  int a[2];

  if (pipe(a) == -1) // compruebo si hubo errores al crear el pipe
  {
    perror("pipe");
    exit(1);
  }

  // printf("%d", (int*) a);

  int childId;
  childId = fork();

  if (childId >= 0)
  {
    if (childId > 0) // is parent
    {
      sleep(2);
      printf("Yo, padre, escribire en el pipe ahora.\n");
      write(a[1], "holas", 6); // escribo en el extremo de escritura
      close(a[0]);
      close(a[1]);
    }
    else // is child
    {
      sleep(4);
      printf("Yo, hijo, leere del pipe ahora.\n");
      char buffer[255];
      read(a[0], buffer, 6); // leo de el extremo de lectura

      printf("%s", buffer);
      close(a[0]); // cierro el extremo de lectura
      close(a[1]); // cierro el extremo de escritura
    }
  }
  else
  {
    perror("Hubo un error! El error fue: ");
  }

  return 0;
}