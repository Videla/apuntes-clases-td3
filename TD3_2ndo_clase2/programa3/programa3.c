// Progtrama que nos paso el profesor despues de la clase.

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

char s[30];
int fds[2];
pid_t p, q;

int PADRE(void)
{
  while (1)
  {
    scanf("%s", s);
    printf("PADRE:ingresado %s\n", s);
    write(fds[1], s, strlen(s));
  }
}

int HIJO1(void)
{
  while (1)
  {
    read(fds[0], s, 1);
    printf("HIJO1:leido: %s\n", s);
    sleep(2);
  }
}

int HIJO2(void)
{
  while (1)
  {
    read(fds[0], s, 1);
    printf("HIJO2:leido: %s\n", s);
    sleep(3);
  }
}

int main(void)
{
  pipe(fds);
  p = fork();

  if (p)
  {
    q = fork();
    if (q)
      PADRE();
    else
      HIJO2();
  }
  else
    HIJO1();
}
