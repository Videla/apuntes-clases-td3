# Clase 8 26-09-2019

Que hace el servidor?

Pide un socket.

Despues hace bind paa asociar al socket con una estructura. Que parametros hay que ponerle a esa estructura? El puerto de escucha, pedimos el 6500. Y la IP que le decimos asigname la interfaz default del sistema.

Una esta configurada por default.
El SO define una por defecto.

Despues de esto tenemos que preparar un nuevo socket y hacer el connect. Despues vamos a aceptar conexiones. Pero el accept va a habilitar sobre la variable ns un segundo socket.

El driver del SO a travez de la llamada a funcion que hace mi programa

Solo esta en escucha cuando se esta produciendo la funcion accept.

Esto es un servidor no concurrente. Mientras atiende un cliente, el servicio esta indisponible.

Cada vez que llamo a una funcion desde mi programa,

En la task struct de un proceso hay una variable fd que apunta a un campo donde hay punteros.


Los estados de un socket son:

- Desconectado
- Desconectado en escucha
- Conectado

El socket es un filed descriptor


El que le paso como parametro a accept es el que queda en escucha. Y el que devuelve es el que queda conectado.

todos los recursos que vimos exepto colas, semaforos y etc son file descriptor.


Antes simulamos el servidor con netcat ahora tendriamos que armar el servidor.



El TCP IP tiene una norma que regula la gestion de los paquetes.

En examenes suele pedirse que el servidor acepte solo las conexiones que tienen el numero de puerto determinado. Y en caso de que no sea el numero de puerto deseado cortar la conexion. Para eso hay que recibir la conexion, analizar cual es el puerto y si no es el deseado cortarlo.


Ahora veremos el servidor concurrente.

Vamos a hacer el programa de antes de forma tal que cada conexion que entra, el socket conectado se lo daremos a un thread. Podria ser con fork tambien pero es medio engorroso manejarlos, thread es mas problijo.

Despues del checkeo de error luego de aceptar una conexion, creamos el thread con:

``` c
pthread_create(threadId, funcion, -> parametros); // thread_id, nombre de la funcion y puntero a los parametros
```

que parematro fundamental hay que pasarle? El socket conectado.
Lo que hacemos es decirle al thread este es el socket conectado, habla por aca.

enseguida explicara el colchoncito de cosas parar tener conectado.


Despues de pthread hay que esperar otro cliente. Porque es concurrente. Lo hacemos con un while.

Funcion que hace? La llamaremos atiendeCliente

``` c
atiendeCliente(void*) //recibe un puntero a void, en el que recibe el puntero a los pareametros que le pasamos antes con pthread_create

atiende_cliente(void*){
    send()
    recv()

    shutdown()
}
```

estas funciones internas deben operar sobre el valor de ns al momento de despachar el thread. Porque al mommento de loopear va a pisar el valor de ns con un s



dibujo de taskt struct


para pasar el puntero a ns hay que castearlo

``` c
pthread_create(thread_id, atiendeSI, (void*)&ns);
```

``` c
atiende_cliente(void* cockli){
    int zokete;
    zokete = (int)&sockli;
    send(zokete, ---)
    recv(zokete, ---)

    shutdown(zokete, ---)
}
```

Ya no estamos solos dentro del proceso


Lo que no puedo saber es si al despachar el thread, que no es instantaneo 

El valor al que apunta sockli es el 4 que es el numero de file descritor del task_struct

Para poder pasar el valor del socket antes de que se genere un cambio en el valor del socket, hay que usar un mutex.
Dentro del thread hijo hay que tomar el mutex

El programa quedaria algo asi:

```c
main(void){
    int s, ns;
    int i=0;
    s=socket(...)
    bind(s, ...)
    listen(s, n);

    while(){
        pthread_mutex_lock();
        ns = accept(s, ...)
        pthread_create(thread_id, atiende_cli, (void*)&ns);
    }
}
```

Otra solucion es mediante el control del numero de conexiones maximas que puedo atender.
Cuanto es lo maximo que puedo atender?

Cual es el problema que yo arme un array de enteros

A que puerto esta conectado el socket?
El SO o el driver de TCP/IP del SO tiene una tabla de todos los procesos conectados.

PID |   PO    |   IPO      |   PD    |   IPD      |
    |   6500  |   10.0.0.2 |   827   |   10.0.0.7 |
    |   6500  |   10.0.0.2 |   7221  |   10.0.0.8 |

Cada proceso puede hacer open de hasta 256 archivos.
Si tengo un kernel que se banque mas, entonces si puedo tener mas.

Si son menos de 256 conexiones seguro, es mas facil de programarlo con thread. Pero en funcionalidad es lo mismo
Si quiero mas si o si necesito hacerlo con fork.

## Ahora veremos con fork

Lo que para con fork es que cuando o forkee me aparecen 2 task struct y ambas tendran un fd que apunta al socker donde esta conectado, con lo cual la copia padre de ns se puede cerrar, la copia hijo es la que se va a usar.
Cada uno cierra el que no usa.

```c
main(void){
    int s, ns;
    int i=0;
    s=socket(...)
    bind(s, ...)
    listen(s, n);

    while(1){
        ns = accept(s, ...)

        p = fork();

        if(p==0){ //soy el hijo
            // el hijo cierra el socket en escucha.
            close(s);
            send(ns, ...);
            recv(ns, ...);
            |
            |
            |
            shutdown(ns);
            close(ns);
        }else{ //soy el padre
            close(ns);
        }
    }
}
```

Para reusar codigo, en lugar del createthread ponemos el for y el if con el else y en lugar del if()//padre ponemos la funcion, para reutilizar le codigo.

Con esto esta resuelto el problema y aca tenemos algunas ventajas que son las siguientes.

No se llego a ejecutar y el padre ya esta ejecutando una conexion nueva.

Fork no va a pasar a la instancia siguiente hasta que no este copiada la task struct del siguiente proceso, esto se grarantinza por kernel. Este es un dato no menor porque me permite prescindir del mutex.

Cuando esta todo hecho recien va y se ejecuta.


Si se crea otro hijo mas (FOTO)

En el segundo parametro de listen esta la cantidad de clientes que pueden quedar en espera sin ser rechazados.


El buffer de pedidos de conexion se llama backlog y se guarda en la memoria del driver fisico.

Esto es porque cuando empiezo a atender muchos clientes. Cada paquete tiene una seccion de flasgs y uno de los flags es pedido_inicio_conexion. el driver lo ve y en la maquina de estados dice si esta en e la funcion accept entonces le contesta inmediateamente otro paquete de encabezado sin datos aceptando la conexion.


Cuando se le pone backlog y le llega un paquete y no esta en el accept dice, no le contesto, porque se que el tipo que hizo el pedido va a esperar varios segundos.

Cuando llega el momento de aceptar le responde el paquete de conexion al primero que estaba en la cola. El tipo en china le devuelve.
3 paquetes, 3 way handshake.

El accept espera el 3 way handshake.
Y recien ahi sigue con la siguiente linea de codigo, genera el fork y despues aceptara al siguiente pedido de conexion.


Si al padre no le llega la señal sigchild, porque no implemente el handler. El hijo queda zombie.
Para que no quede zombie tengo que ejecutar en el padre la funcion wait, que espera a que muera el hijo.
Entonces lo que hago es escribir una funcion handler de sigchild. Entonces si hay un hijo esperando morir, lo mata.

Cada vez que acepte una conexion tengo que incrementar el contador de hijos activos y cada vez que me cae el sigchild decremento el contador. La varaible tiene que ser global para que la puedan acceder de ambos lados.

Para cada proceso hijo hay que ejecutar una funcion wait. Porque cada proceso padre que hace nacer un hijo y no se hace un wait el hijo se queda ocupando espacio.

Hay wait o waitpi
wait espera por cualquiera y el waitpi espera por uno en particular.

Por proceso del kernel cuando muere un hijo se le manda siempre la señal sigchild al padre.

En la bjguides de interprocesscomunication.
