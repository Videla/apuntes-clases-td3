// https://gist.github.com/DnaBoss/68c0f663132792c2176d

#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>

#include <arpa/inet.h> //inet_addr
#include <unistd.h>    //write
#include <string.h>    //strlen

#define MI_PORT 6500

int main(int argc, char *argv[])
{
  int socket_desc, client_sock, c, read_size;
  struct sockaddr_in server, client;
  char client_message[2000] = {0};

  //Create socket
  socket_desc = socket(AF_INET, SOCK_STREAM, 0);
  if (socket_desc == -1)
  {
    puts("Could not create socket");
  }
  puts("Socket created");

  //Prepare the sockaddr_in structure
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_port = htons(MI_PORT);

  //Bind
  if (bind(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0)
  {
    //print the error message
    perror("bind failed. Error");
    exit(EXIT_FAILURE);
  }
  puts("bind done");

  //Listen
  listen(socket_desc, 3);

  //Accept and incoming connection
  puts("Waiting for incoming connections...");
  c = sizeof(struct sockaddr_in);

  //accept connection from an incoming client
  client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t *)&c);
  if (client_sock < 0)
  {
    perror("accept failed");
    exit(EXIT_FAILURE);
  }
  puts("Connection accepted");

  //Receive a message from client
  while ((read_size = read(client_sock, client_message, sizeof(client_message))) > 0)
  {
    //Send the message back to client
    puts(client_message);
    write(client_sock, client_message, strlen(client_message));
    memset(&client_message, 0, sizeof(client_message));
  }

  if (read_size == 0)
  {
    puts("Client disconnected");
    fflush(stdout);
  }
  else if (read_size == -1)
  {
    perror("recv failed");
  }

  exit(EXIT_SUCCESS);
}