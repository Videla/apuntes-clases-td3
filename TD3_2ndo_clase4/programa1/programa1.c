// Ejemplo de pipes
// Este programa es equivalente a hacer ls | wc -l.
// Se utiliza para mostrar como se escribe funcionan los operadores pipe |

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>

int main()
{
  int pfds[2];
  pid_t childId;

  if (pipe(pfds) < 0) // compruebo si hubo errores al crear el pipe
  {
    perror("pipe");
    exit(1); // cierro el stdin
  }

  if ((childId = fork()) < 0) // compruebo si hubo errores al hacer el fork
  {
    perror("Hubo un error! El error fue en fork child1Id: ");
    exit(1);
  }

  if (childId == 0) // hijo, es el que obtiene la lista de archivos y la redirije
  {
    close(1); // cierro el stdout
    dup(pfds[1]);
    close(pfds[0]); // cierro el extremo de lectura
    execlp("ls", "ls", NULL); // ejecuta el comando ls.
  }
  else // padre, es el que cuenta la cantidad de palabras
  {
    close(0); // cierro el stdin
    dup(pfds[0]);
    close(pfds[1]); // cierro el extremo de escritura
    execlp("wc", "wc", "-l", NULL); // ejecuta el comando wc -l.
  }
}
