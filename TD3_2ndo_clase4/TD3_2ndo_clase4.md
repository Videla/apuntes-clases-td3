# Clase 4 29-08-2019

BEEJ's guide:

- IPC
- Socket

Un proceso es un contexto, una instancia.

Un thread es una instancia de un flujo de ejecucion.
El thread es la granularidad minima con la que salta el scheduler.
Thread significa hebra o hilo, es una linea de ejecucion.
2 hilos pueden estar dentro del mismo proceso.
Un proceso tiene codigo, datos, pila. Una correcta identificacion de todo eso en el kernel.
Un thread es menos que eso, es la instancia que corre nada mas, no todo el contexto.

Proceso es intancia de codigo en ejecucion que tiene todo un sistema de referencia en el kernel. Y el kernel lo puede calificar con ese sistema de referencia en estados.

El proceso tiene una estructura con muhcas cosas, un campo de esa estructura es el PID. Esa estructura forma el contexto y se refiere a una instancia que es el proceso.

Gracias a que existe una instancia que es el proceso, la task struct le puede poner un estado al proceso. Uno de esos estados es running.

El scheduler esta haciendo round robin a travez de una cola de ejecucion. Esta cola es una lista doblemente enlazada circular.
Si el proceso esta en esa cola, esta en running.

Cuando un proceso se bloquea, el sistema operativo lo saca de la cola de ejecucion y lo manda a otra cola llamada cola espera. El scheduler no lo ve mas y no pierde ciclos de reloj en algo que esta en sleeping.

Como se levanta un proceso de sleeping?
Una interrupcion por lo general, es lo que levanta un proceso de sleeping.

El handler de interrupcion vuelve a poner al proceso en estado running.

Tambien un proceso que esta en sleeping puede ser vuelto a poner en la cola de ejecucion.
Sleeping se divide en 2 estados:
Interrumpible o no interrumpible.

Una señal, aunque no haya un enter de teclado.

Si el estado es uninterruptable no puede ser interrumpido por una señal y si o si tiene que ser vuelto a poner por una interrupcion.

Tambien esta el estado zombie, y que significa un estado zombie?

El parentesco de procesos tiene aparejado un intento de sincronismo.
Se penso el fork tambien para que despues del fork el proceso padre espere a que el hijo termine de hacer algo y despues continuar.

Por eso muchas veces se pone despues de fork un wait.
Eso es antiguo, hoy en dia se trabaja con procesos concurrentes, dejando que el padre siga.
El hijo informa cuando muere con una señal _syschild_.

Hay variantes de wait, pero lo importante es que el wait espera la syschild.

Nunca vamos a crear un padre y dejarlo esperando a que el hijo cree un archivo y despues continuar.

En los sistemas unix quedo un control de sincronismo de esta manera, por eso esta el syschild y el syswait.
Podria ser que el hijo cuando muera emita el syschild, pero el padre no lo espere con wait. Para morir el hijo tiene que hacer wait.

Parte de ese trabajo es enviar la syschild al padre.

Si el padre no genera esa acusa de recibo de recibir el syschild, porque no hace wait, en ese caso la task struct sigue quedando. El hijo queda en estado zombie.

Luego de la inicializacion del programa, cuando entra en regimen, en una especie de while. Podria es la inicializacion, decir que si me llega la señal syschild ir a ejecutar un hanlder. Signal se usaba antes, pero es vieja, pero ahora se usa _SigAction_ que es su reemplazo y tiene otra capacidad para recibir parametros.

Si le digo eso a un proceso concurrente, le puedo sacar el 

En la inicializacion hago sigAction y vinculo el sigchild con un handler.

```
sigAction(sigCHild, handler_child)

fork()
{

}
handler_child
{
    wait
}
```

De esta forma se logra que el proceso hijo no quede zombie. Asi tengo dos procesos concurrentes que no dejan zombies. Reemplazo el antiguo wait por un handler.

Se puede usar execl para dividir esto en dos archivos.

Aca terminamos con el estado zombie.

Ahora, siguiendo con esto, que pasa si mato al padre y el hijo manda un syschild? Queda zombie? No, el abuelo, que suele ser init va a recibir la señal.

Puedo matar al padre tranquilamente.

Tambien pueden matarse a los threads.


Para dar un servicio en red de forma concurrente necesito generar un proceso por cliente. Por eso necesito que haya un sistema IPC de control y por lo general en un servidor si algo se descontrola pueden generarse miles de zombie.
Lo mismo con defunct. Hagamos un programa que genere proceos, hacemos exit, ponemos sleep (C)

Linux genera zombies para que se pueda debuggear.

Tambien tenemos un estado stopped, y cual es la diferencia con sleeping? Que stopped esta parado a procposito y se para con una señal, que es sysstop. Esta esperando que lo reanuden. No espera ni una interrupcion ni salir de un evento.

Para que me serviria? Si tengo una maquina lenta corriendo un proceso pesado, lo puedo parar un rato asi me puedo centrar en otro proceso que quiera correr inmediatamente.

Todos los procesos que estan en 0% estan esperando una cola de ejucion, si no estan calculando, estan esperando eventos.

Lo mas normal es tener la mayoria de los procesos en sleeping interruptable y 2 o 3 en running.

Puedo tener varios en running. Running es que esta en la cola de ejecucion.

Sleeping esta esperando especificamente que yo lo reanude.

Que hace la instruccion hlt?
Pone al procesador en alta impedancia, en bajo consumo. Se pone en alta impedancia y deja de ejecutar instruccines. Se enfria. Cual es la unica forma de sacar a un procesador de hlt? Una interrupcion.
El handler corre y despues hace iret.




Vimos tambien copy on write.

Copy on write es la realidad de como funciona un fork. Cuando dicen que en fork son procesos independientes, no es inmediado que sean independientes. Al hacer el fork se crea otra task struct que apunta al mismo lugar. Hago eso para hacer mas rapido.

La task struct recien creada apunta al mismo lugar, entonces no tengo duplicado todo al momento de usar el fork. La diferenca es que se marca todo como read only. Entonces en el momento de que el hijo quiere escribir una variable, salta page fault y recien en ese momento el kernel le crea las paginas independientes al hijo, pero no todas al mismo tiempo, sino a medida que las que va usando.

Realmente luego del fork se termina teniendo dos procesos indpeendientes.

En el libro Understanding linux kernel estan todos los detalles. Por ejemplo si antes de que se ejecute el copy on write se mata al padre.

## Mecanismos de comunicacino entre procesos:

Primero vimos señáles
Hay una lista de señales existentes que se las puedo mandar a un proceso desde otro proceso o desde la consola del usuario.

Y yo puedo programar un handler dentro de un proceso para que .

Cntrl C tiene una señal asociada, sigInt.

El pipe muchas veces se usa para anular la salida standard (stdout) para mandarla a otro lado.

IPC(Inter Process Communication)
El pipe es uno de los tantos IPC.

Nunca resolver con el read no bloqueante. Usar los otros mecanismos que veremos.

## Pipes

El vector como lo creo dentro de un problema, me obliga, cuando lo creo en un proceso.

Un uso comun del pipe es cuando quiero redirigir la salida estandar (stdout) a otro proceso.

Cualquier programa por defecto me tira lo que hace en la pantalla. Yo podria querer redirigir esa salida y mandarla a otro lado.

Linux es muy modular. Si quiero que por ejemplo el comando "ls" me devuelva las cosas ordenadas o filtradas, la idea no es trabajar con switches dentro del ls. La idea es hacerlo modular y tener otro programa que haga el filtrado.

Por ejemplo: ```ls | grep hola```

grep es un filtro que si lo uso solo tengo que pasarle dos prametros. uno es un archivo, y el otro es el patron.

Linux permite comunicar distintos programas a traves de un pipe.

No le paso el archivo, porque le va a venir de otro lado.

Todo lo que tiran a pantalla es porque asumen el __stdout__ y todo lo que entra por teclado asumen el __stdin__. Y el pipe reemplaza el __stdout__ y __stdin__ respectivamente para poder mandar de un programa a otro.

En el directorio /var estan los archivos del log del sistema. Que cada vez crecen mas porque el sistema va registrando. se llama var porque su tamaño es variable.
Hay un progrma en linux que es el receptor y va metiendo todos los sucesos en el mismo archivo.
Lo que hacen todos los programas es mandar a un pipe y despues.

el > redirecciona a otro archivo.
el pipe relaciona entre procesos.

Ahora vemos un programa.

``` c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
  int pfds[2];

  pipe(pfds);

  if (!fork())
  {
    close(1); // cierro el stdout
    dup(pfds[1]);
    close(pfds[0]);
    execlp("ls", "ls", NULL); // ejecuta el comando ls.
  }
  else
  {
    close(0); // cierro el stdin
    dup(pfds[0]);
    close(pfds[1]); 
    execlp("wc", "wc", "-l", NULL); // ejecuta el comando wc -l.
  }
}
```

esta el ```execlp``` y ```execvp``` y las unicas variantes que hay es la manera mas comoda de pasar los parametros. En uno se pasan entre comillas, en otro se pueden pasar con un array.

Hacer un close(1) es cerrar un archivo que siempre tiene abierto que es archivo 1 es decir el stdout.
si intento hacer un printf no deberia funcionar y deberia tirar error.

El comando ```dup``` busca el primer file descriptor disponible y lo asocia a lo que le ponga como parametro.

si no le ponia close1 dup buscaria como un open y lo meteria ahi.

Ahora, como hice close(1), va a asociar y reabrir el pipe con el stdout.
Despues cierro el cero porque no lo uso. Porque estoy del lado del pipe que escribe.
Y despues reemplazo al proceso por el ```ls```

Y del otro lado hago exactamente lo contrario.
Osea, aca estoy haciendo lo que haria un shell internamente cuando ponemos un pipe __|__.
Para todo este tipo de cosas son los pipes sin nombre.

---

Despues vimos FIFO, tambien llamados "named pipes", entonces puedo comunicar programas que no esten relacionados.
Despues del recreo lo veremos y despues veremos algo mas avanzado. Un tema nuevo sencillo.

---
---
---

## Named pipes

Vamos a ver un ejemplo rapido de named pipes.
el file descriptor es un named pipe

Vemos primero el sender

``` c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

int main(void)
{
  char buff[200];
  int fd;

  if (mknod("/tmp/myfifo", S_IFIFO | 0666, 0) < 0)
  {
    perror("Error en mknod");
    exit(1);
  }

  if ((fd = open("/tmp/myfifo", O_WRONLY)) == -1)
  {
    perror("Error en Open");
    exit(2);
  }

  while (strncmp(buff, "FIN", 3))
  {
    printf("\nEscriba su mensaje: ");
    gets(buff);
    write(fd, buff, strlen(buff) + 1);
  }
  close(fd);
  return (0);
}
```

MKnod no es solo para crear la fifo, puede crear cualquier cosa.

A las entradas de linux se le llama inodos, a los objetos del file system. Para crear inodo se usa la funcion ```mknod```. Cualquier tipo de inodo. Pero con los parametros especifico lo que creare.
Cuando creo una carpeta por debajo hace un ```mknod```.
Cuando llamo a ```mknod``` le pongo un path.
666 son los permisos, permiso de lectura, de escritura, etc.

yo podria diretamente ejecurar sin el if el mknod con los permisos. Pero el mknod me puede dar un error. todas las funciones de linux pueden devolver un error.

S_IFIFO es un numero, una constante.
Falto crear el error.h

Hago un simple open a la fifo que tiene nombre. Se sabe que es una fifo, un archivo binario.


Esto que vimos es super viejo, en lo practica usaremos el IPC System 5 o System V.

Vamos a ver 3 cosas.

La cola de mensajes es como una FIFO pero mas sofisticada, puedo multiplexar. Me puede servir para que en multiproceso cada uno extraiga lo que corresponde.

Memoria compartida.
Tengo 2 procesos separados y declaro un puntero. Es como un malloc donde declaro memoria con un recurso compartido, entonces escribiendo en el puntero puedo hacer que el otro puntero del otro proceso pueda leer lo que escribe otro proceso.

Lo que hago con este recurso de memoria compartida es apuntarlo a una zona comnun.

El otro proceso no sabe. Una de las desventajas de la shared memory. Es que necesito sincronismo, porque uno no sabe del otro. Por eso memoria compartida trabaja junto con semaforos.


La cola de mensajes es como una FIFO mas avanzada, porque tiene orden.

Tiene varios campos.

``` c
struct msgbuf{
    long mtype;
    void* data
};
```

El segundo campo es un puntero void para poner cualquier cosa.
La idea de esto es que la estructura sea un bodoque de datos pero que tenga un encabezado que sera el mtype.

Osea es un dato con un encabezado que lo identifica, que es el mtype.
El resto es un bodoque apuntado por un void pointer.

Entonces lo que viajara por el buffer es un bodoque de datos con un encabezado.
el mtype me sirve para multiplexar.

Conviene usarlo como unidireccional.
el mtype se puede repetir.
es muy comun colocar en el mtype el process id.

Repite cosas y le pone el process id en el mtype.

El logger lee y sabe que proceso esta mandando informacion. Como se implementa? Se puede escribir, leer.

Estan las funciones de creacion, de lectura/escritura y de control.
No escriben ni leen, son funciones de control.

Veremos eso.
Funciones asociadas a las colas de mensajes.

todo esto se trabaja por set o por familia.

```key_y ftok(const char* pathname, int proj_id)```

Por lo general cuando trabajo con cola de mensajes no trabajo solo con cola de mensajes. Me conviene identificar a la familia. Por lo que lo priero que conviene obtener es una clave de famila. que se obtiene con una funcion llamada ftok, que recibe el path y tambien un numero de proyecto, que el programador elije. Devuelve un tipo key. Un tipo key es la redefinicion de un long. Pero poner tipo key porque si el dia de mañana se define difrente el tipo key, lo puedo cambiar mas facilmente.

despues si viene la funcion del creador de la cola de mensajes.

msgget()
le paso como parametro lo que me devolvio el ftok

``` c
int msgget(key_t, key, int msgflg);
```

me devuelve un entero que es el identificador de la cola de mensajes, no es un file descriptor, entonces no funciona con read y write. Por lo que tengo que usar msgsent y msgrcv

hay que mandar un a estuctura y es el segundo parametro, el puntero a la estructura

``` c
int msgsend(int msqid, const void*msgp, size_t msgsz, int msgflg);
```

No se acuerda por que esta definido como const.

La de recepcion es igual a la de envio con un agregado, la posibilidad de elegir que tipo de type quiero leer.

``` c
ssize_t msgrcv(int msqid, void* msgp, size_t msgsz, long msgtyp, int msgflg)
```

al ser de un tamaño desconocido la estructura, siempre tengo que pasarle el tamaño a la funcion.

La diferencia es el parametro mtype o msgtyp

Me va a leer el primero que tenga el valor 18. Entonces me permite multiplexar.
0 es un valor especial porque sirve en esta funcion para agarrar el primero, ahora si ponemos un valor distinto de cero ahi si multiplexa.

la funcion msgctl sirve para controlar.

Recibe un comando, que se puede ver en el manual, uno para borrar, y hace varias operaciones por la cola que son lectura y ecritura. Es una estructura especial que se llama message queue que tiene un monton de cosas para utilizar.
Con el id y eliminar ya esta. No tiene sentido que las explique. Con el man y la estructura ya estaria.
Podemos eliminar algo en particular, o acceder a la cantidad de elementos, etc..

Se puede leer sin eliminar a travez de los flagas. Pero no es tan comun en la practica. Es parecido al uso de fifo pero con la capacidad de multiplexar, es decir de elegir que quiero extraer.

Algunas costantes del comando de control son IPC_RMID, o sino IPC_STAT. Le pasamos un puntero a estructura vacia y me devuelve un monton de datos del status a que hora se creo, quienes se conectaron, etc.

## Pasamos a shared memory

Memoria compartida es otro esquea IPC. Es una zona de memoria que se reserva para ser accedida por varios procesos.

Si hago un Read y le especifico un mtype que no existe en la cola va a quedarse bloqueada. Y quizas otro proceso se conecto a la cola y (C)

Si pongo 0 se va a bloquear solamente cuando no hay nada.

Cualquier FIFO permite sincronizar.

La shared memory es memoria, no es una FIFO, es un puntero. Es una zona de memoria comun a varios procesos.
Como un malloc de varios procesos. Obviamente no estara en el area de datos de ningun proceso. Cuando hago malloc se le asigna al area de datos de ese proceso. La memoria compartida esta afuera de todo, por eso es compartida. Pero los procesos pueden acceder a esa memoria, con alguna funcion especifica puedo conectarla.

Con paginacion puedo referire a direcciones logicas que se traducen a direccinoes lineales.

La direccino lineal de las paginas se mapean como locales. El proceso las ve en el sistema como direccinoes locales. Eso lo puedo cambiar pero por defecto se ve como una direccion local y el sistema de paginacion es el encargado de mandarla. El proceso cree que es de su area.

Lo 1ro que tengo que hacer es crear la memoria compartida. Eso lo hago con:
hay una especie de convencion en System 5

``` c
int shmget(key_t key, size_t size, int shmflg);
```

Me devuelve un identificador de la memoria, un id.

Ahora tengo que apuntar mi puntero. Eso lo hago con shmat(attach) le paso el identificador para que sepa de que objeto estoy hablando. Despues la direccion. Le vamos a pasar siempre un valor por defecto. Y despues los flags. Y que e devuelve? Me devuelve el puntero Voy a poner de este lado el puntero local. Es void porque no se si lo quiero apuntar con un puntero entero, o de que tipo. Solo me cambia de que forma lo apunto con la aritmetica de punteros.

Con esta funcion vinculo el puntero local a la shared memory.
Vamos a poner null para no hacernos problema y que lo elija el sistema en que direccion local quiero que se mapee.
El puntero local tiene asignada una direccino local.

La lineal esta apuntando a la zona de datos del programa. Pero vamos a poner null, no nos complicamos con eso.

Si queremos desconectarlo le ponemos detach.

Siguiendo la misma nomenclatura etan las funciones de control.

## Semaforos

Creo el semaforo y opero sobre el mismo. Decirle necesito pasar y el semaforo, que va a tener un numero, por ejemplo 3. Cuando le digo necesito pasar, baja a 2 me dice que si. Si viene otro proceso, el semaforo baja a 1 y me deja pasar. viene un o mas y baja a 0. Cuando viene el siguiente el proceso se bloquea. Hasta que viene algo que lo desbloquea.

Sirven para sincronizar el acceso compartido a un recurso.

El semaforo no puede ir a negativo. Si intento decrementar con 3 un semaforo que esta en 2 no se decrementa y bloquea.

El valor de un semaforo representa la cantidad de recursos disponibles.

De que depende el valor que le ponga a un semaforo? Es un semaforo por proceso? por recurso?
Por recurso, el semaforo representa el recurso.

El recurso, si volvemos a la shared memory, hay 2 recursos en los que puede estar interesado.
A un proceso que escribe sobre la shared memory le interesa que haya lugar. Al proceso que lee sobre la shared memory.

El lector tambien tiene un interes, al lector le interesa que haya recursos nuevos. Si uno tiene que hacer un esquema con semaforos y shared memory, estaria necesitando 2 semaforos.

El recurso deberia estar asociado al tamaño de la shared memory.

Entonces que valor tendrian que tener mis 2 semaforos?

Ejemplo explicativo:
De el lado que escribe, tengo un while que va a tener un strcpy(&sb, cant).

Voy a tener un semaforo que me represente los huecos lobres y otro que me represente los elementos disponibles.

Al principio estara vacio cuando inicialize el sistema. Entonces el semaforo de huecos si tengo 7 lugares sera = 7, porque tengo 7 huecos libres y el de elemntos disponibles arranca en 0 porque no puedo sacar nada. Entonces que hago del while del lado que escribe? lo primero que hago es un semop(h-1), le digo, dame un hueco. Decrementandolo, escribire. Despues hare un decremento o decremento en el buffer, que ya lo conocemos.

``` c
while(){
    semop(h-1);
    strcpy(*sn, cant);
    semop(e+1);
}
```

Del lado que lee voy a tener un while con una funcion parecida, un strcpy que pone en local lo que encuentra en la shared memory.

``` c
while(){
    semop(E-1);
    strcpy(losi, sh);
    semop(h+1);
}
```

Hay un esquema muy tipico de semaforos que es cuando entrego por red audio.
Al servidor de streaming se le suben clientes.
Cuando escribo un servidor para un streaming, el servidor necesita leer al dispositivo real continuamente. esos Frames los va a tener que poner en una shares memory dentro del servidor que va a tener un monton de hijos. Cada hijo va a estar conectado con un cliente en la red.

Cuando un servidor ve que se conecta un cliente crea un hijo y lo deja al cliente hablando con el hijo.

Si el proceso padre no creara hijos podria atender a un cliente a la vez, no a muchos.

Me queda un proceso que accede al dispositivo, todos los hijos leen la shared memory. Como sincronizo eso? La velocidad a la que se van poniendo cuadros en la shared memory es mucho mas lenta que la PC. No es nada comparado con la velocidad a la que puede girar un while.
Hay mucho tiempo muerto. En audio por ejemplo, talvez tengo el conversor analogico digital mucho tiempo bloqueado hasta que envío el siguiente frame, el proceso hijo transmite el mismo cuadro muchas veces al cliente.

La idea es que tengo que bloquear a los hijos para que no lean 20000 veces el mismo cuadro.
Una vez que el conversor analogico disparo el padre pisa el dato anterior.

El padre se va a bloquear por el propio dispositivo que le entrega los datos.

Lo unico que importa es que los hijos no se queden repitiendo.

No pueden todos los hijos acceder al mismo dispositivo. A memoria no necesariamente tiene que acceder un hijo cada vez. Cuantos semaforos necesito?

El metodo mas simple es que haya 2 semaforos intercalados.

Los hijos tienen que pasar por los 2.

``` c
while(){
    semop A     //todos los hijos se clavan aca
    leer SM     //shared memory
    enviar UPD  //despues veremos que es UDP, seria como enviar al cliente
    semop B     //todos estan bloqueados hasta aca. El padre bloquea el semaforo A y bloquea el B
    leer SM
    enviar dato
}
```

Asi intercaladamente el proceso lector lee 2 semaforos.
Hay una manera que es la mas comun que es decrementar y otra que es operar sin tocarlo que ya la veremos.

``` c
int semget(key_t key, int nsems, int semflg)
```

me devuelve el semid que es el idenfitificador del semaforo


con semop opero sobre el semaforo

``` c
int semget(int semid, struct smbuf *sops, unsigned nsops)
```

``` c
struct sembuf{
    unsigned short sem_num;
    short sem_op; //operation
    short sem_flg; //generalmente sem_undo
}
```

nsops es la cantidad de elemntos sobre los que vamos a operar, siempore ponemos 1 ahi.

En operaciones sobre semaforos esta lo que dijo hoy sobre la operacion, es importante.

Si le paso 0 es el caso especial para usarlo como un flag.
0 no lo incrementa ni lo decrementa. si es cero no es bloqueante y si es cero bloquea. Este es el caso que serviria para el caso del streaming.

Solo bloquea cuando va con el mismo valor.

El padre leeaa un dispositivo(que seguramente este en dev)

``` c
while(){
    read("/dev/...")
    strcpy(SM, BUF); //shared memory
    semop A, 1
    read("/dev/...")
    strcpy(SM, BUF)
    semop A, 0
    semop B, 1
}
```

Con esto termina comunicacion interproceso (IPC)
