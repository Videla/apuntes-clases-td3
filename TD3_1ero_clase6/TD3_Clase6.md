# Notas clase 6 02-05-2019

Despues del recreo habra parte practica.

Podemos controlar aparicion con el master switch de las interrupciones enmascarables.
La forma de identificar las interrupciones en el procesador es con un numero que esta en la tabla tipos predefinidoss (5/106)

El tipo 2 es de la NMI. El resto o viene de pata xterna o viene por violacion, etc.

En el caso de las interrupciones externas, como identifico el numero de tipo?
Tiene que haber junto con intr un byte que viene mediante el chipset del motherboard que tiene controladores priorizados de interrupciones que hay que programar.

Se configura con 8 registros.
Nos habia dicho que lo busquemos en google como configurar este controlador de interrupciones priorizados.


Ahora ya vamos a modo protegido, no importa como sea que vino la interrupcion. 

Digamos que se genero una int N, que hace el procesador cuando le llega esta int N? El procesador estaba ejecutando otra cosa. No me interesa por que se produjo esta interrupcion.
Puede ser por interrupcion externa o porque se produzco una violacion de proteccion, alguien queria escribir un segmento que no debia, etc.
Tenemos una rutina de atencion de interrupcion(ISR). El procesador debe tener una tabla guardada con las direcciones de memoria esta tabla lo que tiene es una coleccion de punteros a memoria donde residen las ISR de cada interrupcion. Lo logico es que esa tabla este ordenada por numero de tipo.

Ahora veremos cuanto ocupa cada uno de estos vectores en la memoria, pero en principio, la tabla esta indizada por numero de tipo de interrupcion. 

La GDT, donde cargo los descriptores para todos los segmentos, que tienen que estar bien descriptos.
De la misma forma tengo que tener bien descripta la tabla de vectores de interrupcion.

No es de extrañar que la forma que tengan los punteros de la tabla sea una forma de descriptores de interrupcion. Lo que pasa es que un descriptor de interrupcion ocupa el mismo espacio que un descriptor de segmento, la forma en que esta acomodado el vector no es igual que para un descriptor.
Esto que nos cuenta lo hace automaticamente le procesador en un ciclo de maquina.

Por todos lados en internet vamos a encontrar una tabla de descriptores estandar.

Lo 1ro que haremos va a ser lo siguiente, vamos a escribir un pedazo de codigo que sera la ISR-comun, que sacara por la pantalla. Una UART estandar de PC son 8 registros. Podmos hacer que la simulacion de un puerto serie vaya a un archivo de texto. Se puede configurar el Bochs para que mande todo a un archivo de texto. Haremos una rutina que tire un mensaje que diga "soy la ISR de la int X", donde X se le tiene que pasar como parametro en algun lado.
Todos los descriptores de interrupcion apuntaran a la ISR comun.

Ahora veremos como es el formato de como se guardan los 8 bytes del descriptor de interrupcion.

Hay 3 sabores o 3 tipos de descriptores.

En algunos de los bits de atributos se codifica el tipo de descriptor. Dentro de esos 16 posibles tipos, 3 de ellos son los que describen a esos 3 sabores de interrupcion.

Se llaman:
- Interrupt gate
- Trap gate
- Task gate

Hay otro de los atributos que se llama S, que sean un descriptor del sistema significa que S tiene que ser = 0.

Lamentablemente hay un nivel de indireccion, porque el descriptor de interrupcion es un puntero indirecto.
Esto quiere decir que, (por supuesto voy a tener una GDT), es un puntero indirecto por lo siguiente. El descriptor de interrupcion tiene un selector que describe/ selecciona un segmento de codigo y este es el que apunta o describe el lugar donde esta la rutina de atencion de interrupcion. Esto es automatico por hardware en un ciclo de maquina, esta preparado para esto.
Lo que tenemos que hacer es (C).

El selector que estara dentro del descriptor de interrupcion selecciona un segmento de codigo qe selecciona la ...

Asi como hay un registro llamado GDTR que describe la GDT, la IDT tiene un registro, llamado IDTR que la describe y tiene el mismo formato que la GDTR, (tamaño-base). 

El segmento de codigo de la ISR comun, puede ser tan complejo o sencillo como queramos.

El nos recomienda que la rutina que imprime el cartel llame a una rutina que use el puerto serie. Hacerlo modular, para poder reacomodar.

Selector me selecciona un segmento a travez del descriptor.
El punto de entrada en el segmento, siempre va a entrar en el cero? En general, un selector que se usa como parte de la direccion logica, va acompañado de un offset, que seria el punto de entrada. Un selector en memoria ocupa 16 bits de los 64 que tiene el descriptor. Hay 12 bits del descriptor que son de atributos, me sobran un monton. En concreto un descriptor de interrupcion es una direccion logica y atributos. Los unicos atributos que tengo que poner es un descriptor de interrupcion, los atributos del descriptor de codigo van a estar en otro lado.

En la IDT esta guardada la direccion logica.
Todos con el selector seleccionan el mismo segmento. La direccion logica son 16 bits de offset, 32 bits de selector.

# Imagen 3
![3.png](3.png)

el selector tiene 16 bits, pero tiene el formato de los selectores de modo protegido. 
16 bits es 8192 descriptores. 
El descriptor de segmento de codigo es igual a lo que vimos cuando vimos GDT.

Intel decidio que en la IDT los descripores tengan este formato.

Lo que pondre en los descriptores de interrupcion seran direcciones logicas, por eso me fuerza a usar la GDT.

Una cosa mas vamos a ver: Logre entrar a la parte de ISR comun donde esta el codigo para int N.
Una instruccion atomica significa que no puede ser dividida, se ejecuta completa.
Una vez termino con la instruccion atomica, hace el recorrido y llega al codigo de la ISR, pero a donde retorna? normalmente hay una instruccion para retornar, llamada _IRET_ porque una interrupcion es como un call. call y una llamada de interrupcion van a usar la pila, pero la interrupcion ademas guarda los flags del procesador, porque la interrupcion en curso puede modificar los flags. Por ejemplo una interrupcion de comparacion altera los flags despues de ejecutarse, por ejemplo signo, cero overflow, carry. Si yo no guardo los flags, los puedo perder. Por eso como medida precautoria ademas de guardar en la pila la direccion de retorno, guardo los flags. El iret tiene en cuenta que tiene que devolver los flags.

Ahora hay un problema con el tema de ejecutar la instruccion siguiente, eso era cierto en el caso de la mayoria de interrupciones, pero resulta que con el sistema de proteccion activado o en el caso de la division por cero. Ver columna clase de 6/106

Imagen de 6/106.

En el caso de fallo:
En caso de division por cero por ejemplo, puede se que le indique que vuelva a ejecutar la ultima interrupcion.
Porque si todo sale bien, en vez de dividir por cero va a dividir por un numero muy chico, y por lo tanto podra devolver un valor valido.
Por eso a veces necesito que no vuelva a la instruccion siguiente, sino que vuelva a ejecutar la instruccion anterior, porque se corrigio algo del contexto de esa instruccion. 
Otro caso clasico es fallo de pagina, cuando vuelva a ejecutarse, tendra la pagina correctamente.  
Las que son de clase interrupcio, vuelve a la instruccion siguiente.
Las excepciones no son siempre en tipo fault o falla, la mayoria lo son, pero por ejemplo hay una que es de tipo abort.
De la 32 a la 255 no pueden configurarse como otra clase, esta determinado que son siempre interrupcion.

El registro IDTR funciona de la misma manera que GDTR.

El linker script hace que pueda dividir el programa en archivos. El linker script es para indicarle cada pedazo de programa que le vamos a dar en archivos separados como se va a ubicar en memoria. La GDT en algun momento se va a tener que hacer dinamica. Los programas nos van a ir pidiendo que podamos cargar y descargar cosas. Por eso nos pide copiar los ejercicios 2 y 3.
El linker script va ubicando las tablas en distintos lugares, etc. Con eso hacemos que se desenrosque correctamente el programa.



______

# Pila modo usuario.

Ahora empezamos a hablar con nueva nomenclatura, hablaremos de modo usuario o supervisor/kernel.

La idea es que vamos a escribir un software que es de un sistema operativo. Su funcion es administrar los recursos para que otros programas hagan uso controlado de esos recursos.
No en forma directa, sino a travez de rutinas que me provee el sistema operativo.

En particular en nuestro caso ya que esta pensado para 32 bits, lo que va a organizar es no solo los recursos, sino tambien el tiempo. Corre un programa, se termina su rodaja de tiempo  (time slice), le da el control al otro programa, etc. No dejamos al tipo acceder directamente al puerto serie, lo hace a travez de rutinas que le proveeremos. Tiene que haber una distincion entre el status social de el codigo que corre los programas de usuario y el codigo que corre el sistema operativo, a eso se llama nivel de privilegio.

Entonces habra codigo privilegiado y codigo no privilegiado.
Instrucciones privilegiadas y no privilegiadas.

Instruccion no privilegiada: mov ax, 4.
Instruccion privilegiada: lgdt.

Yo a un usuario no le dejo tocar la tabla de descriptores global. Bajo ningun concepto.
Si queremos ir al detalle, hay una presentacion anterior a esta que se trata sobre proteccion de memoria, en una parte de ese documento cuenta cuales son las instrucciones privilegiadas.
Otra privilegiada es CR0, las que tiene que ver con proteccion de memoria, todas esas instrucciones son privilegiadas, eso quiere decir que solo se pueden ejecutar en segmentos de codigo que esten marcados en el maximo nivel de privilegio. Los desriptores tienene entre sus atributos uno que es nivel "nivel de privilegio del descriptor" cuando me dejan correr codigo con ese nivel de privilegio quiere decir que soy codigo privilegiado y puedo.

El querer ejecutar instrucciones privilegiadas en codigo no privilegiado es una violacion de privilegio. Producen la int 13, llamada #GP (page protection).
El fallo de pagina es la int 14 #PF (page fault)

Cuando estamos empezando con los programas de interrupciones es muy frecuente la #DF. llego a cualquier interrucpion y al querer encontrar la interrupcion se produce una segunda.
La pantalla azul de Windows es el handler de la DF(doble fallo) se produjo una interrupcion en algun momento y tratando de atenderla de produjo este fallo.

Hay una teorica #TF(triple fallo) que yendo a buscar el handler de DF le vuelve a pasar lo mismo. Ahi es directo reset.

Si llego al handler y ocurre otra es anidamiento de interrupciones, si no llego a entrar al handler ahi se produce la #TF. Es una pregunta tramposa de examen.

Mas adelante en eta presentacion hay una descripcion exhaustiva de la int 8 que es una pregunta fija de la parte teorica del parcial. Pueden pedir un ejemplo practico de como se daria un doble o triple fallo.

Si encuentra que llego la int N y ve que no es ninguno de los tipos permitidos de interrupcion entonces se produce una int 8. Entra al handler de la int 8. Va a la GDT donde esta el descriptor de codigo .
# (C)


Nosotros vamos a usar el 0(mas privilegiado) y el 3(menos privilegiado) modos de privilegio. Los ARM por ejemplo tiene 2 modos de privilegio.

Si instalo un driver malicioso, puedo tomar completo control de la computadora?


Lo mas frecuente es que se este ejecutando codigo no privilegiado y se produzca una interrupcion.
Cada programa de usuario tiene su propia pila.

Y cuando se va a ejecutar el programa de ese usuario no tiene por que meterse en la pila de el.

Ahora. Esta pila donde cuando vino la interrupcion se guardo ahi. El sistema operativo tiene que tener una pila propia que solo pueda tocar el y en el nivel de privilegio mas alto.

Anota rapidamente donde volver y ya tenemos la direccion de retorno.

Si no tiene lugar la pila del programa, que pasa si se desborda? Produce una interrupcion de #GP, int 13, tratando de ejecutar una int N se ejecuta la int 13, entonces se va a ejecutar la int 8. Podria llegar a salvarse, si la int 8 se da cuanta que no alcanzo la pila y la puede expandir. Podria pasar tambien que termine pasando un triple fallo y se resetee.

Proximamente vamos a llegar en la teoria al punto donde vamos a poder contestar esto.

Entonces al irse de un programa, guarda todos los datos en la pila y toma el control del programa la rutina de atencion de interrupcion.

En el PDF, # 23/106, explica que significa cada tipo de excepcion. Puede pasar que ademas de EIP, CS y eFlags guarde un codigo de error.

Fault: Por ejemplo la division por cero o fallo de pagina. El procesador guarda en l apila la direccion de la instruccion donde se produjo la falla. Es decir, la vuelve a ejecutar.
Trap: Guarda la pila la instruccion siguiente. Esto sirve por ejemplo para hacer debugging por software. Esta no la vamos a usar.
Por ejemplo, tenemos Breakpoint y Overflow.
La 1 puede ser fault o trap dependiendo de una pata del procesador que tocan ellos. O por ejemplo de algun bit de los registros de control. entonces dependiendo del estado de ese bit puede ser que (C)
Abort: Excepcion que no siempre puede tener una descripcion de lo que causo la interrupcion, por ejemplo la Doble fallo. 
Ahora, luego de ese pequeño parentesis, saemos que algunas interrupciones pueden generar un codigo de error, ademas de pushear lo que esta ya en la pila, puede pushear un codigo de error.

En la columna codigo de error muestra que tipo produce un codigo de error y cual no. DF Produce un codigo de error pero siempre es cero.


### Para que sirve el codigo de error?
Cualquier violacion al sistema de proteccion produce una GP o int 13. Una vez vectorizado (llegue a travez de su vector a ejecutarla) el handler de interrupcion. Podemos determinar la causa para poder resolverla.

Esto nos lleva a pensar el ejercicio que nos planteo de armar la IDT, en el caso de la 13, poner: Soy la int 13 y mi codigo de error es tal.
Las que no tienen codigo de error solo pongan la int tal.

Esta desactualizado cuando hablan de capitulo y volumen del manual de intel. Donde esta muy datellado el codigo de error.

Para los codigos de error se usan los 16 bits menos significativos de los 32. Los 3 primeros son atributos los 2 primeros coinciden con el selector y el 3ro indica de donde salto en la tabla. 

Primero habla de los bits, los empieza a describir del 0 en adleante.
EXT: Si esta en 1 se produjo por algo externo al procesador.
IDT: Cuando esta en 1 indica que el campo Segment Selector Index o Indice al selector del segmento, se refiere a un descriptor de puerta en la IDT. Es decir que el codigo de error que aparecio aca fue por algo que paso en la IDT y me apunta a que descriptor lo causo.
Que pasa si esta en 0, no fue en la IDT, fue en la GDT o en la LDT.
Si esta en 0, el siguiente bit, TI, nos indica si fue la GDT o la LDT.
Puedo tener mas de una GDT, pero cuando me salta el error me habla de la que esta en uso, que es la que esta señalada por el GDTr

Los programas de usuario no se meten con las interrupciones, son victimas de ellos, o las producen, pero no las atienden.

Si estoy ejecutando una parte interna del Kernel.
La rutina esta determinando a que parte del codigo le corresponde ejecutarse.

Hay una diferencia entre una interrupcion que se produce mientras se ejecutaba codigo de usuario o se ejecutaba codigo del Kernel.

pag 29/106.
Si la interrupcion se produjo en modo usuario, aplica lo que dice ahi y el kernel toma el control.

Las rutinas de atencion de interrupcion forman parte del kernel del sistema operativo.
Conclusion: No hay cambio de contexto.

El contexto de ejecucion es como estan los registros del procesador en ese momento. Que estan como estaban cuando se interrupio. Pero si hay un cambio de nivel de privilegio de ejjecicion.
Ahora que estoy en nivel privilegiado puedo acomodar mas cosas.
(C)


Reglas de proteccion del procesador pag 30/106

No cambia el valor de por ejemplo SS y ESP. Siguen apuntanod a la pila del usuario.
Llega la interrupcion y el procesador le usa la pila al usuario. La interrupcion no produce un cambio de tarea.
Cambiar de tarea seria un cambio de contexto de tarea.

Entonces lo 1ro que hace es activar la pila de la 

Ahora SS y ESP apuntaran a la pila del kernel.

El ESP queda ahi señalando a la parte de error code de la pila del handler del interrupcion 32/106

Lo que se hace automaticamente es copiar toda la pila de usuario en la pila del kernel, entonces lo que hago es decir que los valores de SS y ESP apuntan a la nueva pila.

Cuando hacemos IRET dentro de la interrupcion, desenrosca esto solo.

El kernel tiene solo esta pila, una sola y no es que hay una para cada interrupcion.


Lo importante de esto es entender que no le puedo usar la pila al usuario.


Las PC actuales no tienen PIC, tienen ADIC(Advanced PIC)

Nos salteamos que cambia en 64 bits porque no lo vemos.

En la pagina 49 empieza a explicar cada una, de la 0 a la 32.

Prestar atencion a la interrupcion tipo 8, doble falla, que la usaremos. pag 60, 62.

A las excepciones las clasifica por el numero, en benignas, contributivs y page fault.
En pagina 61, explica este comportamiento
Si entro por overflow, es GP, vamos a la tabla anterior y vemos que int 13 es contributiva, la 1ra es benigna, la 2nda contributiva, las procesa en serie.

Vino la de overflow, tiro las cosas del usuario en la pila pero cuando las estaba tirando aparecio la #GP, y las procesa en serie, entonces hace primero la GP y despues la del overflow.

La 60 y la 61 hay que memorizarlas para el parcial.


Las paginas que faltan hasta la 106 son las interrupciones que faltan hasta llegar a la 106.

Tiene que ser una contributiva contra una no contributiva.

Puede ser error de division y Stack fault.

La de Stack de proteccion general es Stack fault, 12.

En el programa quiso hacer dir/0 prodice fallo de pila.

Overflow es desborde de otra cosa, no de un segmento. No de una pila.
Lograr la vectorizacion es poder llegar a ejecutar la 1er instruccion de la rutina de atencion de interrupcion.

Stack fault genera una entrada en el mecanismo de interrupcion, entonces me dice que vaya ...

Que se rompa un segmento de Stack es muy grave.

Depende, porque en compuerta de tarea hay un puntero de stack para cada nivel de privilegios.

Si yo intentando vectorizar la division por cero se me produce un fallo de stack se produce un doble fallo, si intentando vectorizar el doble fallo no llego entonces se produce un triple fallo.

Esta presentacion es un buen resumen del manual de intel. Son 30 paginas. Los manuales de intel son como libros de texto y son bastante amigables.

Ya terminamos la teoria de interrupciones. Despues sigue un poco una explicacion de los PIC.

APIC es el Advancen PIC.

Podemos inferirlo o buscar en internet la configuracion de los PIC.
El tiempo que nos queda abrir la practica y continuar.
