# Clase 13-06-19

Las imagenes son del PDF de 175 páginas.

Inicializacion

```
B8000h          80x40

|   8 bits      |               atrib
|   ascii       | BL | R  | G  | B  | I  | R  | G  | B  |
                     |      Back    |    |      Fore    |
```

![pag120.png](pag120.png)

Todas las entradas para traducir páginas que hagan referencia a donde esta el codigo de printf van a tener este atributo(global), lo vamos a necesitar en la practica. Si hacemos una prueba de velocidad se va a notar la diferencia.
El que tiene este atributo es mas rapido porque no pierde tiempo en hacer el cambio de tareas.

El atributo PAT no lo usaremos por ahora, lo dejamos en 0.

En cierto modo, las 1024 entradas de las DTP apuntan a tablas de 4MB cada una. Tiene un atributo de tamaño de página (PS: Page Size) nos dice si es de 4kb o 4Mb la página. Se sigue llamando DTP. Puede mezclar simultaneamente si uso paginas de 4k o de 4MB.

![pag118.png](pag118.png)

Hasta ahora vimos

![pag119.png](pag119.png)

![pag121.png](pag121.png)

D: Dirty, indica que la página fue modificada. Se setea de forma automática en cada escritura.
El algoritmo de SWAP usa este bit para que si no esta modificado lo manda al disco rígido.
Puede entrar en el parcial teórico.

El profesor mando finales de los últimos 10 años por Whatsapp.
Practicar con eso para el parcial.
No dar bolilla a lo que hable de procesador en modo 64bits.
Me avisa que no es consistente con la copia que esta en memoria virtual.

El procesador ya no ejecuta de memoria física, sino que copia en memoria estatica que es mas rápida. Cuando esta activado el controlador de memoria cache de nivel 1 y 2 ya se maneja diractamente por HW. Ayuda marcando con un bit en las entradas de tabla de página y directorios de tabla de páginas, que este sucia indica que no es consistente con lo que tenemos en memoria virtual.

De la misma forma que funciona el atributo Dirty funciona el atributo 'Accedido', que permite elaborar estadisticas de uso que permiten decidir cuando voy a sacar cosas de la TLB. De igual forma que marca las que estan sucias, el de accedido tambien lo hace de forma automatica. Los algoritmos eran el LRU y FRU para decidir cual va a tirar o intercambiar.
Cuando tengo activado el manejo del procesador. Lo maneja solo el procesador, no me tengo que preocupar.

![pag122.png](pag122.png)

Usuario menos privilegios. cuando en modo usuario quiero ejecutar lgdt, salta el sistema de proteccion. Porque no se pueden ejecutar instrucciones privilegiadas.

El procesador chequea el CPL que es el nivel de privilegio que tiene en el descripor interno del procesador el CS.

El procesador tiene registros de segmento:

SS
DS

CD

Cada uno tiene atributos

```
CS   |  L   |   B   |   AH| | |
                           |
                          CPL
```

Si nosotros somos el sistema operativo, CPL sera 00.
El procesador chequea cual sera el valor de CPL.
Cuando tiene CPL = 1 y encuentra una pagina con modo supervisor activado, no podra acceder a nada de la pagina y se produce una excepcion.

Hay diferencia entre fallo de pagina y fallo de permisos.
En fallo de página no puedo acceder a las

Si no tengo permiso de escritura e intento escribir es fallo de página.

P: Indica si la página esta en memoria, por que no podria estar en memoria? Porque podria estar en memoria virtual.

Con esto terminamos paginacion de 32 bits.

Ahora veremos los métodos para poder acceder a mas memoria física.
Primero en los ejercicios hacemos identity mapping.
Despues no identity mapping y la evolucion de eso es con PAE.

![pag124.png](pag124.png)

Permite trabajar hasta 52 bits de memoria Física.
Hay una instruccion llamada CPUID

Primero hacemos un
mov ax 0x80000008
CPUID
Lo que nos muestra es el número de bits físicos.
Otro numero podemos poner y nos muestra el modelo del procesador. Lo hace con CPUID.
En el manual de Intel nos va a mostrar que nos devuelve cada codigo de CPUID. Y nos dice si devuelve tanto significa tanto.

Cuando trabaja con 64 bits lo hace si o si con paginacion activada.
Cuando trabaja con 32 bits puede hacerlo con segmentacion o paginacion.

![pag125.png](pag125.png)

Que era PGE? PGE: Page Global Enable.
El bit PAE es el bit 5

![pag126.png](pag126.png)

Lo que me dice es que hay una tabla en una posicion de memoria que esta alineada en memoria en 2^5, cuando la buscamos en memoria tienen que estar en 0 los últimos 5 bits.

Ahora partira la direccion lineal en 4.

![pag127.png](pag127.png)

Parte a la direccion de 32 bits en 2 bits para direccionar los 4 DTP que estan en la PDTP.

El esquema esta en:

![pag130.png](pag130.png)

CRC la puedo ubicar con mucha granuralidad en la memoria física.
