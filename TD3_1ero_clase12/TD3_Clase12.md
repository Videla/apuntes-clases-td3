# Clase 12

Queremos implementar multitarea en el procesador.
Hay varias formas de implementarlo.
Se puede hacer a lo bruto, poniendo un timer que interrumpa cada cierto tiempo y guardo a mano cada registro, guardo el contexto del programa que interrumpi, termina, corre el programa que tiene que correr, hasta que viene la interrupcion, guardo el contexto que esta corriendo en ese momento, etc.

O podemos usar algo que Intel provee en sus procesadores que lo hace casi automaticamente.
Linux tiene una rutina que copia todos los registros del procesador a una zona de memoria, por que no lo hace por hardware?
Podria ser para optimizar, para poder elegir que copio y que no.
Hay otra razon mas comercial, que es para hacer al sistema operativo multiplataforma.

En el conetxto esta el CR3 que describe todo el sistema de paginacion.
Uno puede decidir que todas las tareas tengan el mismo sistema de paginacion, pero eso no es practico, no se aprovecha al maximo las capacidades del procesador, podria usar la memoria mas eficientemente si tuviera un CR3 por tarea.
El contexto del programa para nosotros es los registros del procesador por ahora.

El scheduler tiene asociadas unas variables o vectores condatos donde registra las tareas que ejecuto y cual viene despues.
Cuando empezo todo, la roja indica en la lista la tarea 1
Cuando decide cambiar de contexto, pasa a apuntar a la tarea 2.
Y le dice de donde tiene que sacar el contexto para la siguiente tarea.
La lista va a tener 3 tareas, el sistema operativo y 2 tareas mas.
Uno puede complicar el scheduler realizando prioridades. Y elemento de la lista pasa a ser una estructura.

Hay que entender el modelo.
Intel sugiere un modelo de programacion donde habla de esto.

El cambio de trabajo le insume mucho tiempo al procesador, porque debe escribir 15 veces en memoria, una vez por cada registro.
Tengo que empezar a tener en cuenta el tiempo de cambio de contexto.

Al programa hacerlo por hardware le va a costar un monton de ciclos de maquina, vamos a usar una sola instruccion.

Cuando Linus Torvalds escribio linux, lo hizo usando el mecanismo de cambio de tareas de Intel, pero luego, lo hizo a mano, eso permitia optimizar, copiar solo los registros que uso, correr mas rapido.

En la guia lo vamos a tener que hacer de ambas formas, a mano y con el scheduler de Intel.

El tiempo de cambio de contexto tiene que ser al menos como mucho un orden de magnitud menos que el tiempo de tarea.

Las tareas pueden no durar lo mismo. Los tiempos del scheduler deberian ser todos iguales.
Aproximacion de cavernicola, a todos le tocan 10mSeg, los necesite o no.
2nda aproximacion, cuando una tarea dice ya no la uso mas, o cuando yo estoy esperando un dato o algo externo.
En el 1er scheduler teniamos que esperar que se cumpla el tiempo. En la 2nda forma podiamos ver que cuando la tarea no tenia nada que hacer llamaba al scheduler.
Si una tarea tiene que esperar a un evento, llama al scheduler.

En el segundo cuatrimestre veremos la diferencia entre un SO de tiempo real y uno No real time.

Algo que estamos haciendo de forma ineficiente es el manejo de la memoria, estamos ocupando mucha memoria.

No solo tengo que administrar eficientemente el cambio de contexto, sino tambien encontrar y decidir rapidamente a quien le toca.
Hay un limite para esto. Hay un limite no solo de memoria, sino tambien de tiempo. si todas las tareas usan su Time slice al maximo, cada una usa 110mSeg, hasta que si tengo muchas tareas voy a tardar mucho en volver a la primer tarea.
Ahi tiene sentido que la tarea que mas prioridad tiene en el procesador sea la de mover el mouse, para que se vea de forma fluida.

Cada programa que lanzo a ejecutarse no solo ocupa memoria con el codigo y dato sque maneja sino tambien.
Cuantos bytes puede ocupar todos los registros del procesador. 8x16bytes seria mas o menos todos los registros del procesador

#### Slide definciiones tarea, espacio de ejcucion

Los 2 puntos siguientes no siempre son asi. Handler de interrupcion y sercicio de kernel.

Llama __contexto de ejecucion de la tarea__ a los valores que deben tener los registros del procesador cuando se ejecute la tarea.

__Espacio de contexto de ejecucion:__ Es la memoria donde guardo el contexto de ejecucion
Ya que intel nacio usando segmentos, (los seguemtnos siguen existiendo aunque este activa la paginacion). Inlel define que el espacio de contexto de ejecucion se guarda en segmentos, que ese segmento coincida con una pagina es casualidad.

Ahora vamos a tener que usar segmentos separados que no se toquen. Se termino el modelo FLAT.

Con que se va a empezar a poblar la GDT? con un monton de descriptores de esos segmentos, que dependiendo del diseño del sistema operativo se cargan dinamicamente o no.



En la GDT (que es para describir segmentos) se guardara

Descriptor de codigo para el kernel
descriptor datos para el kernel
descriptor de dato para guardar el contexto del Kernel

descriptor codigo para tarea 1
descriptor datos para tarea 1
descriptor de segemento para guardar el contexto de la tarea 1

Hay un tipo nuevo de segmento

Cada descriptor de segmento tiene atributos, y hay uno que dice tipo de desctriptor, habia uno llamado TSS Task State Segment.
Intel nos dice, defini para cada programa que quiera correr.

El espacio de memoria donde guardaba el estado del procesador, intel lo resuelve mediante TSS, y se codifican mediante bits de atributos.

Los segmentos especiales TSS guardan segmentos de datos. guardan contexto, que son registros del procesdador.
Como es automatico los va a guardar siempre en el mismo orden. La forma de guardarse es una sola.

#### Slide El TSS implementa el Espacio de Contexto

SS2, SS1 y SS0 son para apuntar a un segmento, para encontrar un punto dentro de ese segmento usa los ESP.
Hay 3 punteros a la pila. cuando veamos niveles de privilegio veremos cual usar. Cada uno representa un nivel de privilegio. El nivel 3 de privilegio esta representado por el SS y ESP que estan mas arriba.

Intel nos provee un gran nivel de detalle cuando hacemos el cambio de contexto.

Hay registros en los que no puedo hacer mov directo.


Esta tambien CR3(PDBR(Registro base de directorio de pagina)). 

Hay que ponerle al tamaño del descriptor por lo menos 104 bytes. Puede ser mas si quiero guardar datos ahi. Pero nadie lo usa ni nos van a pedir que lo usemos. Pero hay que tener en cuenta que todo lo que dice reservado no se puede tocar.

Podemos buscar tiempo de cambio de contexto en el manual de intel. Originalmente tardaba ciento y pico ciclos de clock, ahora tarda menos por el avance de la tecnologia y las menoras.

La primera vez que ejecutamos un programa tenemos que precargar valores en la TSS. Aunque despues los cambios de contexto sean automáticos.

El registro TR, que se un selector, apunta al descriptor de la GDT que tiene que ser un TSS del kernel
Cuando el kernel quiere conmutar a una tarea, 
El EIP del procesador se tiene que cargar con el EIP que esta en el TSS.
Es un mov, le carga un valor a TR. Le cargamos un valor a TR que sea coherente, tiene que ser un selector que selecciona el TSS al que quiero ir.

Va a ejecutar la instruccion que este apuntada por el TSS y el IP.

Por eso antes de lanzar la tarea hay que precargarle (C) y eso se hace con el linker script.
En el .asm tendemos a pensar que la siguiente instruccion es la que sigue a aquella que hace: mov TR <- X;


___

___

# Luego del recreo

Cuando hacemos un cambio de contexto se actualizan todos los registros.

El segmento que estaba usando para la pila pasa a ser otro.

El IP tambien cambia.

Las tareas de usuario corren en un nivel de privilegio mas bajo.

Los registros del procesador se van a guardar si hacemos push y pop.

Hay muchas formas manuales de hacerlo, si no cambiamos el nivel de privilegio no podemos ver la potencia que tiene esto.

El programa de usuario que llame tiene acceso a la pila, etc.

La idea es que el programa de usuario no tenga forma de tocar los segmentos de datos que usa el sistema operativo, que no se pueda salir de sus restricciones el codigo que ejecuta.

Hay una forma de lo que yo llame conmutar tarea a mano. Cambiar de contexto cambia todo, no solo los registros, solo puede acceder a lo que tiene definido ahi. Cualquier cambio que quiera hacer, explota.
No me protege de nada.

Tenemos que darnos cuenta de la necesidad de poder ejecutar codigo sin que el mismo crashee mi sistema.

Se pueden combinar las 2 cosas para lograr esta seguridad, combinar privilegios de tareas y paginacion.
Paginacion es lo practico y lo que se usa, tareas no se usa mas. Porque todos los procesadores modernos manejan paginacion, no todos los procesadores modernos manejan tareas.

Entonces para mayor compatibilidad, implemento mi sistema de cambio de tareas con paginacion.


Cuando se produce el cambio de contexto cambia todo en el procesador, cuando voy a correr una nueva tarea, le voy a tener que decir, CS esta aca, todo eso lo vamos a guardar en la memoria para esa tarea, tengo que armar tambien un descriptor de tipo TSS para ese TSS, le damos nivel de privilegio, etc... Una vez que hice esto, hago una instruccion con la que TR selecciona el ---- del registro que quiero ejecutar. Y le cambia el CS y el EIP al procesador. Y automaticamente va a amanecer corriendo el codigo de esa tarea nueva.
La siguiente instrucicon no va a ser la que siga a la instruccion mov TR, X, sino la que corresponda a la tarea.

No es la unica forma de hacer un cambio de tarea, uno puede hacer un call intersegemento. Y uno puede pner como destino de esa llamada un TSS. Y eso tambien genera un cambio de tarea. Cuando hagamos el call vamos a ir al segemnt... No llamamos al Cs sino al TSS, el TSS dice cual es el segmento de codigo que se va a cargar.

El segmento de codigo de la GDT esta en ese lugar.

Cuando uno retorna, se hace el ret, y al retornar, vuelve a la tarea que hizo el call, que probablemente sea el Kernel.

Tareas

Call T1
call T2
call T3
loop tarea

Esto seria un SO de tiempo real, que no se usa para cualquier aplicacion, se usa para aplicaciones que sean conscientes del tiempo.
Con cada cambio o call puede cambiar todo el esquema de paginacion.

Intel propone esto, que uno haga el call y el sistema resuelva el resto solo.


En la IDT podiamos poner descriptores de varios tipos 

De inter gate
trap gate
task gate: Son para que si pongo una interrupcion de timer, el handler del timer sea una tarea que funciona con esto, con TSS.

Todos los descriptores tienen un selector, el task gate apunta a un TSS.
Si a los 100 mSeg no largo, viene una interrupcion de timer.
Estas son las tareas de usuario y hay una 5ta tarea que seria el watchdog. Si cai aca, si apareci, quiere decir que una de las tareas se colgo.
Lo que tiene que hacer es entre cada tarea ejecutar el watchdog. Si antes de los 100mSeg vuelve a la tarea que mande...

Por interrupcion se cambia de tarea a una tarea timer. En ese punto debo determinar por que llegue a ese punto, que debe ser porque alguna de las 3 se colgo.

#### Seguimos avanzando

Vamos a seguir viendo que otras coasa hay en la TSS
Previous task Link, ocupa 16 bits, que como ocupa 16 bits que puede ser? Probablemente un registro selector, y efectivamente, lo es.
Si tiene task en el nombre seguramente sea un selector de TSS, y efectivamente lo es.

Tambien tenemos un I/O Map Base Adress, que nadie usa, pero esta en la teoria y nos lo pueden tomar.

Los privilegios son 0,1,2 y 3. Solo lo que esta en el nivel de privilegio 0 puede ejecutar instrucciones privilegiadas, una de las instrucciones que solo pueden ejecutar los de nivel de privilegio 0 son las In/Out.
Intel de todas formas me permite que un programa de nivel de privilegio 3 pueda acceder a las In/Out.
En el descriptor dice el nivel de privilegio que tiene (C) intel en un caso particular, me puede dejar usar los In/Out. Para que me deje hacer In/Out a la placa que yo como usuario diseñe.
La forma de hacerlo es mediante TSS y la forma que hay de indicarle los puertos que puede usar o no es con el I/O Map Nase Adress, donde coloco un puntero de 16 bits, que son los ultimos 2 bytes de los 104 bytes de la TSS. Estos 2 bytes son la entrada a una TSS extendida, donde cada bit representa que puede hacer In / Out a cada uno de los puertos de entrada/ salida.
En el micro hay 64k puertos.
En la PC hay del 0 al 0x3ff. Del 0x3FF para adelante, y dependiendo de la tecnologia, porque puede que necesite mas puertos la PC, puedo mapear la placa que yo diseñe.

Yo quiero que el usuario tenga acceso al byte 0? No, entonces le pongo 0(no esta disponible). Como del 0 al 0x3ff no estan disponibles, los pongo en 0 tambine.

Cuando uno pone 8 en 1 seguidos, lo corta. No disponible = 0.
No importa si no sabemos como se hace esto, lo que hay que saber es que puedo poner en el TSS un mapa de bits que diga lo que puede y lo que no puede usar.

Aparte de esto, hay que poner el mapa.

En cuanto a EFLAGS, hay 2 bits que se dicen IOPL (Nivel de privilegrio I/O). Yo le tengo que poner el nivel de privilegio. si falta alguno de los 2, no puede usar las intruccinoes IO.

En este caso la TSS mide mas de 104 bytes.
Antes el dijo que la TSS puede tener cualquier tamaño, porque ademas puede guardar datos la TSS.
Se puede guardar cosas entre los 104 bytes y el mapa de bits.
Nadie usa el espacio de almacenamiento extra ni el mapa de bits en la practica.



#### slide 18/36

el descriptor de un TSS es igual en estructura que un segmento de datos o de codigo, lo que cambia es la codificacion del tipo.
10X1 son de tipo TSS.

Cuando lo veamos en la tabla codificado, vemos que.
Puede ser
1001
1011
--B-

Cual es la diferencia entre ambos? Que la tarea que hace el call esta ocupada. Porque la tarea 1 se puede marcar como ocupada, la tarea ocupada es la que hace el call.

T1:
...
...
...
ret

T2:
...
...
...
call tarea aux

Ahora ambas tareas estan ocupadas.
Esto es para que en un error de kernel no pueda (C)
Habia un bit que tenia que ser S.

Si abrimos la tabla completa, hay 32 codificaciones posibles, porque son 5 bits, 4 codigican el tipo y el 5to es el S.

Despues va a ser uno mas como la GDT, que tendra una base y un limite que sera 67h va a ser el limite siempre porque es 103(en decimal).

Todos los descriptores tienen un bit P. Para nuestro efectos practicos, siempre esta en P.

Si el bit P esta en cero, genera una interrupcion de bit no presente.
Por ahora no hay practica donde tengamos que usar P en 0.
Supongamos que dejamos predefinido un TSS en tarea de usuario.
Al TSS que esta en memoria lo cargo con P en 0.

Un descriptor de la GDT describe al TSS.

Esto nos muestra como TR es un selector que puede apuntar a TSS.

Todos los selectores tienen un cache interno donde se guardan cache, atributos, etc.

Esta usando el TR para señalarlo porque carga una tarea.

Por eso antes de cargar un valor en el TR, tengo que precargar los segmentod de datos y codigo que se van a ejecutar.


#### Task gate: otra forma de acceder a una tarea

Puedo cambiar de tarea usando una puerta de tarea, que es un __descriptor de tarea__.
Puedo cargar una puerta de tarea en una IDT, LDT o GDT.
Dentro tiene un selector de TSS, ... y nada mas.
Lo puedo hacer para cambiar de tarea mediante una interrupcion.
Lo que esta en GDT se puede cambiar por programa.
Un programa quiere atender una interrupcion como si fuese una tarea.
El mismo descriptor lo puedo poner en cualquier lado, entonces puedo correr una tarea como si fuese una interrupcion o como si fuese una rutina.
Como llego a la ... con int3, puedo poner una puerta de tarea a una int3, entonces si un programa intenta ejecutar una instruccion privilegiada sin tener privilegios, salta la int 3 y encuentra la puerta de tarea y dice, se va a resolver la interrupcion usando la tarea, es decir, llama al handler de la interrupcion. YA que tenemos cambio de contexto con tarea, lo meto en todos lados. Da facilidad para escribiri y reutilizar codigo.

Si el DPL = 11 .... Guardamos este comentario para cuando veamos niveles de privilegio la proxima clase.

No hay offset en el task gate, porque va a ir al EIP que este en la TSS, va a ir al CS y EIP que esta en el TSS.
De hecho cuando hacemos un call, no ponemos un offsete, hacemos: call TG1:offset. porque va a (C) Nosotros hata ahora haciamos un call dentro del segmento. el procesador despues no lo usa.
Como sabe que el offset no lo tiene que usar ni siquiera lo mete en la memoria. El va a ir a donde diga el TSS del taskgate.

TSS es un segmento de datos en la memoria.
Es un lugar en memoria donde se guarda el contexto de la tarea. Y a la vez es un segmento, como todo segmento, tiene que tener un descriptor en una tabla de descriptores, un TSS se puede poner solo en la GDT o en la LDT. No puede ponerse en la IDT. Es un segmento de datos, pero no lo puedo usar como un segmentos comun.

Si queremos podemos hacer la conmutacion de tareas cuando hacemos un TSS.
El selector de la TSS no coincide con el Task register, cuando el task register cae ahi se carga con el valor del selector de TSS.

#### 21/36

Asi como puedo hacer call tarea 1 puedo hacer jmp tarea 1. Que no se puede volver, a la tarea anteror. No puedo hacer ret para volver. Es la misma limitacion de hacer un jmp convencional en codigo convencional.
Hay una forma de hacerlo, per

Por una int 13 podemos poner una taskgate y llegar a un handler de una interrupcion.

Si cambio de tarea con un call, vuelvo con ret.
Si cambio de tarea con interrupcion, vuelvo con iret.

El bit NT es Nested Task.

#### 22/36

#### 23/36

En CR0 hay 2 bits importantes, habilitacion de proteccion y paginacion, entre otros, entre esos otros esta el de tarea conmutada. Es importante recordar para mas adelante que este bit señaliza que se cambio de contexto.

#### 24/36

Nos muestra el descriptor de cada una de las tss, para mostrarnos como usa el previous task link de la TSS y para mostrarnos el bit B de cada descriptor de TSS, el previous task link nos muestra donde volver, porque no tengo pila que me indique donde volver.


T1
...
...
...
...
...
...
call T2
...
...
...
fin de T1

T2
...
...
...
...
call T3
...
...
...
...
ret

T3
...
...
...
...
...
...
...
ret

jo vamos a dibujar en la GDT para que quede mas patente.

El PTL es un selector, se usa para guardar un selector, que selecciona el TSS de la tarea que me llamo.
Si la T1 llama a la T2, cuando el procesador hace el cambio de contexto, guarda en el PTL de la tarea 2 un selector que selecciona el descriptor de segmento de la TSS de la tarea 1.

Mientras esten anidadas se mantienen en Busy.

Falta ver el bit B.

Lo que dice el manual es, cuando se hace un call queda ocupada la tarea que hace el call.
Mientras se mantiene ocupada, pone en 1 el bit B. El bit B se pone en 1 o 0 automaticamente por hardware.
El bit busy especifica si la tarea esta ocupada con un call o no.

Si por algun motivo interno o externo, por interrupcion o porque la tarea lo llama se llama a la tarea 1 desde la 3, al momento de llamarla vera que esta ocupada, y evitara una cosa recursiva que no esta permitida a nivel de hardware. La recursividad como vemos en info 1 y 2 esta implemnetada en la logica del compilador. La recursividad se puede hacer por software. Si eso pasa se genera una excepcion de tarea ocupada. ESto es facil, hay que saber donde estan los bits.

El procesador aparte de todos los registros de proposito general tiene algunos que son de punto flotante, ahi se usan los registros que estan ahi.

Los numeros en punto flotante se anotan

1,27321x10^exponente.

La mayor cantidad de bits son para la mantisa. Mientras mas numero tenga despues de la coma, mas precision tengo.

El procesador tiene 8 registros de punto flotante + algunos registros de estato..


Despues tiene los registros MMX y XMM que son vectores, donde puedo meter valores de muestras, y en otros registros meto ... Y tengo instrucciones que hacen convoluciones, etc..
Como todos estos son registros muy grandes el intel dice que no los guarda en la TSS ni loco. Lo que hace es. Da un bit para que cuando hay un cambio de tarea salta una interrupcion y prende un bit para avisarlo y que yo lo pueda guardar.

Antes era para simular por software las inastrucciones de procesamiento de señales.
Cada vez que el procesador conmuta de tarea, se setea el bit CR0.TS .

El procesador chequea TS.

Son las instrucciones que usamos nosotros cuanso salta la interrupcion 7.




