# Clase 10 10-10-2019

Sistema de archivos Ext2.
En ext2 se agrupan los bloques en bloques de 8192 bytes

Varios bloques en conjunto pueden configurar un _block group_

En el block group encontramos 1 bloque, el primero, que se llama super block y que por ser un block va a ocupar 8192 bytes.
Hasta ahora sabemos

El numero n no es coincidente. En rigor tendiran que lalmarse n, m y o por ejemplo

El disco se divide en grupos de bloques para reducir la fragmentacion.

El sistema filesystem se llama en rigor "virtual filesystem manager"

Pag 55 de 89, fs.pdf

Un inodo es una estructura
Que tiene tipo, atributos, nomnre del archivo o objeeto del filesystem, numero de inodo, fecha de ... etc..

Es un registro de la base de datos.

Un device file es un inodo. Un socket es un inodo, un pipe es un inodo, un archivo es un inodo.

Un device file ocupa un inodo pero no ocupa espacio de datos en el filesystem.

En un block group la tabla de inodos puede contener un millon de inodos.

Por ejemplo si tengo una pelicula en MP4 me ocupa un inodo y un monton de bloques de datos.

En un block group puede haber varios archivos.

8192 bytes = 8k

En el inodo hay un puntero al bloque de datos donde esta el archivo.

Cuando el inodo describe un dispositivo. se hace referencia no a nun bloque de datos sino al driver.

Los data blocks no son block groups.
Uno no usa cada inodo.

Cuando uno intenta buscar un archivo por el nombre.
Primero se va a fijar solamente en los inodos que en bitmap aparezcan como ocupados. Para no perder tiempo.

En el inodo hay una zona reservada para punteros a bloques.

El archivo cabe en 4 bloques.

El bitmap son mapa de bits. En cada block group entran 64k inodos.

En ext3 los tamaños de todo son mas grandes.

Un inodo esta vacio cuando no referencia ningun archivo.
El numero de inodo esta reservado.

El tamaño formateado es mucho menos que el tamaño de inodos porque tengo que descontar todo el espacio dedicado a organizacion. El espacio esta reservado.
Los inodos son consecutivos. Primero inodo1, despues inodo2, despues inodo3, etc.
El inodo es como si fuera el task struct de un archivo.
Si el archivo ocupa 16k, le ocupa 2 inodos, si ocupa 16k+1byte va a necesitar 3.

En principio los bloques por default de ext2

en windows los bloques de 8192 se llaman clusters.

si el disco es muy grande, por ejemplo 2TB, al formatearlo con ext2, el tamaño de bloque seuramente sea mas grande, de 256 talvez.

La peor granularidad es que cada archivo ocupe un inodo. El peor caso de granularidad seria 64k archivos de un bloque cada 1.
Otro caso es un archivo solo que ocupe todo el block group, que en este caso serian 512MB.

En ext2 el tamaño de archivo maximo es 512MB. Si quiero archivos mas grandes formateo con un tamaño mayor y entornces como cada bloque sera mas grande, el maximo archivo posible sera tambien mas grande.

```
65526 = 2^16
x 8192 = 2^13
_____________
2^29 = medio giga = 512MB.
```

Cada bloque mide 8192 bytes
El inode bitmao es la cantidad maxima de bloques que puede haber por grupo.

El inode bitmap es un mapa de bits que tiene 8192 bytes, donde cada bytes es 8  bits. Entonces hay 65536 bits en el bitmap de inodos y 65536 bits en el bloque de inodos.

El tamaño maximo que puede tener es (C)

El kernel utiliza solo el superblock y el group derscriptro del primer bloque.

Del encabezado del super bloque y del group descriptor se repite, es copia.
El siempre ituliza el primero, pero cada vez que lo utiliza mira que sean todas las copias iguales. Cuando hay una discrepancia se prenden todas las alarmas del kernel.

El super block y el group descriptor son del sistema y de la particion. Si se rompe eso se rompe la particion. Por eso esta repetido en cada block group. Entonces usa siempre la que esta en el block group 0. Pero cada vez que lo usa chequea que todas las copias sean iguales.

data block bitmap, inode bitmap y inode table se refieren a data blocks.



Que hay en el superblock?  
En el superblock se guarda: pag 57/89

Cantidad de montajes. Es la cantidad de veces que el kernel se monte. Cuando se hace eso aumenta el contador de montaje en 1.

Llega un momento de booteadas por ejemplo 100, cuando hara un chequeo de sanidad del filesystem. Ese checkeo se llama FSCK File System Check.

Cualquier cosa que monte va a pedir que haga el checkeo del file system.

Cada particion tiene su contador.

El checkeo del file system puede hacerse con el file system montado o desmontado, siendo el ultimo mas profundo.


Chown cambia el dueño del archivo.

chown pepe holatx, cuando uno cede un archivo a otro usuario.

el puntero 13 tiene un nivel de indireccion, osea apunta a un conjunto de 12 punteros que apuntan a bloques
el 14 tiene 2 niveles de indireccion, osea a un conjunto de 12 punteros donde cada puntero apunta a un conjunto de 12 punteros donde cada uno apunta a un bloque.
y el puntero 15 tiene 3 niveles de indireccion.

Despues del recreo empezamos a ver drivers.

___
___

En general todo se divide en overhead y payload

Sitara AM335x es un SoC

El kernel de linux que maneja device tree esta preparado para interpretar el device tree.  
Entonces el linux para sistemas SOC es especial.

Del device tree me da el fuente y el compilado.

Vamos a entrar mas en detalle, pero es complejo.  
La idea es que cuendo me meten un timer en el SOC, ese timer tiene que tener registros para configurarlo. El linux generico viene con un driver para manjear timers. No sabe en que posicion del mapa de memoria estan los registros del timer, hasta cuando puede contar, etc..

El driver lo tengo que escribir para que pueda buscar en el device tree la informacion que necesita para poder usar el timer.

El device tree asi como tiene un timer tiene una uart.

El buffer de recepcion y transmision en que posicion esta?  
Dependiendo de que version del dispositivo sea esta en distintas posiciones de memoria.

Mi diseño del driver esa preparado para poder abrir.

Los drivers son dependientes de la plataforma y del sistema operativo.

El acceso se hace a traves de los inodos del file system

No confundir los block device con los bloques del file system que vimos antes.

El read me devuelve de a 512 bytes que es el tamaño de datos con el que el kernel accede a los datos.

``` c
buffer = malloc(512);
fd = open("/dev/sda", r);
read(fd, buffer, 513);
```

LKM es Linkable Kernel Modules, se vincula al Kernel cuando esta funcionando, no se introduce en el kernel cuando se compila. Tengo el driver precompilado y se adjunta a la memoria sobre la marcha. A los drivers que se agregan de esta forma los llamamos LKM.

Registratse con el driver es informarle al kernel las capacidades del driver e informarle sobre el hardware. Por que? Para que ni bien se registra, me va a decir que sabe hacer y que necesita.

Hay 2 formas  
Una es decirle al kernel: a partir de ahora dispones de estas capacidades pero el driver duerme hasta que se le solicita que use las capacidades.

Cuando se necesita usar el dispositivo hardware se instala en el kernel.  
Pero eso no significa que se vaya a empezar a usar inmediatamente.

Son 2 niveles:  
Uno es manual  
El driver de SPI no viene. Lo hago, lo compilo, lo tengo como driver LKM.  
Va a tener el nombre midriver.ko  
Como hago para que el kernel sepa que lo puede empezar a usar?  
Con insmod midriver.ko  
Cuando uno hace eso, el kernel lo agarra, lo levanta del disco rigido. Lo abre y dentro tiene que encontrar determinada estructura.  
Tiene el dispositivo listo para usar.

Cuando el kernel lo abre, lo levanta y lo mete en memoria, ejecuta una parte de inicializacion de ese driver.

Los kernel se compilan con esa opcion config_modules por defecto.

Es como una biblioteca de funciones que implementa puntos de entrada para diferentes funciones. Esto es como si estuviera sobrecargando la funcion.

``` bash
insmod midreiver.ko
rmmod midriver
```

si uno se olvida de escribirle al rmmod la parte que elimina los registers, queda en memoria.

Como somos el kernel, no hay ningun free automatico.

Vamos a encontrar como que el dirver va a tener una parte que tiene funciones que se van a ejecutar con muchos punteros, para open, para read, para write.  
Otra parte que se va a ejecutar con insmod y otra parte con rmmod.

un bloque con todas las funciones de uso del driver  
un bloque con todas las llamadas a registros que voy a utilizar  
y otro bloque para desregistrar todo eso cuando lo desinstale.

el objeto ejecutable no tiene solo un punto de entrada con llave que abre y llave que cierra. El driver no, depende de que estoy haciendo tiene varios puntos de entrada.

La placa de red wifi depende del driver usb.  
Los drivers tienen que venir con una firma digital de autenticidad.

Que necesitamos para compilar y correr un modulo?  
Necesitamos los headers (los .h) del sistema  
eso lo hacemos con uname -r que me dice la version del kernel y en la ppt esta el comando para isntalar los headers del kernel, que los necesito para poder compilar drivers.

en los headers del kernel tenemos:  
sked.h  
modules.h  

insmod y rmmod los necesito para insertar y eliminar modulos del kernel.

El modulo debe ser recompilado para cada version del kernel.  
Si actualizo el kernel, tengo que recompilar para poder ejecutar.  
Si cambio el kernel me tengo que bajar los drivers para el kernel nuevo.  
Mi driver.ko tiene grabado dentro la version del kernel para el que se compilo.

Nosotros podemos poner insmod -f midriver.ko para forzarlo.

En el ejemplo de la pag 29/159 solo se us la parte de insmod y rmmod, no el otro bloque que mencionamos.

init -> insmod  
exit -> rmmod

los alert funcionan llamando a la funcion de shell dmesg, que es un buffer de memoria. Ahi van a parar todos los printcat de memoria.  
Luego, todo lo demas, osea, de error para arriba ... Estan en orden de criticidad.

/var/log/syslog  
ahi dice con que comando habilitar el nivel de logeo por default.  
Por defecto es de kernel info para arriba. Podemos decirle, me interesa solo desde critico para arriba o algo asi.

Por lo que van a aparecer los printk en 2 lugares, en el dsm y en el syslog.

hay un par de aliases que tienen embebidos los niveles de criticidad.

Compilacion: Se utiliza un makefile que debe tener esto adentro.  
Mas adelante esta explicada la sintaxis.

El que quiera puede empezar a probar el hola mundo desde driver.  
Cuando empezamos a jugar con drivers, hacer backup de la flash. Tener cuidado.
