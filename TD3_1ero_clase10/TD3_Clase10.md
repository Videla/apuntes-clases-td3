Clase de hoy.

- Modos de paginacoin
- TLB


La unidad de paginacion tene un area de memoria interna llamado CACHE de traduccion, donde se guardaran las entradas de la tabla que va usando.

Cuando fuimos a buscar en el directorio de tablas de pagina una entrada para realizar la traduccion , la guardamos en esa zona de memoria de la unidad de paginacion.
De esa entrada resolvio la direccion de la tabla de pagina, entonces en otra entrada guardo y ya con esas 2 entradas resolvi la direccion de la tabla de pagina.

De esa forma no tengo que llamar al procesador dos veces, sabe que tiene que cambiar cuando no tiene la direccion dentro de la unidad de paginacion.


Luego veremos la implementacion de esto.

Cuando se llena el cache de paginacion, internamente tiene una columna de bits o bytes adicionales donde va guardando la cantidad de veces que uso cada entrada. Cuando se llena, hay 2 criterios para liberar memoria. 2 algoritmos que va conmutando. Los candidatos a desalojarse son o los menos accedidos(LFU(frequent) o los que hace mucho no se usan(LRU(recent)).

Decide aleatoriamente que metodo usar para decidir cualdesalojar.
Todo esto se ve en pag 101 de 175.

Si el programa salta constantemente de una pagina a la otra, puede quedarse mucho tiempo en la cache.

Los 12 bits del offset pasan de largo en la traduccion realizada por la Unidad de paginacion, porque no son necesarios para la traduccion.

Por eso cuando guarda una entrada, solo esta trabajando con 20 bits. hay 12 bits que le sobrarian y los usa para guardar los privilegios de la pagina, solo lectura, privilegios.

La tabla de CACHE de traduccion se llama TLB, Translation Lookaside Buffer.
Look aside es mirar a un costado, ir mirando un lado y el otro dos paginas que estan una al lado de la otra.


El tamaño del buffer es mas grande cuando mas grandes es el procesador.

Antes se mezclaba todo en el cachde de traduccion, ahora hay 2, uno para paginas de codigo y otro para paginas de datos y eso en relacion a la arquitectura del procesador tiene beneficios.

Cuando tira la cadena(flsuh) del buffer se vacia el TLB. Cada vez que cambio de tarea traigo un mapa nuevo de tabla de paginas.
El TLB es del procesador, en el cual puede correr muchas tareas. Tiene que vaciarlo cada vez que cambia de tarea.
Nada impide que si uno diseña un sistema chico, se pueda organizar de forma tal que diga, todas estas paginas son de un proceso, todas estas son de otro y estas son de otro y en ese caso tener 



(C)

Como tengo granuralida a nivel de pagina, dos programas necesitan 2kB cada uno, puedo compartir una pagina? y que un programa use los primeros 2k y el segundo los siguientes 2? No, esto no se puede por la granuralidad.



Cuando tengo un procesador de varios nucleos, todo esto se multiplica para cada uno de los nucleos. Pero la memoria es una. Cuando veamos microarquitectura interna de los procesadores.
Hay un cache mucho mas grade.

Hay 2 niveles de cache internos del procesador. (C)

Las memorias hay estaticas y dinamicas.

Las estaticas son mas rapidas pero mas caras. Ocupa mas superficie de silicio.
Las dinamicas hay que referescarlas cada tanto y mientras se refresca no se puede acceder. Esa espera representa muchos ciclos de maquina para el procesador.



Ahora vienen los modos extendidos, el PAE y el PSE-36. Estos modos pretenden generar direcciones fisicas mas alla de los 4GB, por ejemplo la unidad de paginacion puede resolver mapeos sobre 8GB, en 32 bits no podre mapear todas las direcciones. Podre mapear 4GB sobre cada uno. Pero esto me servia para tener 4GB para cada programa. Esto me resuelve 
Si necesito mas de 4GB, uso alguna de estas 2 especificaciones y puedo acceder a mas memoria.

Cuando las paginas son de 4MB en el metodo de paginacion standard es mas sencillo todo. El offset es mas grande, uso 10 bit para acceder al directorio de tablas de pagina y despues con 22 bits de offset recorro los siguientes 4 MB. No tiene tablas de paginas. Es un solo nivel para encontrar la pagina

Si tengo que ir intercambiando paginas dentro y fuera del procesador son de 4MB.



El kung fu esta en como hablar las entradas y los bits de las tablas de paginas.
Tengo CR3, los 20 MSB(bits mas significativos) son un puntero a memoria a un SLOT que va a ser la direccion fisica del DTP(directorio de tablas de páginas)
los 12 bit LSB son 0, voy a aprovechar para guardar informacion en esos 12 bits.

Los ultimos 3 son ignorados

Los PWT y PCD
Los modos de escritura estan relacionados a la estructura de escritura en memoria cache.
Cuando vimos la traduccion con el TLB en la cache de la unidad de paginacion.
PWT sirve para elegir el modo de escritura. Nos van a decir que modo elegir, no hay que pensarlo.

Despues PCD. Se relaciona a la memoria no cacheable(no se va a guardar esa entrada en el TLB) eso lo vamos a dejar activado. Eso es para todo el directorio.

Como voy a acceder una vez cada mucho tiempo no tiene sentido que lo use para eso.
Como nosotros estaremos conmutando tareas lo dejaremos activado. De forma tal que cada vez que hacemos un cambio en el CR3 se flushee todo el TLB.
Tenemos que dejar en 0 a PCD.


Ahora veremos la estructura de una entrada de DTP, estos se llaman descriptores de tabla de pagias, un descriptro de tabla d epaginas esta en un...

Porque describe una tabla de paginas.
Los primeros 20 bits me dicen en que parte fisica de la memoria esta la tabla de paginas N. De la misma forma que la tabla donde esta el DTP esta en tal lugar.

Page fault seria cuando encuentra una tabla de paginas vacia.

Tenemos 117 CR3
118 descriptor de talba de paginas
119 decriptor de pagina

Los atributos son casi coincidentes entre el descriptor de tabla de paginas y el descriptor de pagina.

De la 120 en adelante estan explicados los bits.

Hoy vamos a ver hasta aca


Nos va a adelantar que van a describir los bits. ahi va a estar si la pagina es con nivel de privilegio de usuario o de superisor, puede ser privilegio alto o bajo, solo 2 no 4 ocmo ula unidad de segmentacion. Cuando marco privilegio alto puedo poner codigo que se ejecute con nivel de privilegio alto, si margo un apagina con menos privilegios y si encuentra una insruccion privilegiada, va a saltar error de protecicon GP (General Protection). Da Page Fault(PF) cuando no puede acceder a la pagina. Cuando el error se produce encontrando la direccion, es page fault. 
Se puede marcar si la pagina es de solo lectura o lectura escritura(igual que los segmentos). Hay un bit G que no hay que confundir con Granuralidad, que era un atributo de los segmentos. Este bit G(global) que dice si este esta en 1, tiene efecto la activacion de la funcionalidad global. Si meto en uno de los atributos de la entrada del TLB, si esta prendido el bit G de esa entrada, cuando cambie de CR3, esa queda en la cache de la unidad de paginacion, para que si quiero conservar memoria para el


Cuando activamos PSE o PAE tenemos una direccion fisica de mas bits.

La direccion lineal en lugar de partirla en 3 la parte en 4.

