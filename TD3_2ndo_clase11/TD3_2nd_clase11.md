## Clase 11 17-10-19

Sabado 19 tarde: 14:00

BMP180 __NO__ (NO es SPI)

Backlog dinamico __NO__

LKM's

man syslog

syslogd -> /var/log/syslog.log
            /etc/syslog.conf

Se puede instalar un man para la vesrion de desarrollador. Algunas distros tienen version de desarrollador de kernel que tienen incluido el manual. Sino se googlea y listo.

Version del man de linux para desarrollo del kernel.


syslog.log.z

Esto de recien era para aclarar a donde van los printk


.ko = kernel object.


se puede hacer un objectdump sobre un kernel object

``` bash
objdump -h hello.ko
```


Cuando se compilan diferentes partes del mapeo del espacio de memoria tienen que corresponderte con esas secciones, poerque el kernel dentro tiene esas secciones.


Este libro que nos pasan en las diapositivas es sobre una vesrion del kernel muy antigua, pero muchos de los conceptos estan muy bien explicados, con mucha profunidad. Pero hay que tener la precaucion de chequear si lo que esta en el libro esta aun activo en un kernel nuevo.

En el kernel no hay compatibilidad hacia atras. Se le garantiza la compatibilidad hacia atras a un selecto grupo de gente.

Para evitar quilombos se recomienda el otro libro, que es el de abajo de todo.

Hay uno que no esta en la presentacion llamado Derek Molloy.
Tiene mucho sobre beagleboard.

Para compilar Es mandatorio poner como se va a llamar el objeto.
Hay que poner para hacer modulos el obj-m += helloWorldModule.o

Puedo compilar un driver para una version distinta al kernel que estoy ejecutando.

Despues debajo tenemos un comando que haciendo ```shell uname -r``` compila para la version del kernel correspondiente.

Si quiero compilar para la version que estoy corriendo lo dejo asi. Si quiero compilar para otro kernel me tengo que bajar los headers para esa version en particular.

si voy a:

``` bash
cd /lib/modules
```

hay una carpeta que tiene el nombre de la version de kernel que estoy corriendo.  adnetro tiene por carpetitas los .h de todo. El build tiene los headers. Por versiones. Ahi podemos ver: Samples, securrity sound tools ubuntu, etc.. Al compilar va viendo en ese arbol todo lo quue necesita.

Para instalar headers

``` bash
apt-get instal kernel-modules
```

es algo asi, verlo en google.

Se compila con make

El directorio build

Vamos a tocar el device tree del linux para deshabilitar constrolador por defecto de SPI.


Podemos usar el driver por defecto para probar el sensor. El fabricante por lo general nos da una rutinita de prueba para la beagle. Buscar por internet.


### hola mundo

el hola mundo es muy sencillo, al compilar genera el .ko.

Para instalar y remover se utilizar

insmod(ejecuta la parte de init), rmmod(ejecuta la parte de exit) y modprobe.

Es como un programa con 2 main que tiene una forma para llamar a cada uno de ellos, uno que inicializa y uno que remueve.

El modprobe se fija si el driver nuestro depende de otro. A veces pasa que un driver depende que previamente este instalado otro driver.

Seria para el platform device.

Puede haber varios niveles de dependencia entre kernel objects. El modprobe prueba todos. Si uno pretende instalar su driver antes de que aquellos de los que depende el modprobe va a saltar.

Hay que hacer 2 drivers, uno que maneja el spi y otro que levanta los datos.

Para el caso de nuestro driver no es mandatorio. En realidad cualquier driver puede ser intalado con insmod.

```insmod``` hace que se ejecuta la funcion ```module_init()```
```rmmod``` o ```modprobe -r``` hace que se ejecuta la funcion module_exit()

module_init y module_exit se comporatn como funcion contructora o destructora, pero el conceptro de objeto de kernel no es igual al de objeto que se ve en sistemas.


Dentro del proc/ teniamos un monton de carpetitas que representaba el pid de cada proceso. Ademas de las carpetitas que tienen los pid de todos los procesos que estan funcionadno tenemos un archivo modules que esta en ram, y dentro tiene una tabla con todos los drivers que estan instalados.

si hacemos ```cat modules``` nos muestra todos los drives que estan cargados.
Si nos fijamos vamos a ver cosas que en el nombre tienen pcm, que son de la placa de audio.
Lo que dice midi tambien es de audio.
tambien podemos ver las dependencias.

Por ejemplo smd_rawmidi depende de snd_seq_midi. Esto se ve a la derecha de el modulo.

Esto se ve por los simbolos, por los nombres de las funciones que llama el modulo. Por eso puedo llamar desde el modulo que hago al scheduler del kernel, porque esta definido con un nombre global.

El modprobe antes de ponerlo en memoria se fija los noombres de las funciones que va a llamar y se fija si estan corriendo todas las funciones que llamaremos.

El numero que esta a continuacion pareciera ser la cantidad de memoria que ocupa. Y el numero siguiente puede ser la cantidad que estan colgados de ese.

Este es el archivo que se muestra al hacer __psmod__? El profesor no esta seguro


si hacemos cat interrupts nos muestra las interrupciones, primero las de hardwares. Nos lo muestra por cpu.
En IRQ0 esta el timer y en irq1 esta el i8042(controlador de teclado). Y lo que muestra debajo de cpu0 es la cantidad de vee s que se llamo a la interrupcion.

Para observar los resultados, es decir ver lo que imprimio, tenemos 2 formas, una es el dmesg que muestra los mensajes que fuerino entrando al syslog. con el tiempo se refresca.

Para ver toda la historia vamos al syslog.log

Es preferible usar modprobe para que muestre las dependencias de todos los drivers.

Despues del recreo veremos la demo de compilar el hola mundo.

___
RECREO
___


Vemos un ejemplo nos lo pueden pasar.

tail y head hacen lo mismo que cat pero head muestra las primeras 10 lineas y tail las ultimas 10

``` bash
tail -f
```

en particular muestra las ulitmas 10 lineas y no lo cierra, lo deja abierto leyendolo constantemente y si alguien escribe algo lo muestra inmediatamente.

tail -f /var/log/syslog

en esta version se llama syslog en lugar de syslog.log

El make va a dar warnings pero el driver funciona.

Despues nos mostrara el codigo fuente y veremos que hay denuevo con respecto al helloworld basico que habiamos visto antes.

Nos va a pasar el codigo de un driver para una competencia de botones que muestra.

Aca no se exige doxygen.
Funciona con cualquier lenguaje.
Tiene una minima configuracion, donde le decimos para que lenguaje hay que usarlo.

El make va a generar varios archivos, solo le vamos a pasar a insmod el .ko.

``` bash
insmod trivialkm.ko
```

Y se agrego a el syslog los mensajes que tiro el driver.

Nos pone entre [] el contador de tiempo del kernel, es el timestamp del procesador y nos pone tambien el nombre con el que se registro el disposivo.

TriviaLKM

En el fuente veremos como hacer para que se registre con un nombre predeterminado.

Ahora hacemos 

``` bash
rmmod tivialkm
```
va sin el .ko

Imprime el numero de veces que se llamo a read, nos imprimio cero porque no ejecutamos ningun programa de usuario que llame al driver entonces no llamo a read en ningun momento.


Ahora veremos el fuente y veremos que estan implementadas las funciones: open, read, write y close


El hola mundo solo pone printk en insmod y rmmod

este driver tiene otras cosas mas, pero no las explicara hasta que explica otras cosas.


las funciones read y write requieren asm/uaccess.h


Despues de las librerias vienen unos define con un nombre particular que tienen que ser asi:
DEVICE_NAME
CLASS_NAME

y que tienen el nombre que va a tener como dispositivo y el nombre que va a tener como clase de dispositivo


en cd /sys/

podemos ver una descripcion del hardware del sistema. Ademas puede ser que un mismo dispositivo este descripto en mas de una categoria.
Por ejemplo lo que esta en block tambien esta dentro de clase clasificado como disco rigido, usb, dentro de usb estara el flash del pendrive. Todo eso que esta ahi son inodos, no son archivos y carpetas fisicos como almacenamiento. en /dev tenia el nombre del dispositivo, pero tambine puedo verlo desde el /sys.

Adentro de class vamos a encontrar


A mi no me llama directamente el usuario

La funcion open del kernel busac la asociacion de que cuando te pidan este nombre buscas este objeto. Eso se hace en el init modules que hace la registracion y asociacion de eso.

Y el otro parametro lo que indica es en que modo o que esta permiitido hacerse con las funciones de ese driver. Si lo uso solo para lectura no voy a poder hacer write, si intento hacelro el kernel me va a devolver a error. El kernel lo va a parar antes que mi driver.

No tiene que ver con como esta guardado. Una cosa son los permisos del inodo y otra son los modos sobre los que quiero operar sobre el archivo.

Una vez se garantiza la apertura no puede hacer read.

No puede hacer write.
Si lo abre solo para escritura no lo puede leer de la misma forma.

Con que me tengo que quedar hasta aca? Mi dispositivo se puede abrir? Si, si spuede abrir se tiene que poder cerrar. Se puede leer y escribir? Lo unico que puedo cambiar es el nombre que le doy a cada funcion. Lo que hay que respetar es los prototipos de las funciones, osea ,los parametros que recibe.

En los fuentes del kernel las referencias a la funcion close se llaman release. Podria ponerle close, pero por una cuestion de costumbre el usa release que es el nombre con el que se conoce a la funcion dentro del kernel.

Lo que retorna es ```static``` para que sea accesible desde cualquier lado del kernel.

Cuando hago un programa en C y defino una variable static, lo que hago es que sea indeleble el valor.

Read y write devuelven otra cosas que tiene un tipo nuevo que en realidad es un entero. Se llama ssize_t. En general read y write devuelven el tamaño que pudo operar, la cantidad que pudo leer o escribir. Lo hace con ese tipo por una cuestion de portabilidad a otro modelo de procesador. En definitiva en todos los procesadores modernos coincide con un int en PC y ARM.

Vamos a ir viendo lo que vamos a ir usando.

Las funciones que voy a usar las voy prototipando de esta forma.

Cuando llamoa mi funcion open me pasa 2 punteros a estructura. En el caso de este driver en particular esos punteros no los necesita, porque lo que hace el driver no los necesita. Puede haber otro tipo de dispositivo que si necesita esos punteros.

Que tiene este inodo en el file system?
"dev/trivialkm"

En la registracion del dispositivo tengo que crear la clase del dispositivo o conectarme a una clase existente.

Cuando se hizo el kernel lo bautiza como ...

Voy a poder necesitar informacion que esta en el inodo que describe al dispositivo. De la misma forma me lasa un puntero a una estructura de tipo archivo que .... al indodo.

Lo mismo pasa con lo que seria el close, le pasa los mimsos parametros.

Al read y write les vuelve a pasar la misma estructura file.

Para operar el dispositivo el kernel utiliza un buffer intermedio.

Cuando quiero escribir un dispositivo el usuario le pasa un buffer.

Va a esperar que el usuario haga algo que le obligue a escribir.

si hacemos un printf("hola") le pasa a la estructura (B)

salvo que le ponga un /n que es el caracter que indica que hay que flusheralo.

Para el caso del read y write lo vamos a usar del buffer del usaurio 


Uno le pasa a la funcion copiar esa estructura.

Esto es todo un pasaje de punteros a estructura. Despues el que lo recibe ya sabe como desenroscar la estructura.

Despues por supuesto tenemos los tamaños que indican el tamaño que estamos operando.


Lo que sigue es muy importante.
Mas adelante viene una estructura definida en la fuente del kernel. Y yo defino para el uso de mi driver una variable de ese tipo, creo una variable de tipo file operations. 
Esa variable tiene dentro los puntero s a las funciones que implementa el driver.

El kernel del ddriver de linux me requiere que prototipe mis funciones. Y que rellene los campos.

Estos no los puedo cambiar porque estan fijos en el kernel. el nombre que le pongo es el que le puse a las funcoines anteriormente.

``` c
{
    .open = dev_open,
    .read = dev_read,
    ...
}

```

Todas las operaciones que uno puede hacer un con archivo son funciones que se pueden implementar.


En la parte de estructura file opeartions esta *read, *write, *open y *release. Tambien esta *lock(para bloquear el arvhivo), *flush(para limpiar el buffer)
Para el driver necesitamos las 4 que vimos. A lo sumo la ioctl que si se llama desde el programa de usuario se llamara a compat_ioctl.

Uno puede abrir la placa de sonido haciendo open/dev/audio.

Si lo leemos, que pasa? Toma muestra del microfono. Si hacemos write convierte los bytes en PCM y los saca por el parlante. Y la pregunta seria a que bitrate? Abra algun default? Si y se configura con ioctl y esta configurado en el driver de linux.

Imaginemos que ahora tenemos el dispositivo que vamos a conectar que mide temperartura o presion
Que le podemos configurar? Tenemos que escribirle al chip algunos valores para calibrarlo.

Como hacemos para desde el programa de usuario pasarle la calibracion del dispositivo? Si queremos escribirlo, no seria correcto. Si lo leo leo datos de temperartura, si lo escribo, ... que pasa?

En un purto serie comom le escribo el baudrate? con ioctl.

La calibracion y configuracion no va con write, va con ioctl. Se puede hacer, pero no es como se hace.

Un sensor de temperartura es solo lectura, no deberia implementar la funcion write.
Y se calibra con ioctl.

Esto es por buenas costumbres.

Pero ambas funciones podrian hacer lo mismo.



Con la funcion register_chrdev lo que hacemos es decirle al kernle que este driver a va a manejar un dispositivo tipo char

Estan los tipo char y tipo block, lo va a pilotear como si fuera tipo char.

Le pasamos el nombre del dispositivo con &fops

Que nos devuelve la funcoin register_chrdev?

Ahora vamos a volver a dev

cd /dev

lo instalamos denuevo.

ll ttyS0

hay un inodo creado en /dev para el puerto serie fisico.

ttyS0 es equivalente al com1 de windows.

Por supuesto es tipo char

esos 2 tamaños estan en todos los inodos que describen un dispositivo.

Cuando se instala un dispositivo va a aparecer ahi como:

/dev/trivialkm.

veremos que hay 2 numeritos ahi asignados.


una vez instalado al hacer:

ll trivialkm

crw --------- 1 root root 245, 0 oct 17 22:11 trivialkm
Tenemos 2 numeros llamdos:
                        NMayor Nmenor

Nmayor = 245
Nmenos = 0

El numero 0 es el numero instancia del driver.

En este caso seria 0.

Existe puerto serie? Si, ttyS0
Existe tambien un segundo puerto serie.

Si hacemos ll ttyS* me los muestra todos

todos tienen el mismo numero mayor porque el que maneja al driver es el mismo, el numero menor es el que maneja al driver.

Con un mimsmo numero mayor registro un mismo kernel object para contorlar todo slos dispositivos.
Cuando el driver recibe el nombre saca del inodo el numero mayor y el numero menor.

El usuario hace open ttyS0. Y le pasa al driver que abrieron el 64. El driver entonces usa el hardware asociado con ese numero.

El puerto serie usa el registro de lectura y escritura donde van los bits.

Dependiendo del numero menor que me paso se que registro tengo que usar si el de recepcion transmision del puerto 1 o del 2. 


El kernel cumple en decirme cual es el dispositivo que estan abriendo, yo driver tengo que ver que hago.

quien da el mayor y el menor? El kernel? Ahi estea el punto. La funcion register_chrdev me deuvleve el numero mayor, porque es del kernel object. Como es con el numero menor? El numero menor hay 2 formas de obtenerlo. Yo puedo decidir que numero menor ponerle. Puedo crear yo a mano el inodo.

Le pongo numero mayor tanto y numero menor tanto.

El numero mayor depende de que dispositivos estan instalados



Para los numeros mayores de los drivers estan fijos los numeros mayores. De esa forma se comieron todos los numeros mayores mas chicos. Del 200 en adelante son para dispositivos custom definidos por le usaurio.
Hasta cuanto? Son 2 bytes. Un byte para el numero mayor y un byte para el numero menor.

El unix hp tiene el numero mayor y el numero menor de 2 bytes cada uno.

Lo conveniente es que el numero mayor lo 

Cuando a insmod le devuelven un numero negativo ,se que es un codigo de error. A insmod le devuelve un numero negativo va a decir que no se pudo registrar porque estan todos ocupados o lo que sea.

Ahora hay que registrar la clase del dispositivo.

si majorNumber es < 0 

Si quisiera hacer este dispositivo de una clase que ya existe tnego que usar class_atach o algo asi.


entonces tengo que crear la clase y le pongo el nombre que quiero que tenga con CLASS_NAME

Si quiero hacer un class_create con un nombre de clas que ya existe me va a devolver error. Por eso voy a hacer chequeo de error.



Si falla tengo que hacer un cleanup, es decir, desregistrra el chardev.
Es recomendable hacerlo asi.

Para que funciones bien el codigo de error del insmod hay que devolverlo de esta forma que esta especificado ahi. (return PTR_ERR(triviaClass))

(C)

PTR_ERR es una macro para que el insmod explique correctamente el eerror.

IS_ERR devuelve un valor de verdad para que funciones si esta bien o no.

Si no recuerdo mal creo una estructura donde le paso los 2 valores.

triviaClass y trivaidevice son punteros a estructura.

A medida que va fallando mas adentor tengo que ir deshaciendo los pasos que fui haciendo al revez.

Una vez logre hacer todo pongo dispositivo creado correctamente.

Puede haber errores al hacer exit, si de mi driver dependen otros modulos tengo que hacer los chequeos de cada parte del proceso de destruir lo que fui creando.


Podemos poner en el comentario no hago chequeo de erores porque nadie depende de mi driver.


El open, el read y el write  son mas breves. En este dispositivo el open no hace nada.

En el del jueguito del trivia si hace algo.


