Quedan 2 temas grandes, esto (paginacion) y tareas.



Reemplazo una presentacion por otra.

Partiendo de la pagina 75 de la presentacion que es la version 2019.
Hasta la pagina 75 es igual, despues de la 75 en adelante, que explica paginacion y agrega paginacion de procesadores ARM.



El procesador lo usamos en el modo de segmentacoin unicamente.
Luego de definido el segmento, un offset o desplazamiento dentro de ese segmento.

En el camino entre la salida de la unidad de segmentacion y ..... se puede activar la unidad de paginacion.

Una vez que se activa la unidad de paginacion, la conversion de direcciones es transparente.


Mi programa genera direcciones. Requiere que el procesador ponga una direccion al bus de datos. Tiene que sacar la instruccion de la memoria. Cada vez que el IP(Instruction Pointer) avanza, tiene que ir a buscar, siempre esta traduciendo direcciones en el bus de direcciones


El procesador tiene que convertir la direccion en direccion lineal.

Se llama lineal porque linealmente puedo apuntar a cualquier direccion de 4GB.

Luego ,la direccion lineal se transformaba en una direccion fisica.
Ahora vamos a encender la unidad de paginacion, que esta en el medio.
Sin la unidad de paginacion el numero N es el mismo que la direccion fisica, esto se llama identity mapping.

La gracia es poder hacer una traduccion que me de lo que necesito, ahora iremos a ver que necesito y para que.


Ya sea con identity mapping o no, la unidad de paginacion trabaja con tablas de descriptores de pagina.

Es como una mini GDT para un conjunto de paginitas.

Se divide toda la memoria en paginas del mismo tamaño y ahora la tenemos que agrupar en grupitos. Para cada grupito tendremos una mini GDT.

Se setea mediante los valores que coloque en las tablas de páginas.

Podemos hacer indentity mapping por regiones.



Cuando la unidad de paginacion esta activada y le tiramos para que traduzca alguna direccion y no le tiramos.

Para la GDT tenemos GDTR
Para las tablas de paginacion tenemos el CR3.

No pueden tener cualquier valor, hay unos bits de control que tenemos que activar. Lo primero que hara es ver que tenga un valor consistente.

Hay una excepcion para paginacion, llamada #PF (Page Fault)

CR3 es el puntero a donde esta el comienzo de esas tablas.
No es como la GDT que tenemos una sola y con esa nos arreglamos.
GDT tiene varios niveles, es como un arbol. CR3 apunta a una tabla que apunta a tablas que apunta a paginas.


Primer nivel = equivale a GDT
Segundo nivel = tablas de páginas

Ni bien activo el bit de la unidad de paginacion, a la siguiente instruccion, el procesador ya fue a traducir la direccion.
Si queremos tener todo preconfigurado antes como en la GDT podemos hacerlo.

Una vez que tenemos todo listo y perfecto habilitamos la paginacion, y una vez que esta habilitada la podemos utilizar.


La paginacion no se puede activar si no estamos en modo protegido.

Page fault salta siempre que se produzca algun error en la traduccion de páginas. Y tambien hay niveles de privilegio para las páginas. Si se viola la proteccion de esos privilegios, va a saltar el page fault. Es igual que en segmento, salta primero el Page fault pero despues nos trae mas informacion.

Si el bit de inicializacion esta en 1, significa que CR3 no esta configurada en un lugar válido.

El codigo de error nos trae much ainformacion para poder determinar que fallo en la unidad de paginacion para poder resolverlo rapidamente.

Cuando la direccion se traduce y no hay memoria física instalada.
Ni hay traduccion para la direccion lineal que entró.

La tabla de páginas tiene que estar configurada. Eso dara una excepcion. no tener cargada una entrada en la tabla de páginas, da una excepcion.

Debe encontrar y configurar para poder hacer la traduccion correctamente.


La unidad de paginacion puede funcionar en varios modos, en la teoria hay 4 pero usaremos 2.
En principio tendriamos 2 niveles de indireccion.

Usamos el procesador en 32 bits. Las direcciones que entran en la unidad de paginacion son de 32 bits. Las direcciones de 32 bits. Pueden acceder a 8Gb de ram. Necesitaría 33 bits. No me sirve la unidad de paginacion en ese modo nativo de 32 bits. En el procesador 80386 la unidad de paginacion no era necesaria la unidad de paginacion. Le agregaron modos que permitian acceder a mas memoria.

Hay un detalle para poder acceder a mas memoria.

PAE: Phisical Adress Extension.

Esto me dice que si tengo de - a 8Gb, las direcciones lineales se mapean y pueden caer en cualquier lado.

Cualquier entrada me puede dar cualquier direccion en el mapa, puede hacerse como quiera.

Llega un punto en que voy cubriendo y todo lo que cubro suma 8Gb. PAra que sirve entonces? PAra que cuando un programa esta metiendo direcciones aca, el programa ve una direccion lineal continua de 0 a 4gb. Y puedo tener otro programa de usuario 2 que tambien ve un mapa lineal de 0 a 4gb.
La idea es que esten protegidos los programas y uno no pueda meterse en el otro.


El contexto de ejecucion de cada programa es distinto al del otro.

Dentro de los registros del procesador que estaba usando esta CR3, que lo puedo cambiar como contexto.

El CR3 cambia la estructura de paginacion de las páginas. Uno usa la direccion de memoria y el otro usa el resto.

Esto me dice que yo no podria hacer que un programa usara simultaneamente los 8GB.



Dentro de CR4. Si en la bios se habilita PAE se puede usar mas de 4 gb de ram.



Cuales son las ventajas y desventajas de usar paginacion y segmentacion.

Segmentacion: Meto cada programa en un segmento y cada  (C)

Tengo que tener 4 programas que corren simultaneamente en la memoria fisica.

Cuando quiero correr el tercero, descargo de memoria uno de los 2.

En cambio las páginas son mucho mas chicas, son de 4kB, son mas rigias para la programacion de aplicaciones, y eso no es tan asi, porque puedo cambiar mis tablas de paginas.

Que los bloques sean del mismo tamaño y

Cuando se acaben los 4k salta un page fault, si aun sigue en su tiempo, le pongo otra pagina de 4k. Rara vez hay que buscar un dato lejos, a otro lado. Le voy cubriendo los huecos que va necesitando.(C)

El procesador le va poniendo páginas a medida que se va quedando sin ellas, a medida que va pidiendo memoria para funcionar.
Podemos observar que cuando corremos el office empieza a haber muchos fallos de página. Y una vez que se estabiliza, se queda estable y no pide mas memoria.
Si pegamos una imagen en el word veremos que hay movimiento y pide memoria para el word.


Porque no usar segmentos de 4k?
La GDT tiene 8k. Las paginitas necesitamos que sean chicas para que las transacciones sean mas rápidas.

Si el acceso fuese instantaneo no habria problema, el problema es el tiempo de acceso a la memoria virtual.
Otro detalle que ahora no estamos teniendo en cuenta son los niveles de cache del procesador.
Como lo vemos ahora, el procesador tiene que ir afuera a buscar..

Tenemos 1024 tablas de páginas y cada una tiene 4096 entradas.


Para poner la Unidad de paginacion hay que (C)


Como le poniamos el bit en 1 al CRC? con que instruccion ? enmascarabamos el bit.. 

Hay que tener la GDT y la rutina de atencion de interrupciones pagefault lista. Para hacerlo al mismo tiempo hay que saber mucho, nosotros no haremos eso.


A la Unidad de Paginacion le entra una direccion de 32 bits en nuestro caso.



Modo de 32 bits. Con la salida del pentium PRO, aparecio el PSE PAge Size Extension. Nosotros no usaremos este mdod
Modo nativo de 32 bits
Modo PAE crea direccion fisica de 2^56 bits.

Modo PAE, aparece junto con el PSE de diversas formas accede a mas de 4Gb.

Los diseñadores de sistemas opeartivos optaron por usar PAE, nosotros usaremos este.

IA-32e: En los manuales de intel nunca dice IA64, veremos IA32e, porque IA64 fue registrado por AMD.
el modo de paginacion de 64 bits de intel esta basado en el PAE. Es como el PAE pero llega a 64 en lugar de 56.

Todavia no existe un procesador que tenga 64 patas de procesador de direccion.


Podemos ver cuantas direcciones.


CPUID nos dice que modelo de procesador es.

Es como una forma indirecta de ajustar el procesador. Nos devuelve el AX del EAX(C).


Hacen falta algunas estructuras, hay varios niveles. Las tablas de páginas apuntan a las paginas en memoria. Antes de las tablas de páginas tenemos el directorio de tablas de páginas.

Las tablas de páginas tienen atributos.

La segmentacion se conjuga con la paginacion.
La unindad de segmentacion no se puede apagar.
Tengo que usar segmentos y paginacion.
El problema se resuelve muy facil.
No protegemos a nivel de segmentos, protegemos a nivel de páginas.

La GDT tiene que tener 3 semgnetos, uno para el codigo, uno para la pila y uno para. ... el resto páginas.

Podemos y tenemos que tener una zona de memoria que no este páginada. En ese pedazo de memoria no paginado metemos el nucleo del sistema operativo.



Cuando el procesador ejecute 
mov eax, dato(implicitamente dato = dw ptr[ds:dato] ) 
con la paginacion activada, yo no tengo que hacer nada mas, no me tengo que preocupar en pensar como sera la traduccion de eso.


Cada entrada del "directorio de tabla de paginas(DTP)"(1er nivel) apunta a una "tabla de páginas(PT)". y cada tabla de pagina apunta a pagina de memoria.

Cada 

1024 entradas, 4k, entonces cada tabla tiene 4 bytse.(C)

Puedo ubicar 2^20 paginas de 4k.



En el ejercicio 6 se usa el teclado.
- Habilitacion de teclado en bochs.

La unidad de paginacion recibe(come) los 32 bits de la direccion lineal y devuelve una direccion fisica.






Pregunta de parcial y de final.

Nos va a poner numeros y nos va a decir como poner las tablas de paginas para poder acceder al dato.
me pone numeros de direccion y dice esta instruccion esta en la direccion lógica tal..


