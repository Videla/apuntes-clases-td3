# Clase 7

La memoria de programa va de arriba hacia abajo y la memoria de pila va de abajo hacia arriba, porque inicialmente estaba pensado para que no se choquen entre 

## ABI (Application Binary Interface)

ES una normalizacion de facto.

Vamos a tener varios archivos, podemos tener codigo en assembler, en C. La forma en que interactuan los simbolos, variables, llamadas a funcion debe estar preacordada.
Asi es que se determinan las calling conventions.

Nos plantea una programacion modular en varios lenguajes. Para aumentar la eficiencia.
Hay librerias estaticas y dinámicas.

Para cargar argumentos pueden pasarse a travez de registros, para saber por medio de que registros se pasaran debemos tener una convencion para saber como se va a manejar. Otra opciones usar la pila, por lo que.
Otra opcion seria pasar un puntero a memoria donde la direccion tenga todo lo que necesitamos.

Se lo tiro en la pila en el orden en que esa arquitectura maneje la pila.

Si le paso un entero, un char y un flotante a la funcion, a la pila.
Lo primero que apilo es el ultimo de los argumentos.

## Ver dibujo Pila

En el caso de ABI32, el valor de retorno pasa por EAX.
Los apilo en el orden inverso en que aparecen en el argumento de la funcion.

Ahora definimos el concepto de API (Application Prograamming Interface.)


Aca explican el comando man de linux, si hacemos man kill nos da 2 opciones.

Wrapper organiza los datos.
Antes de comermelos, los envuelve.
Es como una funcion con un proposito particular.


Quedan por ver System call y Servicios.

## Ver dibujo Servicios Kernel

La entrada al subsistema de servicios de linux. Hay un descriptor en el SLOT 80 (C)


Vemos presentacion Llamadas a funciones. 

Extern es modificador, no es tipo de variable
const es variable, pero no varia.


locales
globales
estaticas
volatiles

Vamos a aplicar la api, vamos a hacer decompiladores y transformar a assembler.

Primero pusehamos el ultimo parametro y despues llamamos a la funcion.
Crece hacia arriba la pila.
Suponemos que los arg son enteros, como todo entra dentro de 32 bits.
Siempre se hacen de a bloques de a 4 bytes. Aunque use tamaños de memoria mas chicos.
Si uso tamaños de memoria mas grandes, usa 2 bloques de 4 bytes y asi sucesivamente.

Ahora aparece el tema Stack frame, que es abrir una nueva ventana.

Cuando hago call guardo en la pila la direccion de retorno, si es un salto intersegmento guardo una variable, si es extrasegmento, guardo 2.

## dibujo pila 2

En el stack frame se guardan las variables locales.

El precompilador se fija cuantas variables locales tendre en la funcion y en base a eso genera el numero nx4 de variables para el stack frame.

sub esp, 16 ; stack frame.

Cuando entra a la funcion, guarda el valor de ebp 

El ABI 32 deja establecido que devuelve con EAX entonces no hay que mover el resultado.

El resultado no se entrega a travez de la pila, se devuelve a travez de EAX.

Al salir de la funcion, tenemos que liberar el espacio en el stack frame, tenemos que mover denuevo el ESO a EBP, mediante la suma de 16.

La pila aun no quedo balanceada.

Si constantemente estoy llamando, tarde o temprano tengo que sacar los argumentos de la pila.

En las funciones que tienen indefinida la cantidad de argumentos, uno de los argumentos es la cantidad de argumentos.

Ahora quedaria por ver Linker Script.


Uno de los parametros de la IDT es el DPL de quien me puede llamar, cuando hago una llamada por interrupcion, como kernel estoy obligando a entrar siempre por el mismo lugar. Genero puntos de entrada controlados despues se puede por algun error transformarse en un agujero en la seguridad. Son puntos de entrada controlados por servicio.

La API es el prototipo de la funcion, con la api el compilador sabe como debe colocar la pila para cuando viene el codigo.
Es la interfaz de programacion que debo saber para acceder a esa funcion.
El ABI es la interfaz binaria de programacion que, en caso de querer acceder a una funcion, tengo que saber como pasar argmentos, si los paso a travez de registros o atravez de la pila.

La ABI de ARM es diferente a la de Intel.

Cuando quiero combinar dos lenguajes distintos, C y assembler, o C y pascal. Como acomoda este codigo depende del compilador.


La instruccion mov [edi], 4

quiere escribir 4, pero no sabe si escribirlo en byte, en int, en char, etc.. si no se lo ponemos, el compilador dara un error.

mov dword [edi], 4

Esta forma es mas explicita y dice que lo escriba en un dword

int 80h es el bervicio be impresion de pantalla
Cuando se maneja punto flotante, se maneja mediante los registros del coprocesador.

Se apila primero la parte alta (+4) y despues la parte baja.

---

f_

## Ahora explicaran Linker Script.

Primer propuesta, con define.

El problema de esta propuesta es que ocupa demasiada memoria. Al inicializar el puntero con las 4MB posiciones de FF. No es viable porque el programa esta en ROM y seria mas grande de lo que puede entrar en la ROM.


El segundo codigo, va a ser mas compacto, pero va a estar en ROM.

El compilador no sabe como es la arquitectura ni como es el mapa de memoria de mi procesador.


La respuesta a estos problemas es el linker, que es quien se va a engargar de ubicar las cosas en memoria.

Primero aclararemos algunas cosas:
Cada etiqueta que usamos al programar en assembler, se llama Symbol.
Los simbolos no ocupan lugar en memoria, son punteros.

Nosotros vimos recien que podemos armar nuestro codigo en varios archivos fuentes y cada archivo sera un modulo.

# dibujo modulos.

Cuales seran las tareas del compilador?
El preoprocesamiento, parsing, optimizacion de codigo, donde lo que haec es si una variable no hace nada, no la coloca por ejemplo.


Luego tenemos en "Mision del Linker" un mapa de como trabaja el Linker.

Los compiladores me convierten el codigo en archivos objeto, en formato elf32, son archivos binarios.(C)
Una vez obtengo los archivos objetos, que pueden ser externos, de librerias que compro, o librerias estaticas, que significan linkear el codigo dentro de mi propio codigo.

El linker junta todos los archivos objeto y los junta en un solo objeto.

Luego se carga en memoria y en ese proceso se cargan las librerias dinamicas.

Que pasa si yo tengo interdependiencia entre los archivos? El compilado se hara por cada un o de los modulos.

Hay una manera de declarar la funcion diciendole que esta afuera.

Entre otras cosas hara la interdependencia de simbolos.

El compilador dara error y dira que no encuentra la referencia a X.

Como le indico al compilador que otro elemento mas adelante va a resolver las relaciones horizontales entre los modulos. Le tengo que avisar que estas variables estaran en otro modulo.


Extern es una directiva de assembler.

Todos los simbolos mueren dentro del modulo, para que se puedan ver desde afuera, tengo que usar la palabra global.


Como es en el caso de C?

## Dibujo 3.

Todas las variables en C que estan fuera de las funciones son variables globales.

Printf es una libreria dinamica.

Hasta ahora usabamos el NASM y no linkeabamos, pero por que? En realidad por la forma en que llamabamos al NASM ya estaba linkeando.


Global y extern van siempre de a pares.

Que yo quiera tener variables locales en assembler, es automatico (C)

Aca aparecen 2 conceptos nuevos, despues va a aparecer un 3ro cuando aparezca paginacion.


Vamos a tener que tenes 2 espacios de memoria.

LMA: Load Memory Adress.

Estan referidas a una direccion virtual.

Voy a tener que definirle 2 mapas de memoria al programa, pero tambien voy a tener que ponerle otro mapa de memoria. 
Tener en cuenta que estara en otro lado y cargarle estas referencias.

En otro momento estas direcciones seran fisicas.

Puedo tener bloques de datos que sean reubicables o no.

progbit
nobit

Cuando compilamos este modulo, producimos un archivo objeto. Ese archivo tendra un binario y en la parte superoir tendra un encabezado en formato ELF32, donde estaran las dependencias, tablas de simbolos, constantes, etc.

Como se llamaran los segmentos de codigo? Secciones.

Las secciones definen partes del codigo.

Datos no inicializados(BSS) seria por ejemplo:
int i;
No ocupa memoria, porque reservo lugar en RAM, en tiempo de ejecucion.

.init .finit
Tienen codigo de inicializacion y de desinicializacion.

Estas palabras que empiezan con . son palabras predefinidas del gcc, pero puedo crear las mias propias.

Normalmente mi codigo lo pongo en la rom y mi entry point iba al principio de todo.

El punto de entrada al codigo no necesariamente tiene que estar arriba de todo.

Como se da cuanta el linker cual es el punto de entrada? Cual es el main?
Dentro del encabezado, hay una variable o simbolo que es el entry point. por defecto para assembler es el _start: 
_f


Como se asocia el entry point con la direccion de reset de memoria? mediante un jmp en 0xFFF0.

Main es el entry point de C.


El compilador no mueve el codigo, lo cablea. Tengo una referencia una variable x. 

## dibujo


Lo que se quema en la rom es el VMA y el LMA es lo que realmente se va a estar ejecutando.(o viceversa.

-32M es 32 bit 

LNA es donde meto el codigo. VMA es que quiero que lo interprete como que lo esta ejecutando en otro lado.

Defino una seccion .text de salida.
Como aun no toque nada respecto a la VNA vamos a definir un archivo objeeto, que quiero que se interprete como que esta en 0x08000.

La convencion de hacer __ para por ejemplo:
	__codigoInicio viene del lenguje C

Y se escribe asi tanto en Assembler como en C.
