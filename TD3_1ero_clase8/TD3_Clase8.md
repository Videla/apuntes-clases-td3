# Clase 8

Sobre el linker script.
La unica regla es:
Si vamos a mover codigo, hacerlo antes de mover el codigo, si vamos a mover datos, antes de tocar los datos.


Primero vamos a ver un ejemplo.
el "holamundo.c"

Como es un lenguaje de alto nive, se escribe de a bloques.
main si bien es el punto de entrada, antes de eso esta init y finit.

Vamos a ver un codigo en C para ver como funciona la ABI, etc..

Creo que es importante la diferencia entre variables inicializadas y no inicializadas.

las variables locales solo viven en su ambito, en el caso de las variables estaticas, no tienen esa particularidad. Puedo volver a entrar a la funcion y mantienen su valor, no se manejan a travez de la pila. Sigue viviendo dentro del ambito, desde afuera no la veo, pero si vuelvo a entrar en la funcion mantiene su valor.

Es bueno tener un encabezado bien completo en los codigos. Es bueno cuando se trabaje en gupo, tener un titulo, quien lo hizo, cuando, etc..

-m32 es mdo 32 bit.

para hacer un desensamblado hay que agregar 
-masm=intel: modo assembler de intel (para diferenciarlo de AT&T)
-O0 Optimizacion cero, no quiero que optimice nada. 
.dis es el desensamblado.

Las directivas del compilador no ocupan lugar

En Ejercicio 2.asm se ve que llama a la pila, la ABI.

ENTER y LEAVE. Todo lo que estoy haciendo a mano en assembler me lo resuelven las funciones ENTER y LEAVE.
Sirven para manejar el Stack frame, llerlo en el manual.

El linker script es referenciado por el Makefile.


Linker Script es diferente a Linker, el linker es el linker de C de toda la vida, el linker script es algo que usa el makefile para armar la combinacion. El linker usa al linker script.


ELF: Executable linkable format.

# haciendo objdump -h Ejercicio 2 vemos columnas que nos dan mucha informacion.

Hay mas secciones que las que definimos originalmente.

parece las coconidas, init y finit que son codigos que se llaman en la carga y salida de un archivo en C. 
.text es el codigo.

El GCC genera muchas de las secciones automaticamente.

Son secciones bastante grandes, porque agrega codigo en C que ocupa bastante espacio.


# objdump -t me da la tabla de simbolos

Figura la seccion a la que pertenece y el nombre del simbolo.

printf es una llamada a la funcion de la LIBC y como aun existe, 
La librerias dinamicas se cargan una vez en memoria.
Las estaticas, el codigo se agrega a mi codigo y se compila todo junto. 
Las dinamicas se cargan en memoria y mi codigo las llama.

Que ventaja tienen las dinamicas? printf no tiene sentido que la compile 1000 veces entonces 

Ahora hacesmos un objdump de uno solo de los codigos objeto
objdump -t Ejercicio2.o
La salida es mucho mas chica.

El compilador si no enceuntra el simbolo tira error, pero si le aviso, no lo marca como error.

objdump -t Library.o

objdump -a muestra el formato del binario


Otra herramienta que se puede usar es el 
# readelf -a


Finalmente tenemos el 
# hexdump -C
Con el que veremos el LMA. Veremos si quedaron huecos en ese binario de salida.

A la izquierda tenemos el indice dentro del binario, no en memoria.

Tenemos el hexadecimal, la traduccion a ascii.
Dentro del binario que dijimos que habia arriba de todo? El encabezado ELF.

Un ELF arranca con .ELF en ASCII. 

El archivo que va a la memoria no es el ELF.

El sistema operativo lee el archivo ignora el encabezado.
Si quiero el codigo llano tengo que agarrar el objeto y convertirlo a binario sacando todo el binario.


gcc -f binary, le digo que no me haga un archivo ELF de salida, sino uno binario directamente.

ROdata
Read only data.



# Linker Script
#cuaderno
tengo un mapa de memoria que representa a mi sistema.

Podria tener un codigo que haga: jump iniicio

C:db 4
a: resb 2

inicio:
	mov ax, [c]

Como lo va a compilar el compilador en cuanto a dependencias entre simbolos?

EAh es el codigo del jump.
Tiene que haber una direccoion de 32 bits. Cuando son saltos cortos se manejan en forma relativa. Esto seria un jump +3

c: ff0Eh

Necesito para poder mover variables en el mapa de memoria, poder referenciarla como quiera. Por ejemplo si necesito que a este en otro lado.

Para que quiero mover la variable? Eso no tiene sentido aca, tambien podria mover Inicio
Si trabajo en un sistema operativo, todos nuestros programas estaran en memoria.. Ahi empieza a tener sentido a mover datos en memoria. 

Asumir que estan en otro lugar de memoria al cablearlos.

Quien va a mover o inicializar eso realmente ahi? Nosotros. En nuestro caso, nosotros construiremos nuestro propio loader.

Entry point

parte de datos, parte de codigo.
Apenas entra en el codigo, tengo que mover las variables inicializadas  a su lugar correcto en memoria.

.bss son secciones no inicializadas, que no ocupan memoria.

Lo unico que tengo que haces es reservar memoria y no escribir en esos lugares porque en el futuro esas variables van a inicializarse.

Si bien desde el punto de vista teorico no hay que inicializarlas. el GCC por razones de seguridad las inicializa en Cero.

El GCC escribe secciones extra, init y finit.

El codigo lo que tiene que hacer es mover las secciones.

Si no inicializara el ...



El loader se encarga de copiar las cosas de LMA a VMA.

El loader es un programa cargado en ROM, que es parte de mi codigo y que la VMA y la LMA van a coincidir.

Habia quedado pendiente deifnir como escribir las LMA.

Este codigo esta basado en un template que usamos para los ejercicios.

Hoy nosotros interpretamos la direccion lineal como la direccion fisica. A travez de un mecanismo de traduccion se puede transformar a fisicas. Nosotros trabajamos siempre con un codigo en la misma direccion.  

Como se define el linker script.

Una de las cosas que se pueden agregar al linker script son las sections, pero hay mucho mas que se puede agregar. 


#El location counter esta asociado a la VMA (Muy importante)

Cada seccion que estoy colocando ahora.
El linker script se toma en todos los parciales y finales, talez no tan completo o largo como esta en rom.lds, pero se toma.

Defino el location counter en RAM, porque se que va a ir en VMA.

rom_header es el nombre de la seccion de salida.

El rom header es el nombre de mi seccion de salida.

Entre llaves ira la seccion de entrada.

En el binario podemos ver en que orden se van apilando. 

Para mover datos tengo que saber 3 parametros, desde donde, hasta donde y cuanto.

Siguiente seccion de salida, main16.

Cual sera la direccion? como no toque el location counter sera directamente abajo de lo anterior.
Como quiero dejar un espacio

Todas las secciones que se llamen init16 perteneceran a main.

Si no lo calculo bien puedo pisar la seccion anterior, pero el linker dara un error.

Si no pongo AT, pone todo pegado. AT especifica la LMA.

Son 2 formas de definir la VMA

. = 0x000D000;		usaremos mas esta
.rom_header 0xD0000:

luego pongo mi location pointer mas arriba, osea, calramente en RAM.

Una vez definido el location counter voy a definir otra seccion de salida.

Una vez definimos el simbolo apuntando ahi, definiremos.

main32 quiero que este justo abajo de main32.
El binario en ROM es todo compacto, despues lo voy a desparramar en la RAM.

Defino el simbolo de la LMA y las secciones que metere ahi dentro.

Y despues voy a establecer otro simbolo que sera igual al location counter.

Se carga abajo, pero interpreta que esta arriba.

	____________________________________________________________________

Hay que tratarlo como 2 roms distintas.
Una es un bodoque binario de bytes consecutivos. HAce un salto a un lugar que no esta en ese bodoque.


Una vez que inicializamos trabajaremos en modo protegido.
Y despues empiezo a hacer el movimiento de datos.

Aparece un ainstruccion nueva, movsb: mov string byte.
Mueve de abytes algo que esta apuntado en ds:esi -> es:edi
Que haces de particular incrementa los punteros, va incrementando ambos, esi y edi.

rep lo que hace es repetir movsb "ecx" veces


No hace falta hacer un call/ crear una funcion para que copie, porque hace lo mismo que la instruccion de copia, pero en una sola linea, es mas compacto.

Los punteros se incrementan o decrementan segun el flag de direccion que puede ser cld o std, no lo vamos a tocar, asi que por defecto se incrementan.

Lo que haces es ir moviendo todas las secciones.

# Estamos viendo Bochs LD\init32.asm

Se puede dividir por 4 sin usar ciclos de reloj?

Si! haciendo la distributiva

Lo que queremos hacer es (A-B)/4
Entonces hacemos A/4 - B/4
Con todos los valores constantes podemos hacer operaciones aritmeticas y logicas, el compilador lo soporta,

SIM. >> 2

Puede hacer desplazamientos incluso.

stosb vuelve eax.

el XOR se puede hacer con al solamente y ahorra.


#Ahora hacemos la practica
La secuencia de inicializacion de ROM es aquella en la que pasamos a 32 bits, movemos algunas cosas, etc.


Codigo para compilar 

nasm orig1.asm orig2.asm -f bin
